﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "BedGraphShader/Shader01"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 pos : POSITIONT;
				float3 normal : NORMAL;
				float3 pos2 : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.pos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.pos2 = v.vertex;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float h = 4*saturate(i.pos2.z );
                fixed4 col = fixed4(0,0,1,1);
                if(h<=1){
                    col = lerp(fixed4(0,0,1,1),fixed4(0,1,1,1),h);
                }else if(h<=2){
                    h = h-1;
                    col = lerp(fixed4(0,1,1,1),fixed4(0,1,0,1),h);
                }else if(h<=3){
                    h = h-2;
                    col = lerp(fixed4(0,1,0,1),fixed4(1,1,0,1),h);
                }else{
                    h = h-3;
                    col = lerp(fixed4(1,1,0,1),fixed4(1,0,0,1),h);
                }

				//compute world space view direction
				float3 V = normalize(UnityWorldSpaceViewDir(i.pos));
				col = col * dot(V, i.normal);
                return col;
            }
            ENDCG
        }
    }
}
