﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshControllerScript : MonoBehaviour
{
    [Range(2,100)] public int resX;

    [Range(2,100)] public int resY;

    [Range(0,1)] public float mode;



    MeshFilter meshFilter;
    bool settingUpdated;

    Mesh mesh;
    void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        mesh = meshFilter.mesh;
    }

    void OnValidate() {
        settingUpdated = true;
    } 
    // Update is called once per frame
    void Update()
    {
        if(settingUpdated){
            
            updateMesh();
            settingUpdated=false;
        }
    }

    
    public void updateMesh(){

        Vector3 trans(Vector3 v){
            float a = v.y * ( Mathf.PI / 2 );
            return Vector3.Lerp(new Vector3(v.x, (1 + v.z) * (Mathf.Sin(a)), (1 + v.z) * (Mathf.Cos(a))), v, mode) ;
        }


        foreach(MeshFilter mf in GetComponentsInChildren<MeshFilter>()){
            Vector3[] vertices = mf.mesh.vertices;
            Vector3[] newVertices = new Vector3[vertices.Length];
            for(int i = 0; i<vertices.Length; i++){
                newVertices[i] = trans(vertices[i]);
            }
            
            mf.mesh.vertices = newVertices;
        }
        
    }

}
