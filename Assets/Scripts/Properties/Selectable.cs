﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelectable
{
    void Selecting(Vector3 toolPos);
    void Deselect(HandScript handScript);
    Transform GetTransform();
}
