﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class InteractableInterface: MonoBehaviour
{
    public abstract void Pinch(Transform t);

    public abstract void Pinching(Transform t);

    public abstract void Unpinch(Transform t);
    
}
