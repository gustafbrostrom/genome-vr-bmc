﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntervalTree;

[RequireComponent(typeof(Renderer))]
public class Heatmap : MonoBehaviour
{   
    private Texture2D texture;
    [Range(1,1000)]public int width = 100;
    [Range(1,1000)]public int height = 1;
    
    public Color baseColor = Color.white;
    private Color[] baseArray;

    public Gradient gradient;
    private Color[] precalculatedGradient;
    private int colorResolution = 256;

    // Start is called before the first frame update
    void Awake()
    {
        texture = new Texture2D(width, height);
        texture.filterMode = FilterMode.Point;
        GetComponent<Renderer>().material.mainTexture = texture;
        baseArray = new Color[width*height];
        for(int i = 0; i < width*height; ++i) 
        {
            baseArray[i] = baseColor; 
        }  
    }

    void OnValidate(){
        if(gradient == null)
            return;
            
        precalculatedGradient = new Color[colorResolution];
        for(int i = 0; i < colorResolution; ++i)
        {
            precalculatedGradient[i] = gradient.Evaluate((float)i/(float)(colorResolution - 1));
        }
    }

    public void ClearTexture() {
        texture.SetPixels(baseArray);
        texture.Apply();
    }

    public void UpdateTexture<T>(IEnumerable<KeyValuePair<Interval<int>, T>> intervals, int l1, int l2) {
        int ToTextureCoordinates(int l){
            return (int)(((float)(l - l1)/(float)(l2 - l1))*width*height);
        }

        int[] heatArray = new int[baseArray.Length];
        int maxHeat = 0;
        foreach(KeyValuePair<Interval<int>, T> kvp in intervals){
            Interval<int> interval = kvp.Key;
            int texl1 = ToTextureCoordinates(Mathf.Max(l1, interval.Start));
            int texl2 = ToTextureCoordinates(Mathf.Min(l2, interval.End));

            for(int i = texl1; i <= texl2; ++i){
                heatArray[i]++;
                if(heatArray[i] > maxHeat)
                    maxHeat = heatArray[i];
            } 
        }
        Color[] colorArray = new Color[baseArray.Length];
        int index;
        for(int i = 0; i < heatArray.Length; i++){
            index = (int)((((float)heatArray[i])/((float)maxHeat))*(float)(colorResolution - 1));
            if(heatArray[i] > 0)
                colorArray[i] = precalculatedGradient[index];
            else
                colorArray[i] = baseColor;
        }
        texture.SetPixels(colorArray);
        texture.Apply();
    }


}
