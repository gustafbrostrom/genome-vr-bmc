﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Texture2DHandler))]
public class SelectableCylinder : MonoBehaviour, ISelectable
{

    private float lowerPercentage = -1f;
    private float upperPercentage = -1f;
    private Texture2D tex;

    public void Start()
    {
        tex = GetComponent<Texture2DHandler>().texture;
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public void Selecting(Vector3 toolPos)
    {
        float newPercentage = WorldPosToCylinderPercentage(toolPos);
        if(lowerPercentage == -1f)
        {
            lowerPercentage = newPercentage;
            upperPercentage = newPercentage;
        }
        else
        {
            int x1 = 0;
            int x2 = 0;
            if (newPercentage < lowerPercentage)
            {
                x1 = TexCoordFromPercentage(newPercentage);
                x2 = TexCoordFromPercentage(lowerPercentage);
                lowerPercentage = newPercentage;
            }else if (newPercentage > upperPercentage)
            {
                x1 = TexCoordFromPercentage(upperPercentage);
                x2 = TexCoordFromPercentage(newPercentage);
                upperPercentage = newPercentage;
            }
            for(int i = x1; i< x2; i++)
            {
                for (int j = 0; j <tex.width; j++)
                {
                    tex.SetPixel(j, i, Color.blue);
                }
            }
            tex.Apply();
        }
    }

    public void Deselect(HandScript handscript)
    { 
        if(lowerPercentage !=-1 && upperPercentage != -1)
        {
            
        }
        lowerPercentage = upperPercentage = -1;
    }

    private int TexCoordFromPercentage(float percentage)
    {
        return (int)(percentage * (float)tex.height);
    }

    private float WorldPosToCylinderPercentage(Vector3 pos)
    {
        Vector3 dir = CylinderTop() - CylinderBottom();
        Vector3 dir2 = pos - CylinderBottom();
        Vector3 v = Vector3.Project(dir2, dir);
        
        return Mathf.Clamp01(v.magnitude / dir.magnitude);
    }

    public Vector3 CylinderTop()
    {
        return transform.TransformPoint((Vector3.up * this.gameObject.transform.localScale.y));
    }

    public Vector3 CylinderBottom()
    {
        return transform.TransformPoint((-Vector3.up * this.gameObject.transform.localScale.y));
    }
}
