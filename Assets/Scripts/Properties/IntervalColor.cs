﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntervalTree;

[RequireComponent(typeof(Renderer))]
public class IntervalColor : MonoBehaviour
{   
    private Texture2D texture;
    [Range(1,1000)]public int width;
    [Range(1,1000)]public int height;
    public Color baseColor = Color.white;
    private Color[] baseArray;
    private IntervalTree<int, Color> intervals;

    // Start is called before the first frame update
    void Start()
    {
        intervals = new IntervalTree<int, Color>();
        texture = new Texture2D(width, height); //Flippdy floppidy
        texture.filterMode = FilterMode.Point;
        GetComponent<Renderer>().material.mainTexture = texture;
        baseArray = new Color[width*height];
        for(int i = 0; i < width*height; ++i) 
        {
            baseArray[i] = baseColor; 
        }  
    }

    public void AddInterval(int l1, int l2, Color c){
        intervals.Add(l1,l2,c);
    }

    public void UpdateTexture(int l1, int l2) {

        int ToTextureCoordinates(int l){
            return (int)(((float)(l - l1)/(float)(l2 - l1))*width);
        }

        Color[] colorArray = baseArray;
        foreach(KeyValuePair<Interval<int>, Color> color in intervals.GetIntervalsOverlappingWith(new Interval<int>(l1,l2))){
            Interval<int> interval = color.Key;
            int texl1 = ToTextureCoordinates(Mathf.Max(l1, interval.Start));
            int texl2 = ToTextureCoordinates(Mathf.Min(l2, interval.End));
            
            for(int i = texl1; i < texl2; ++i)
                colorArray[i] = color.Value; 

        }
        texture.SetPixels(colorArray);
        texture.Apply();
    }


}
