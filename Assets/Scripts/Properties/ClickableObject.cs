﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]

public class ClickableObject : MonoBehaviour
{
    public UnityEvent onClick;

    // Start is called before the first frame update
    void Awake()
    {
        if(onClick == null)
            onClick = new UnityEvent();
    }
    // Update is called once per frame
    void Update()
    {

    }
}
