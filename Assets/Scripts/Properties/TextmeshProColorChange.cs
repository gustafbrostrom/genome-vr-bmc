﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextmeshProColorChange : MonoBehaviour
{
    private Hoverable hov;
    private TextMeshPro text;
    private Color stdColor;
    public Color hoverColor = Color.blue;
    // Start is called before the first frame update
    void Start()
    {
        hov = GetComponent<Hoverable>();
        hov.onEnter = Enter;
        hov.onExit = Exit;
        text = GetComponent<TextMeshPro>();
        stdColor = text.color;
    }

    private void Enter()
    {
        text.color = hoverColor;
    }

    private void Exit()
    {
        text.color = stdColor;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
