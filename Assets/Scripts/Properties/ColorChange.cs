﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorChange : MonoBehaviour
{

    private Hoverable hov;
    private Image img;
    private Color stdColor;
    public Color hoverColor = Color.blue;
    // Start is called before the first frame update
    void Start()
    {
        hov = GetComponent<Hoverable>();
        hov.onEnter = Enter;
        hov.onExit = Exit;
        img = GetComponent<Image>();
        stdColor = img.color;
    }

    private void Enter()
    {
        img.color = hoverColor;
    }

    private void Exit()
    {
        img.color = stdColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
