﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Collider))]
public class Hoverable : MonoBehaviour
{
    // Start is called before the first frame update
    public Action onEnter = delegate { };
    public Action onExit = delegate { };
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
