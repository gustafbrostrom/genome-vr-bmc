﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderKnobInteraction : MonoBehaviour
{
    private InteractionTool tool;
    private RectTransform sliderTransform;
    private RectTransform sliderHandleTransform;
    private Slider slider;
    private BoxCollider collider;
    // Start is called before the first frame update
    void Awake()
    {
        sliderTransform = transform.parent.parent.GetComponent<RectTransform>();
        slider = sliderTransform.GetComponent<Slider>();
        sliderHandleTransform = GetComponent<RectTransform>();
        collider = GetComponent<BoxCollider>();
        collider.size = new Vector3(sliderHandleTransform.rect.width, sliderHandleTransform.rect.height, 1f);
    }

    // Update is called once per frame
    void Update()
    { 
        if (tool && tool.isPinching)
        {
            float xpos = sliderTransform.InverseTransformPoint(tool.transform.position).x;
            float totalLength = sliderTransform.rect.width;
            float barLength = sliderHandleTransform.rect.width;
            xpos += totalLength / 2;
            xpos -= barLength / 2;
            float percentage = xpos / (totalLength - barLength);
            if (percentage < 0)
                percentage = 0f;
            if (percentage > 1)
                percentage = 1f;
            slider.value = percentage;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("InteractionTool"))
        {
            tool = other.GetComponent<InteractionTool>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("InteractionTool"))
        {
            tool = null;
        }
    }
}
