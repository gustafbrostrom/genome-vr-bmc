﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollBarInteraction : MonoBehaviour
{
    public InteractionTool tool;
    private RectTransform scrollbarTransform;
    private RectTransform barTransform;
    private Scrollbar scrollbar;
    private BoxCollider collider;

    // Start is called before the first frame update
    void Start()
    {
        scrollbarTransform = transform.parent.parent.GetComponent<RectTransform>();
        scrollbar = scrollbarTransform.GetComponent<Scrollbar>();
        barTransform = GetComponent<RectTransform>();
        collider = GetComponent<BoxCollider>();
        collider.size = new Vector3(barTransform.rect.width, barTransform.rect.height, 1f);
    }


    // Update is called once per frame
    void Update()
    {
        if (tool && tool.isPinching)
        {
            float ypos = -scrollbarTransform.InverseTransformPoint(tool.transform.position).y;
            float sizeOfScrollBar = barTransform.rect.height;
            ypos -= sizeOfScrollBar / 2;
            float percentage = ypos / (float)(scrollbarTransform.rect.height-sizeOfScrollBar);
            if (percentage < 0)
                percentage = 0f;
            if (percentage > 1)
                percentage = 1f;
            scrollbar.value = 1-percentage;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("InteractionTool"))
        {
            tool = other.GetComponent<InteractionTool>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("InteractionTool"))
        {
            tool = null;
        }
    }
}
