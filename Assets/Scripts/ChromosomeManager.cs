﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using RangeTree;
using System.Linq;
using System;
using System.Diagnostics;
using System.Data;
using Mono.Data.Sqlite;
using System.Threading;
using System.IO;
using System.Globalization;

public class ChromosomeManager : MonoBehaviour
{
    public ReferenceManager rm;

    private Sql database;

    public GameObject displayPrefab;

    public TextMeshPro searchBar;

    public GameObject KeyBoard;

    public List<TextMeshPro> suggestions;


    //Cache
    private Dictionary<string, List<Interaction>> intraChromosomalInteractions;
   
    private Dictionary<string, Dictionary<string, List<Interaction>>> interChromosomalInteractions;

    private Dictionary<string, List<Gene>> geneListsByChromosome;

    public Dictionary<string, RangeTree<int, Gene>> geneRangeTreeByChromosome;

    public Dictionary<string, List<ProteinBinding>> proteinBindingsListsByChromosome;

    public Dictionary<string, RangeTree<int, ProteinBinding>> proteinBindings;




    public List<GameObject> ChrKeeper;

    private string proteinBindingsPath = "URI=file:Data/ProteinBindingsSortedByChromosome.db";

    //Data

    public Dictionary<string, float> percentageOfLoadedProteinBindings = new Dictionary<string, float>();


    public Dictionary<string, List<PointOfInterest>> pois = new Dictionary<string, List<PointOfInterest>>();


    

    // Start is called before the first frame update
    void Start()
    {

        database = rm.database;
        ChrKeeper = new List<GameObject>();
        interChromosomalInteractions = new Dictionary<string, Dictionary<string, List<Interaction>>>();
        intraChromosomalInteractions = new Dictionary<string, List<Interaction>>();
        suggestions = new List<TextMeshPro>();
        suggestions.Add(KeyBoard.transform.Find("Suggestion").Find("SuggText").GetComponent<TextMeshPro>());
        for (int i = 0; i < 8; i++)
            suggestions.Add(Instantiate(suggestions[suggestions.Count - 1].transform.parent, suggestions[suggestions.Count - 1].transform.parent.position + Vector3.down * 0.18f, suggestions[suggestions.Count - 1].transform.parent.rotation, suggestions[suggestions.Count - 1].transform.parent.parent).GetChild(0).GetComponent<TextMeshPro>());

        geneListsByChromosome = new Dictionary<string, List<Gene>>();

        proteinBindings = new Dictionary<string, RangeTree<int, ProteinBinding>>();
        proteinBindingsListsByChromosome = new Dictionary<string, List<ProteinBinding>>();


        //addDisplay("chr1", new Vector3(1, 1, -1), Quaternion.identity);
        //loadProteinBindingsForChromosome("chr1");
        //readAndCompareInteractions();

    }

    public void SpawnTest()
    {
        
        GameObject g = Instantiate(rm.LaserPrefab, transform);
        PointOfInterest p = g.AddComponent<PointOfInterest>();
        p.Setup("chr1", 100000000);
        pois.Add("chr1", new List<PointOfInterest>() { p });

        g = Instantiate(rm.LaserPrefab, transform);
        p = g.AddComponent<PointOfInterest>();
        p.Setup("chr1", 50000000);


        pois["chr1"].Add(p);
        pois["chr1"].Sort((x1,x2) => x1.location - x2.location);



    }


    public List<PointOfInterest> GetPointsOfInterest(string chr, int l1 = -1, int l2 = -1)
    {

        if (!pois.ContainsKey(chr))
            return new List<PointOfInterest>();

        if (l1 == -1 && l2 == -1)
            return pois[chr];

        List<PointOfInterest> temp = pois[chr];
        int botIndex = 0;
        int topIndex = temp.Count - 1;
        try
        {
            while (temp[botIndex] && temp[botIndex].location < l1)
                botIndex++;
        }
        catch(Exception e)
        {
            print(botIndex);
            
            print(temp[botIndex]);
        }
        while (temp[topIndex] && temp[topIndex].location > l2)
            topIndex--;

        return temp.GetRange(botIndex, topIndex - botIndex + 1);

    }

    public List<Gene> GetGenes(string chr)
    {
        if (!geneListsByChromosome.ContainsKey(chr))
            geneListsByChromosome.Add(chr, rm.database.GetGenes(chr));

        return geneListsByChromosome[chr];
    }

    public List<Interaction> GetIntraInteractions(string chr, int l1 = -1, int l2 = -1)
    {
        if (!intraChromosomalInteractions.ContainsKey(chr))
            intraChromosomalInteractions.Add(chr, database.GetIntraInteractions(chr, l1, l2));

        return intraChromosomalInteractions[chr];
    }


    public Dictionary<string, List<Interaction>> GetInterChromosomalInteractions(string chr, int l1 = -1, int l2 = -1)
    {
        if (!interChromosomalInteractions.ContainsKey(chr))
            interChromosomalInteractions.Add(chr, database.GetInterInteractions(chr, l1, l2));

        return interChromosomalInteractions[chr];
        
    }

    public List<ProteinBinding> GetProteins(string chr, int l1 = -1, int l2 = -1)
    {
        return database.GetProteinBindings(chr, l1, l2);
    }

    public void addDisplay(string chr)
    {
        GameObject newDisplay = Instantiate(displayPrefab);
        DisplayHandler newDisplayHandler = newDisplay.GetComponent<DisplayHandler>();
        ChrKeeper.Add(newDisplay);
        newDisplayHandler.transform.Translate(1, 1, -1.2f);
        newDisplayHandler.SetupAndPassOnReferences(chr, rm);
    }

    public void addDisplay(Gene g, Vector3 pos = default)
    {
        GameObject newDisplay = Instantiate(displayPrefab, pos, Quaternion.identity);
        newDisplay.transform.LookAt(rm.cameraGO.transform);
        newDisplay.transform.Rotate(new Vector3(0, 180, 0));
        DisplayHandler newDisplayHandler = newDisplay.GetComponent<DisplayHandler>();
        ChrKeeper.Add(newDisplay);

        newDisplayHandler.SetupAndPassOnReferences(g.chr, rm, g.l1, g.l2);
       
    }

    public void addDisplay(string chr, Vector3 pos, Quaternion rot)
    {
        GameObject newDisplay = Instantiate(displayPrefab, pos, rot);
        newDisplay.transform.localScale *= 0.5f;
        DisplayHandler newDisplayHandler = newDisplay.GetComponent<DisplayHandler>();
        ChrKeeper.Add(newDisplay);

        newDisplayHandler.SetupAndPassOnReferences(chr, rm);
    }
    public void addDisplay(string chr, int start, int stop)
    {
        GameObject newDisplay = Instantiate(displayPrefab, rm.cameraGO.transform.TransformPoint(Vector3.forward * 0.4f), Quaternion.identity);
        newDisplay.transform.LookAt(rm.cameraGO.transform);
        newDisplay.transform.Rotate(new Vector3(0, 180, 0));
        newDisplay.transform.localScale = Vector3.one / 5000f;
        ChrKeeper.Add(newDisplay);

        DisplayHandler newDisplayHandler = newDisplay.GetComponent<DisplayHandler>();
        newDisplayHandler.SetupAndPassOnReferences(chr, rm);
        newDisplay.GetComponent<DisplayHandler>().updateLocations(start, stop);
    }


    public HashSet<Gene> getGenesStartingWith(string s)
    {
        HashSet<Gene> names = new HashSet<Gene>();
        int counter = 0;
        foreach (KeyValuePair<string, List<Gene>> entry in geneListsByChromosome)
        {
            foreach (Gene g in entry.Value)
            {
                if (g.name.ToLower().StartsWith(s.ToLower()))
                {
                    names.Add(g);
                    counter++;
                }
                if (counter >= suggestions.Count)
                    break;

            }
            if (counter >= suggestions.Count)
                break;
        }
        return names;
    }



    public static int BinarySearch(List<ProteinBinding> list, int key)
    {
        int min = 0;
        int max = list.Count - 1;
        while (min <= max)
        {
            int mid = (min + max) / 2;
            if (key == list[mid].mid)
            {
                return ++mid;
            }
            else if (key < list[mid].mid)
            {
                max = mid - 1;
            }
            else
            {
                min = mid + 1;
            }
        }
        return min;
        
    }

    public void ClearAllDisplays()
    {
        for (int i = ChrKeeper.Count - 1; i >= 0; i--)
        {
            Destroy(ChrKeeper[i]);
        }
        ChrKeeper.Clear();
    }


    #region



    public IEnumerable<Gene> GetGenesInRange(string chr, int l1, int l2)
    {
        return geneRangeTreeByChromosome.ContainsKey(chr) ? geneRangeTreeByChromosome[chr].Query(l1, l2) : Enumerable.Empty<Gene>();
    }

    public IEnumerable<ProteinBinding> getProteinBindingsInRange2(string chr, int l1, int l2)
    {
        return proteinBindings.ContainsKey(chr) ? proteinBindings[chr].Query(l1, l2) : Enumerable.Empty<ProteinBinding>();
    }

    public IEnumerable<ProteinBinding> getProteinBindingsInRange(string chr, int l1, int l2)
    {
        int x = BinarySearch(proteinBindingsListsByChromosome[chr], l1);
        int y = BinarySearch(proteinBindingsListsByChromosome[chr], l2);
        return proteinBindingsListsByChromosome[chr].GetRange(x, y);
    }

    public Gene getClosestGene(string chr, int l)
    {
//        if (!geneRangeTreeByChromosome.ContainsKey(chr))
//            return null;

        Gene gene = null;
        int closeset = int.MaxValue;
        foreach (Gene g in geneRangeTreeByChromosome[chr].Query(l - ReferenceManager.MaxSearchDepth, l + ReferenceManager.MaxSearchDepth))
        {
            int dist = (int)Mathf.Abs(l - g.mid);
            if (dist < closeset)
            {
                gene = g;
                closeset = dist;
            }
        }
        return gene;
    }


    public void typingOnKeyboard(string s)
    {
        //if (s == "toggle")
        //{
        //    toggleVoice = !toggleVoice;
        //    toggleVoiceCmd();
        //    return;
        //}
        for (int i = 0; i < suggestions.Count; i++)
        {
            suggestions[i].text = "";
            suggestions[i].transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        }
        if (s == "Clear")
        {
            searchBar.text = "";

        }
        else if (s == "Back")
        {
            if (searchBar.text != "")
            {
                searchBar.text = searchBar.text.Substring(0, searchBar.text.Length - 1);

                int i = 0;
                foreach (Gene g in getGenesStartingWith(searchBar.text))
                {
                    suggestions[i].text = g.name + " " + g.chr;
                    suggestions[i].transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
                    suggestions[i].transform.parent.GetComponent<Button>().onClick.AddListener(() => addDisplay(g));
                    i++;
                    if (i >= suggestions.Count)
                        break;
                }
            }
        }
        else
        {
            searchBar.text += s;
            int i = 0;
            foreach (Gene g in getGenesStartingWith(searchBar.text))
            {
                suggestions[i].text = g.name + " " + g.chr;
                suggestions[i].transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
                suggestions[i].transform.parent.GetComponent<Button>().onClick.AddListener(() => addDisplay(g));
                i++;
                if (i >= suggestions.Count)
                    break;
            }
        }
    }

    public void readAndCompareInteractions()
    {
        List<Interaction[]> result = new List<Interaction[]>();
        RangeTree<int, Interaction> treeSortedByAnchorA = new RangeTree<int, Interaction>();
        RangeTree<int, Interaction> treeSortedByAnchorB = new RangeTree<int, Interaction>();
        using (FileStream fs = File.Open("H3K4me3_FLEKO_5kb.interactions_FitHiC_Q0.05_WashU.bed", FileMode.Open))
        using (BufferedStream bs = new BufferedStream(fs))
        using (StreamReader sr = new StreamReader(bs))
        {
            int counter = 0;
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] s = line.Split(new Char[] { '\t' });
                string chrA = s[0];
                int startA = Convert.ToInt32(s[1]);
                int endA = Convert.ToInt32(s[2]);
                string[] s2 = s[3].Split(new Char[] { ':', '-', ',' });
                string chrB = s2[0];
                int startB = Convert.ToInt32(s2[1]);
                int endB = Convert.ToInt32(s2[2]);
                float score = float.Parse(s2[3], CultureInfo.InvariantCulture.NumberFormat);
                Interaction inter = new Interaction(chrA, chrB, startA, endA, startB, endB, score);
                treeSortedByAnchorA.Add(startA, endA, inter);
                treeSortedByAnchorB.Add(startB, endB, inter);
                counter++;
                if (counter % 10000 == 0)
                    print(counter);
            }
        }
        int reversedFound = 0;
        using (FileStream fs = File.Open("H3K4me3_FLWT_5kb.interactions_FitHiC_Q0.05_WashU.bed", FileMode.Open))
        using (BufferedStream bs = new BufferedStream(fs))
        using (StreamReader sr = new StreamReader(bs))
        {
            int counter = 0;
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] s = line.Split(new Char[] { '\t' });
                string chrA = s[0];
                int startA = Convert.ToInt32(s[1]);
                int endA = Convert.ToInt32(s[2]);
                string[] s2 = s[3].Split(new Char[] { ':', '-', ',' });
                string chrB = s2[0];
                int startB = Convert.ToInt32(s2[1]);
                int endB = Convert.ToInt32(s2[2]);
                float score = float.Parse(s2[3], CultureInfo.InvariantCulture.NumberFormat);
                Interaction inter = new Interaction(chrA, chrB, startA, endA, startB, endB, score);
                //Loop over all interactions in the other list where a in current overlaps a in other
                foreach (Interaction other in treeSortedByAnchorA.Query(startA, endA))
                {
                    //Check if b in current interaction overlaps with b in other
                    if (inter.isAnchorBOverlappingWithOtherAnchorB(other))
                        result.Add(new Interaction[] { inter, other });
                }
                //Loop over all interactions in the other list where a in current overlaps b in other
                foreach (Interaction other in treeSortedByAnchorB.Query(startA, endA))
                {
                    //Check if b in current overlaps with a in other
                    if (inter.isAnchorBOverlappingWithOtherAnchorA(other))
                    {
                        reversedFound++;
                        result.Add(new Interaction[] { inter, other });
                    }
                }
                counter++;
                if (counter % 10000 == 0)
                    print(counter);
            }
        }

        //using (System.IO.StreamWriter file =
        //    new System.IO.StreamWriter("protein_bindings_" + chr + ".txt"))
        //{
        //    foreach()
        //        file.WriteLine(name + " " + l1 + " " + l2);
        //}
        //int i = 0;
        //foreach(Interaction[] inters in result)
        //{
        //    if (++i > 100)
        //        break;
        //    print(inters[0].ToString() + " " + inters[1].ToString());
        //}
        print("Found " + reversedFound + " reversed");
        print(result.Count);
    }





    #endregion
}
