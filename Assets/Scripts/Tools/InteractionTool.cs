﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class InteractionTool : MonoBehaviour, ITool
{

    private List<ClickableObject> clickables;
    public SteamVR_ActionSet actionSet;

    public SteamVR_Action_Boolean interactionAction;
    [SerializeField] private HandScript handScript;
    public bool isPinching;



    // Start is called before the first frame update
    void Start()
    {
        clickables = new List<ClickableObject>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Pinch()
    {
        isPinching = true;
        ClickableObject clickable = GetNearestClickable();
        if (clickable != null)
            clickable.onClick.Invoke();
    }

    public void Unpinch()
    {
        isPinching = false;
    }


    public void Deequip()
    {
        gameObject.SetActive(false);
    }

    public void Equip()
    {
        gameObject.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        ClickableObject s = other.GetComponent<ClickableObject>();
        if (s != null)
            clickables.Add(s);

        Hoverable hover = other.GetComponent<Hoverable>();
        if (hover)
            hover.onEnter.Invoke();
    }
    private void OnTriggerExit(Collider other)
    {
        ClickableObject s = other.GetComponent<ClickableObject>();
        if (s != null)
            clickables.Remove(s);

        Hoverable hover = other.GetComponent<Hoverable>();
        if (hover)
            hover.onExit.Invoke();
    }

    private ClickableObject GetNearestClickable()
    {
        ClickableObject nearest = default;
        float minDist = float.MaxValue;
        float dist = 0;
        clickables.RemoveAll(item => (item == null || !item.gameObject.activeSelf || !GetComponent<Collider>().bounds.Intersects(item.GetComponent<Collider>().bounds)));
        foreach (ClickableObject i in clickables)
        {

            dist = (i.transform.position - transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                minDist = dist;
                nearest = i;
            }
        }
        return nearest;
    }

    public void Resize(int size)
    {
        transform.localScale = Vector3.one * size * 0.01f;
    }

}