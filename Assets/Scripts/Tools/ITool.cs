﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITool
{
    void Pinch();

    void Unpinch();

    void Resize(int size);

    void Equip();

    void Deequip();
}
