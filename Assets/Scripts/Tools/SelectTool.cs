﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class SelectTool : MonoBehaviour, ITool
{
    public SteamVR_ActionSet actionSet;

    public SteamVR_Action_Boolean selectingAction;

    [SerializeField] private HandScript handScript;

    private List<ISelectable> selectables;
    private bool isSelecting;
    private ISelectable currentSelectable;

    // Start is called before the first frame update
    void Start()
    {
        selectables = new List<ISelectable>();
    }

    // Update is called once per frame
    void Update()
    {


        if (isSelecting && currentSelectable!=null)
        {
            currentSelectable.Selecting(transform.position);
        }
    }

    public void Pinch()
    {
        isSelecting = true;
        currentSelectable = GetNearest();
    }

    public void Unpinch()
    {
        if (!isSelecting)
            return;
        isSelecting = false;
        if(currentSelectable != null)
        {
            currentSelectable.Deselect(handScript);
            currentSelectable = null;
        }   
    }

    private void OnTriggerEnter(Collider other)
    {
        ISelectable s = other.GetComponent<ISelectable>();
        if (s != null)
            selectables.Add(s);
    }
    private void OnTriggerExit(Collider other)
    {
        ISelectable s = other.GetComponent<ISelectable>();
        if (s != null)
            selectables.Remove(s);
    }

    private ISelectable GetNearest()
    {
        ISelectable nearest = null;
        float minDist = float.MaxValue;
        float dist = 0;
        foreach (ISelectable i in selectables)
        {
            dist = (i.GetTransform().position - transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                minDist = dist;
                nearest = i;
            }
        }
        return nearest;
    }

    public void Equip()
    {
        gameObject.SetActive(true);
    }

    public void Deequip()
    {
        gameObject.SetActive(false);
    }

    public void Resize(int size)
    {
        transform.localScale = Vector3.one * size * 0.01f;
    }


}
