﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatScript : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "BuckyBall")
        {

        }
        else if (collision.gameObject.GetComponent<Rigidbody>())
        {
            Rigidbody r = collision.gameObject.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
            foreach(Rigidbody cr in collision.gameObject.GetComponentsInChildren<Rigidbody>())
            {
                cr.useGravity = true;
                cr.isKinematic = false;
            }
            //collision.transform.DetachChildren();
        }
    }
}
