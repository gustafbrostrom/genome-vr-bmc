﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using TMPro;


public class BallScript : MonoBehaviour
{
    public GameObject bat;

    public GameObject display;

    private Vector3 start;
    private Vector3 stop;
    private float distance;

    private bool flying = false; 

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "bat")
        {
            start = collision.gameObject.transform.position;
            flying = true;
        }
        else if (flying)
        {
            if (collision.gameObject.name == "ExpandableFloor" || collision.gameObject.name == "FloorMesh")
            {
                stop = this.gameObject.transform.position;
                flying = false;
                displayScore();
            }
        }
    }

    private void displayScore()
    {
        distance = Vector3.Distance(start, stop);

        for (int i=1; i<=3; i++)
        {
            if (distance > PlayerPrefs.GetFloat(i.ToString()))
            {
                PlayerPrefs.DeleteKey(i.ToString());
                PlayerPrefs.Save();
                PlayerPrefs.SetFloat(i.ToString(), distance);
                PlayerPrefs.Save();
                break;
            }
        }




        display.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = PlayerPrefs.GetFloat("1").ToString() + "\n" + PlayerPrefs.GetFloat("2").ToString() + "\n" + PlayerPrefs.GetFloat("3").ToString() ;        
    }


    public void resetScore()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
        display.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = PlayerPrefs.GetFloat("1").ToString() + "\n" + PlayerPrefs.GetFloat("2").ToString() + "\n" + PlayerPrefs.GetFloat("3").ToString();

    }

}
