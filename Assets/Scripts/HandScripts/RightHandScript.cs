﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using TMPro;

public class RightHandScript : HandScript
{


    // Start is called before the first frame update
    protected override void Start()
    {
        id = 0;
        base.Start();
        otherhand = rm.leftHand; 
    }

    // Update is called once per frame
    protected override void Update()
    {

        base.Update();
        //Handle zooming and translating on the display
        if (isPressingTrackpad && otherhand.isPressingTrackpad && currentDisplay != null && currentDisplay == otherhand.currentDisplay)
        {
            currentDisplay.scaleLocations(otherhand.transform.position, transform.position);
        }
        else if (isPressingTrackpad && currentDisplay != null)
        {
            currentDisplay.translateLocationsWithRightHand(transform.position);
        }
        else if (otherhand.isPressingTrackpad && otherhand.currentDisplay != null)
        {
            otherhand.currentDisplay.translateLocationsWithLeftHand(otherhand.transform.position);
        }

        if(isPinching && otherhand.isPinching){
            CylinderDisplayHandler cdh = null;
            if(laserPointer.active){
                cdh = laserPointer.GetNearestWithScript<CylinderDisplayHandler>();
            }
            if(cdh == null){
                cdh = GetNearestWithScript<CylinderDisplayHandler>();
            }
            
            if (cdh != null){
                cdh.Zoom(interactionPoint.position, otherhand.interactionPoint.position);
            }
        }       

    }

}
