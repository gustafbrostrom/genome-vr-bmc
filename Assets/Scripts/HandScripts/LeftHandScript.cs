﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftHandScript : HandScript
{
    private float lastDist;
    private Vector3 a, b;
    // Start is called before the first frame update
    protected override void Start()
    {   
        id = 1;
        base.Start();
        otherhand = rm.rightHand;
        lastDist = float.NaN;
    }

    // Update is called once per frame
    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (currentGrabbable != null && GameObject.ReferenceEquals(currentGrabbable, otherhand.currentGrabbable))
        {

            Vector3 aa = interactionPoint.position;
            Vector3 bb = otherhand.interactionPoint.position;

            float dist = (aa - bb).magnitude;

            if (float.IsNaN(lastDist))
            {
                lastDist = dist;
                a = aa;
                b = bb;
                return;
            }

            float scale = dist / lastDist;
            lastDist = dist;

            Vector3 axis = -Vector3.Cross(bb - aa, b - a);
            float angle = Vector3.Angle(bb - aa, b - a);
            currentGrabbable.transform.RotateAround(aa, axis, angle);
            if (currentGrabbable.transform.localScale.magnitude > 0.0001 || scale > 1.0)
            {
                currentGrabbable.transform.localScale *= scale;
            }
            a = aa;
            b = bb;
        }
        else
        {
            lastDist = float.NaN;
        }

    }
}
