﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LaserPointer : MonoBehaviour
{
    LineRenderer line;
    float length = 0;
    float maxLength = 2f;
    public bool active = false;

    [SerializeField]
    Dictionary<string, GameObject> contactInteractables;

    Collider collider;
    [HideInInspector]    
    public Transform sphere;
    public RaycastHit hit;

    MeshRenderer sphereMesh;
    
    // Start is called before the first frame update
    void Start()
    {
        sphere = transform.Find("Sphere");
        collider = sphere.GetComponent<Collider>();
        sphereMesh = sphere.GetComponent<MeshRenderer>();
        sphereMesh.enabled = false;

        contactInteractables = new Dictionary<string, GameObject>();
        line = GetComponent<LineRenderer>();
        line.useWorldSpace = false;
        line.positionCount = 2;
        line.startWidth = 0.01f;
        line.endWidth = 0.001f;
        line.startColor = Color.red;
        line.endColor = Color.red;
        line.SetPositions(new Vector3[]{Vector3.zero, Vector3.zero});
        
    }

    public void Toggle()
    {
        active = !active;
        sphereMesh.enabled = active;

    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if(!active){

            if(length > 0){
                length -= 0.02f;
                line.SetPosition(1, Vector3.forward*length);
            }
            return;
        }

        if(length < maxLength)
            length += 0.02f;

        Vector3 point;
        if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit))
        {
            sphereMesh.enabled = true;

            point = transform.InverseTransformPoint(hit.point);
            
            sphere.transform.position  = hit.point;// + transform.forward*0.025f;
            line.startColor = Color.yellow;

            Collider[] hitColliders = Physics.OverlapSphere(sphere.position, 0.05f);
            foreach(Collider c in hitColliders){
                if(!contactInteractables.ContainsKey(c.name)){
                    contactInteractables.Add(c.name, c.gameObject);
                    Hoverable h = c.GetComponent<Hoverable>();
                    if (h != null)
                    {
                        h.onExit.Invoke();
                    }

                }
            }
        }
        else
        {
            line.startColor = Color.red;

            sphereMesh.enabled = false;

            point = Vector3.forward * length;

            foreach (GameObject go in contactInteractables.Values)
            {
                if(go == null)
                    continue;
                    
                Hoverable h = go.GetComponent<Hoverable>();
                if (h != null)
                {
                    h.onExit.Invoke();
                }
            }

            contactInteractables.Clear();
        }
        line.SetPosition(1, point);

    }


    public T GetNearestWithScript<T>()
    {
        T nearest = default;
        float minDist = float.MaxValue;
        float dist = 0;
        List<string> toRemove = new List<string>();
        if(contactInteractables == null)
            return nearest;

        foreach (KeyValuePair<string, GameObject> kvp in contactInteractables)
        {
            GameObject i = kvp.Value;
            if (i == null || !collider.bounds.Intersects(i.GetComponent<Collider>().bounds))
            {
                toRemove.Add(kvp.Key);
                continue;
            }

            T script = i.GetComponent<T>();
            if (script == null)
                continue;

            dist = (i.transform.position - transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                minDist = dist;
                nearest = script;
            }
        }
        foreach (string r in toRemove)
            contactInteractables.Remove(r);

        return nearest;
    }

    public GameObject GetNearest()
    {
        GameObject nearest = null;
        float minDist = float.MaxValue;
        float dist = 0;
        List<string> toRemove = new List<string>();
        foreach (KeyValuePair<string, GameObject> kvp in contactInteractables)
        {
            GameObject i = kvp.Value;
            if (!collider.bounds.Intersects(i.GetComponent<Collider>().bounds))
                toRemove.Add(kvp.Key);
            dist = (i.transform.position - transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                minDist = dist;
                nearest = i;
            }
        }
        foreach (string r in toRemove)
            contactInteractables.Remove(r);

        return nearest;
    }

    public GameObject GetNearestWithTag(string tag)
    {
        GameObject nearest = null;
        float minDist = float.MaxValue;
        float dist = 0;
        List<string> toRemove = new List<string>();
        foreach (KeyValuePair<string, GameObject> kvp in contactInteractables)
        {
            GameObject i = kvp.Value;
            if (!collider.bounds.Intersects(i.GetComponent<Collider>().bounds))
                toRemove.Add(kvp.Key);
            if (!i.CompareTag(tag))
                continue;
            dist = (i.transform.position - transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                minDist = dist;
                nearest = i;

            }
        }
        foreach (string r in toRemove)
            contactInteractables.Remove(r);

        return nearest;
    }
}
