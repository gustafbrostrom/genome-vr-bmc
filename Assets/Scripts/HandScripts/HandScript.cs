﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using TMPro;
using System;
using UnityEngine.Events;



public class HandScript : MonoBehaviour
{
    protected int id;
    public float collidersize = 0.05f;


    // Action sets 
    public SteamVR_ActionSet primarySet;
    public SteamVR_ActionSet toolSet;

    // Default action set 
    public ReferenceManager rm;
    public SteamVR_Action_Boolean menuPressAction = null;
    public SteamVR_Action_Boolean doubleMenuPressAction = null;

    public SteamVR_Action_Boolean trackPadPressAction = null;
    public SteamVR_Action_Boolean trackPadTouchAction = null;
    public SteamVR_Action_Vector2 trackPadPos = null;
    public SteamVR_Action_Boolean pinchAction = null;
    public SteamVR_Action_Boolean grabAction = null;

    public string hand;
    public HandScript otherhand;

    public bool isPinching = false;
    public bool isGrabbing = false;
    public bool isPressingTrackpad = false;

    private SteamVR_Behaviour_Pose pose = null;
    public ConfigurableJoint joint = null;

    protected GameObject currentInteractable = null;
    public List<GameObject> contactInteractables = new List<GameObject>();

    public DisplayHandler currentDisplay = null; //Used for translating and zooming

    public GameObject nearestInteraction;

    public GameObject currentGrabbable;
    public Vector3 grabOffset;
    private Quaternion grabAngularOffset;

    private Vector3 laserObjectPush;
    private CapsuleCollider handCollider;

    [SerializeField]
    public Menu menu;
    private bool menuToggled;

    public ITool currentTool;

    protected LaserPointer laserPointer;

    public Transform interactionPoint;

    private bool activeLaserPointerMemory = false;
    private ConfigurableJoint handjoint;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        laserPointer = GetComponentInChildren<LaserPointer>();
        rm = FindObjectOfType<ReferenceManager>();
        handCollider = GetComponent<CapsuleCollider>();
        pose = GetComponent<SteamVR_Behaviour_Pose>();
        handjoint = GetComponent<ConfigurableJoint>();
        menu.EquipTool(menu.interactionTool);
        interactionPoint = transform;
        joint = handjoint;
    }



    // Update is called once per frame
    protected virtual void Update()
    {
        if(laserPointer.active != activeLaserPointerMemory){
            if(laserPointer.active)
            {
                interactionPoint = laserPointer.sphere;
            }
            else
            {
                interactionPoint = transform;
            }
            activeLaserPointerMemory = laserPointer.active;
        }

        if(doubleMenuPressAction.GetLastStateDown(pose.inputSource))
            laserPointer.Toggle();

        if (grabAction.GetLastStateDown(pose.inputSource))
            GrabActionPress();

        if (grabAction.GetLastStateUp(pose.inputSource))
            grabActionRelease();

        if (pinchAction.GetLastStateDown(pose.inputSource))
        {
            pinch();
            //currentTool.Pinch();
        }

        if (pinchAction.GetLastStateUp(pose.inputSource))
        {
            unpinch();
            //currentTool.Unpinch();
        }

        if (trackPadPressAction.GetLastStateDown(pose.inputSource))
            trackPadPressDown();

        if (trackPadPressAction.GetLastStateUp(pose.inputSource))
            trackPadPressUp();

        if (menuPressAction.GetLastStateDown(pose.inputSource)){
            ToggleMenu();
            if(laserPointer.active){
                CylinderDisplayHandler cdh = laserPointer.GetNearestWithScript<CylinderDisplayHandler>();
                if(cdh != null)
                    menu.CustomMenu(cdh.Menu());
            }

        }


        if (menuToggled)
        {
            if (trackPadTouchAction.GetState(pose.inputSource))
                menu.Hover(trackPadPos.GetAxis(pose.inputSource));

            if (trackPadTouchAction.GetLastStateUp(pose.inputSource))
                menu.UnHover();

            if (pinchAction.GetLastStateDown(pose.inputSource) || trackPadPressAction.GetLastStateDown(pose.inputSource))
                menu.Click(trackPadPos.GetAxis(pose.inputSource));
        }
        if (isPinching && !otherhand.isPinching)
        {
            CylinderDisplayHandler cdh = null;
            if (laserPointer.active)
            {
                cdh = laserPointer.GetNearestWithScript<CylinderDisplayHandler>();
            }
            if (cdh == null)
            {
                cdh = GetNearestWithScript<CylinderDisplayHandler>();
            }

            if (cdh != null)
            {
                if (!otherhand.isPinching)
                {
                    cdh.Translation(interactionPoint.position, id);
                }
            }
        }



        ContinuousHover();
        if (isGrabbing && currentGrabbable != null)
        {

            if (!menuToggled && laserPointer.active && trackPadTouchAction.GetState(pose.inputSource))
            {
                Vector2 v = trackPadPos.GetAxisDelta(pose.inputSource);
                if (Math.Abs(v.y) < 0.1)
                {
                    float distToObject = Vector3.Distance(currentGrabbable.transform.position, transform.position);
                    laserObjectPush = transform.forward * v.y * (Mathf.Min(30, distToObject) + 1);
                }
            }
        }



    }


    protected virtual void FixedUpdate()
    {

        if (isGrabbing && currentGrabbable != null)
        {
            currentGrabbable.transform.position = interactionPoint.position - (currentGrabbable.transform.TransformPoint(grabOffset) - currentGrabbable.transform.position);
            if (!GameObject.ReferenceEquals(currentGrabbable, otherhand.currentGrabbable))
            {
                Quaternion q = grabAngularOffset*transform.rotation;
                currentGrabbable.transform.rotation = transform.rotation;
                currentGrabbable.transform.position += laserObjectPush;
                laserObjectPush = Vector3.zero;
            }
        }
    }

    private void ContinuousHover()
    {
    
        SlotComponent slotComponent = null;
        if (!laserPointer.active)
            slotComponent = GetNearestWithScript<SlotComponent>();

        if (slotComponent == null)
            slotComponent = laserPointer.GetNearestWithScript<SlotComponent>();

        if (slotComponent != null)
            slotComponent.Hover(interactionPoint.position);
    }

    private void ToggleMenu()
    {
        menuToggled = !menuToggled;
        menu.gameObject.SetActive(menuToggled);
    }

    private void OnTriggerEnter(Collider other)
    {
        contactInteractables.Add(other.gameObject);

        Hoverable h = other.GetComponent<Hoverable>();
        if (h != null)
            h.onEnter.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        CylinderDisplayHandler cdh = other.GetComponent<CylinderDisplayHandler>();
        if (cdh != null)
            cdh.ResetHand(id);
        contactInteractables.Remove(other.gameObject);

        Hoverable h = other.GetComponent<Hoverable>();
        if (h != null)
            h.onExit.Invoke();
    }


    private void GrabActionPress()
    {

        isGrabbing = true;
        //Null if laserpointer is inactive aswell
        if (laserPointer.active)
        {
            currentGrabbable = laserPointer.GetNearestWithTag("Grabbable");
        }
        if (currentGrabbable == null)
        {
            currentGrabbable = GetNearestWithTag("Grabbable");
        }

        if (currentGrabbable == null)
            return;

        grabOffset = currentGrabbable.transform.InverseTransformPoint(interactionPoint.position);
        grabAngularOffset = currentGrabbable.transform.rotation * Quaternion.Inverse(transform.rotation);
    }

    private void grabActionRelease()
    {
        isGrabbing = false;
        grabOffset = Vector3.zero;
        grabAngularOffset = Quaternion.identity;
        if (otherhand.currentGrabbable != null && GameObject.ReferenceEquals(currentGrabbable, otherhand.currentGrabbable))
        {
            otherhand.grabOffset = currentGrabbable.transform.InverseTransformPoint(otherhand.interactionPoint.position);
            otherhand.grabAngularOffset = Quaternion.identity;//currentGrabbable.transform.rotation.eulerAngles - otherhand.transform.rotation.eulerAngles;
        }
        currentGrabbable = null;
    }

    private void pinch()
    {
        isPinching = true;
        ClickableObject clickable;
        if (!laserPointer.active)
        {
            clickable = GetNearestWithScript<ClickableObject>();
        }
        else
        {
            clickable = laserPointer.GetNearestWithScript<ClickableObject>();
        }
        if (clickable != null)
            clickable.onClick.Invoke();

    }

    private void unpinch()
    {
        isPinching = false;
        CylinderDisplayHandler cdh;
        if (!laserPointer.active)
        {
            cdh = GetNearestWithScript<CylinderDisplayHandler>();
        }
        else
        {
            cdh = laserPointer.GetNearestWithScript<CylinderDisplayHandler>();
        }
        if (cdh != null)
        {
            cdh.ResetHand(id);
        }
    }

    private void trackPadPressDown()
    {
        isPressingTrackpad = true;
        GameObject rowHolder = GetNearestWithTag("GeneRowHolder");
        if (rowHolder)
        {
            currentDisplay = rowHolder.transform.parent.parent.GetComponent<DisplayHandler>();
        }
    }

    private void trackPadPressUp()
    {
        isPressingTrackpad = false;
        if (currentDisplay != null)
        {
            currentDisplay.resetOldHandPosData(hand);
            currentDisplay = null;
        }
    }

    protected T GetNearestWithScript<T>()
    {
        T nearest = default;
        float minDist = float.MaxValue;
        float dist = 0;
        contactInteractables.RemoveAll(item => (item == null || !item.activeSelf || !handCollider.bounds.Intersects(item.GetComponent<Collider>().bounds)));
        foreach (GameObject i in contactInteractables)
        {
            if (i.GetComponent<T>() == null)
                continue;

            dist = (i.transform.position - transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                minDist = dist;
                nearest = i.GetComponent<T>();
            }
        }
        return nearest;
    }

    private GameObject getNearest()
    {
        GameObject nearest = null;
        float minDist = float.MaxValue;
        float dist = 0;
        contactInteractables.RemoveAll(item => (item == null || !item.activeSelf || !handCollider.bounds.Intersects(item.GetComponent<Collider>().bounds)));
        foreach (GameObject i in contactInteractables)
        {
            dist = (i.transform.position - transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                minDist = dist;
                nearest = i;
            }
        }
        return nearest;
    }

    private GameObject GetNearestWithTag(string tag)
    {
        GameObject nearest = null;
        float minDist = float.MaxValue;
        float dist = 0;
        contactInteractables.RemoveAll(item => (item == null || !item.activeSelf || !handCollider.bounds.Intersects(item.GetComponent<Collider>().bounds)));
        foreach (GameObject i in contactInteractables)
        {
            if (!i.CompareTag(tag))
                continue;
            dist = (i.transform.position - transform.position).sqrMagnitude;
            if (dist < minDist)
            {
                minDist = dist;
                nearest = i;

            }
        }
        return nearest;
    }




}

