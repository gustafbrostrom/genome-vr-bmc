﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProteinBinding : IComparable<ProteinBinding>{

    public string name;
    public int l1;
    public int l2;

    public int mid;
    public string seq;
    public int num;
    public string sign;

    public ProteinBinding(string name, int l1, int l2, string seq, int num, string sign)
    {
        this.name = name;
        this.l1 = l1;
        this.l2 = l2;
        this.mid = (l1+l2)/2;
        this.seq = seq;
        this.num = num;
        this.sign = sign;
    }

    public ProteinBinding(string name, int l1, int l2)
    {
        this.name = name;
        this.l1 = l1;
        this.l2 = l2;
        this.mid = (l1+l2)/2;
    }

    public override string ToString()
    {
        return name + " " + l1 + " " + l2;
    }

    public int CompareTo(ProteinBinding other){
        return other.mid - this.mid;
    }
}
