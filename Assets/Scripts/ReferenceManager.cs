﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using IntervalTree;

public class ReferenceManager : MonoBehaviour
{
    public Sql database;
    public ChromosomeManager chromosomeManager;
    public Dictionary<string, int> chrNameToNumber = new Dictionary<string, int>();
    public Dictionary<string, int> chrNameToNumberM = new Dictionary<string, int>();
    public int[] humanChrLength = new int[] { 248956422, 242193529, 198295559, 190214555, 181538259, 170805979, 159345973, 145138636, 138394717, 133797422, 135086622, 133275309, 114364328, 107043718, 101991189, 90338345, 83257441, 80373285, 58617616, 64444167, 46709983, 50818468, 156040895, 57227415 };
    public List<string> numberToChrName = new List<string>();
    public List<string> numberToChrNameM= new List<string>();
    public static int numberOfHumanChromosomes = 24;
    public RightHandScript rightHand;
    public LeftHandScript leftHand;
    public Gradient gradient;
    private TextMeshPro printText;
    public GameObject cameraGO;
    public static int MaxSearchDepth = 100000;
    public Dictionary<string, GameObject> prefabs;
    private Dictionary<string, GeneDataHandler> referenceGenes;
    public ConnectionDataHandler connectionDataHandler;

    private Dictionary<string,BedReader> bedReaders;

    //Prefabs
    public GameObject SelectedRangeMenuPrefab;
    public GameObject chrOverviewPrefab;
    public GameObject interactionPrefab;
    public GameObject interactionDisplayPrefab;
    public GameObject RangeInformationDisplayPrefab;
    public GameObject LaserPrefab;
    public GameObject GameObjectPrefab;
    private Dictionary<string, ConnectionDataHandler> connectionData;

    private void Awake()
    {
        leftHand = FindObjectOfType<LeftHandScript>();
        rightHand = FindObjectOfType<RightHandScript>();

    }   

    public BedReader BedReader(string path){
        if(bedReaders == null)
            bedReaders = new Dictionary<string, BedReader>();

        if(!bedReaders.ContainsKey(path))
            bedReaders.Add(path, new BedReader(path));     

        return bedReaders[path];
    }

    public GeneDataHandler RefGene(string path){
        if(referenceGenes == null)
            referenceGenes = new Dictionary<string, GeneDataHandler>();

        if(!referenceGenes.ContainsKey(path))
            referenceGenes.Add(path, new GeneDataHandler(path));     

        return referenceGenes[path];
    }
    public ConnectionDataHandler ConnectionData(string path){
        if(connectionData == null)
            connectionData = new Dictionary<string, ConnectionDataHandler>();

        if(!connectionData.ContainsKey(path))
            connectionData.Add(path, new ConnectionDataHandler(path));     
            
        return connectionData[path];
    }

    public void print(string s){
        if(printText == null)
            printText = transform.Find("Print").GetComponent<TextMeshPro>();

        printText.text = s;
    }

    void OnValidate()
    {
        return;
        chrNameToNumber.Clear();
        if(chrNameToNumber.Count > 0)
            return;
        chrNameToNumber.Add("chr1", 0);
        chrNameToNumber.Add("chr2", 1);
        chrNameToNumber.Add("chr3", 2);
        chrNameToNumber.Add("chr4", 3);
        chrNameToNumber.Add("chr5", 4);
        chrNameToNumber.Add("chr6", 5);
        chrNameToNumber.Add("chr7", 6);
        chrNameToNumber.Add("chr8", 7);
        chrNameToNumber.Add("chr9", 8);
        chrNameToNumber.Add("chr10", 9);
        chrNameToNumber.Add("chr11", 10);
        chrNameToNumber.Add("chr12", 11);
        chrNameToNumber.Add("chr13", 12);
        chrNameToNumber.Add("chr14", 13);
        chrNameToNumber.Add("chr15", 14);
        chrNameToNumber.Add("chr16", 15);
        chrNameToNumber.Add("chr17", 16);
        chrNameToNumber.Add("chr18", 17);
        chrNameToNumber.Add("chr19", 18);
        chrNameToNumber.Add("chr20", 19);
        chrNameToNumber.Add("chr21", 20);
        chrNameToNumber.Add("chr22", 21);
        chrNameToNumber.Add("chrX", 22);
        chrNameToNumber.Add("chrY", 23);

        chrNameToNumberM.Add("chr1", 0);
        chrNameToNumberM.Add("chr2", 1);
        chrNameToNumberM.Add("chr3", 2);
        chrNameToNumberM.Add("chr4", 3);
        chrNameToNumberM.Add("chr5", 4);
        chrNameToNumberM.Add("chr6", 5);
        chrNameToNumberM.Add("chr7", 6);
        chrNameToNumberM.Add("chr8", 7);
        chrNameToNumberM.Add("chr9", 8);
        chrNameToNumberM.Add("chr10", 9);
        chrNameToNumberM.Add("chr11", 10);
        chrNameToNumberM.Add("chr12", 11);
        chrNameToNumberM.Add("chr13", 12);
        chrNameToNumberM.Add("chr14", 13);
        chrNameToNumberM.Add("chr15", 14);
        chrNameToNumberM.Add("chr16", 15);
        chrNameToNumberM.Add("chr17", 16);
        chrNameToNumberM.Add("chr18", 17);
        chrNameToNumberM.Add("chr19", 18);
        chrNameToNumberM.Add("chrX", 19);
        chrNameToNumberM.Add("chrY", 20);
        chrNameToNumberM.Add("chrM", 20);


        numberToChrName.Add("chr1");
        numberToChrName.Add("chr2");
        numberToChrName.Add("chr3");
        numberToChrName.Add("chr4");
        numberToChrName.Add("chr5");
        numberToChrName.Add("chr6");
        numberToChrName.Add("chr7");
        numberToChrName.Add("chr8");
        numberToChrName.Add("chr9");
        numberToChrName.Add("chr10");
        numberToChrName.Add("chr11");
        numberToChrName.Add("chr12");
        numberToChrName.Add("chr13");
        numberToChrName.Add("chr14");
        numberToChrName.Add("chr15");
        numberToChrName.Add("chr16");
        numberToChrName.Add("chr17");
        numberToChrName.Add("chr18");
        numberToChrName.Add("chr19");
        numberToChrName.Add("chr20");
        numberToChrName.Add("chr21");
        numberToChrName.Add("chr22");
        numberToChrName.Add("chrX");
        numberToChrName.Add("chrY");

        gradient = new Gradient();
        GradientColorKey[] colorKey;
        GradientAlphaKey[] alphaKey;
        // Populate the color keys at the relative time 0 and 1 (0 and 100%)
        colorKey = new GradientColorKey[3];
        colorKey[0].color = new Color(0.5f,1f,0.5f);
        colorKey[0].time = 0.0f;
        colorKey[1].color = Color.yellow;
        colorKey[1].time = 0.5f;
        colorKey[2].color = Color.red;
        colorKey[2].time = 1.0f;

        // Populate the alpha  keys at relative time 0 and 1  (0 and 100%)
        alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;

        gradient.SetKeys(colorKey, alphaKey);
    }
}
