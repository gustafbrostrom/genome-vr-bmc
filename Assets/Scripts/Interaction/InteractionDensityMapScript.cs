﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionDensityMapScript : MonoBehaviour
{
    public DisplayHandler displayHandler;
    private int resolution = 100;
    private int maxDensity = 10;
    public Image image;
    private Material material;
    private Texture2D tex; 
    // Start is called before the first frame update
    void Awake()
    {
        material = new Material(image.material);
        image.material = material;
        image.color = Color.white;
        tex = new Texture2D(resolution, 1);
        tex.filterMode = FilterMode.Point;
        material.mainTexture = tex;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void update(List<Interaction> interactions)
    {
        int[] density = new int[resolution];
        foreach(Interaction i in interactions)
        {
            int a = (int) (displayHandler.getLocationPercentage(i.midA) * (float) resolution);
            int b = (int) (displayHandler.getLocationPercentage(i.midB) * (float) resolution);
            if (a >= 0 && a < resolution)
                density[a] += 1;
            if (b >= 0 && b < resolution)
                density[b] += 1;
        }
        Color[] colorArray = new Color[resolution];
        for(int i = 0; i<resolution; i++)
        {
            float percentage = (float) density[i] / (float) maxDensity;
            if (percentage > 1)
                percentage = 1f;
            if(percentage!=0)
                colorArray[i] = Color.Lerp(Color.blue, Color.red, percentage * 0.8f + 0.2f); ;
        }
         tex.SetPixels(colorArray);
        tex.Apply();

    }

}
