﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(Texture2DHandler))]
public class ChromosomeOverviewCylinderScript : MonoBehaviour, ISelectable
{
    private ReferenceManager rm;
    public ChromosomeOverviewScript chromOverviewScript;
    public TextMeshPro tmp;

    private Texture2D tex;
    private int texHeight;
    private int texWidth = 1;

    private RightHandScript rightHand;
    private LeftHandScript leftHand;

    float oldLeftPercentage, oldRightPercentage;

    public ChromosomeMiniature chrMini;

    public int l1, l2;
    public int maxLoc;

    public Rigidbody myBody;

    public string chr;
    private Color[] currentColorArray;

    private int[] heatMap;
    private int max;

    private float lowerPercentage = -1f;
    private float upperPercentage = -1f;

    // Start is called before the first frame update
    public void passOnReference(ReferenceManager rm, ChromosomeOverviewScript script, int maxLoc, string chr,int texHeight)
    {
        this.rm = rm;
        chromOverviewScript = script;
        this.maxLoc = maxLoc;
        this.chr = chr;
        tmp.text = chr;
        this.texHeight = texHeight;

        tex = GetComponent<Texture2DHandler>().texture;

        currentColorArray = new Color[texHeight];
        for (int i = 0; i < texHeight; i++)
            currentColorArray[i] = Color.clear;

        resetTexture();
        heatMap = new int[texHeight];
        tex.filterMode = FilterMode.Point;
        GetComponent<Renderer>().material.mainTexture = tex;
    }

    void Start()
    {
        oldLeftPercentage = float.NegativeInfinity;
        oldRightPercentage = float.NegativeInfinity;
        myBody = GetComponent<Rigidbody>();
    }

    
    public void clearHeatmap()
    {
        heatMap = new int[texHeight];
        max = 0;
    }

    public void DisplayPointsOfInterest()
    {
        //List with all points of interest in the range displayed
        List<PointOfInterest> pois = rm.chromosomeManager.GetPointsOfInterest(chr);
        int id = GetInstanceID();
        foreach (PointOfInterest poi in pois)
        {
            //Outside
            if (poi.location < l1|| poi.location > l2)
            {
                if (poi.representations.ContainsKey(id))
                {
                    Destroy(poi.representations[id].gameObject);
                    poi.representations.Remove(id);
                }
                continue;
            }
            Transform t;
            if (!poi.representations.ContainsKey(id))
            {
                t = Instantiate(rm.GameObjectPrefab, transform).transform;
                poi.representations.Add(id, t);

            }
            else
            {
                t = poi.representations[id];
            }

            t.position = LocalVector3FromLocation(poi.location);//transform.TransformPoint(LocalVector3FromLocation(poi.location));

        }

    }

    public void addToHeatmap(float percentage)
    {
        heatMap[(int) (percentage * (texHeight - 1))]++;
        if(heatMap[(int)(percentage * (texHeight - 1))] > max)
        {
            max = heatMap[(int)(percentage * (texHeight - 1))];
        } 
    }

    public void drawHeatMap()
    {   
        
        Color[] colorArray = new Color[heatMap.Length];
        for(int i = 0; i<colorArray.Length; i++)
        {
            float percentage = (float)heatMap[i] / (float)max;
             //colorArray[i] = new Color(1, 0, 0, percentage);
            colorArray[i] = Color.Lerp(Color.clear, Color.red, percentage);
        }
        currentColorArray = colorArray;
        tex.SetPixels(colorArray);
        tex.Apply();
    }

    // Update is called once per frame
    void Update()
    {
        if (myBody.velocity.magnitude != 0)
            chromOverviewScript.drawLines(chr);

        if (rightHand && leftHand && rightHand.isPressingTrackpad && leftHand.isPressingTrackpad) //both
        {
            float leftPercentage = worldPosToPercentageAlongCylinder(leftHand.transform.position);
            float rightPercentage = worldPosToPercentageAlongCylinder(rightHand.transform.position);
            if (oldLeftPercentage != float.NegativeInfinity && oldRightPercentage != float.NegativeInfinity)
            {
                float scale = Mathf.Abs(rightPercentage - leftPercentage) / Mathf.Abs(oldRightPercentage - oldLeftPercentage);
                float oldMid = (oldRightPercentage + oldLeftPercentage) / 2;
                float mid = (rightPercentage + leftPercentage) / 2;
                int transDist = (int)((mid - oldMid) * (l2 - l1));
                l1 += transDist;
                l2 += transDist;
                if(l1<0 || l2 > maxLoc)
                {
                    l1 -= transDist;
                    l2 -= transDist;
                }
                //if (l1 < 0)
                //    l1 = 0;
                //if (l2 > maxLoc)
                //    l2 = maxLoc;

                //then scale
                int t1 = (int)(mid * (l2 - l1));
                int left = (int)(t1 * scale) - t1;
                int t2 = (int)((1 - mid) * (l2 - l1));
                int right = (int)(t2 * scale) - t2;
                l1 += left;
                l2 -= right;
                if (l1 < 0)
                    l1 = 0;
                if (l2 > maxLoc)
                    l2 = maxLoc;
                chromOverviewScript.updateLists(chr);
                chromOverviewScript.drawLines(chr);
                chrMini.updateLocations(l1, l2);
            }
            oldLeftPercentage = leftPercentage;
            oldRightPercentage = rightPercentage;

        }
        else if(leftHand && leftHand.isPressingTrackpad) //left
        {
            float leftPercentage = worldPosToPercentageAlongCylinder(leftHand.transform.position);
            if (oldLeftPercentage != float.NegativeInfinity)
            {
                float diff = leftPercentage - oldLeftPercentage;
                int numberOfBases = (int)((float)(l2 - l1) * diff);
                l1 -= numberOfBases;
                l2 -= numberOfBases;
                //if (l1 < 0)
                //    l1 = 0;
                //if (l2 > maxLoc)
                //    l2 = maxLoc;
                if (l1 < 0 || l2 > maxLoc)
                {
                    l1 += numberOfBases;
                    l2 += numberOfBases;
                }
                chromOverviewScript.updateLists(chr);
                chromOverviewScript.drawLines(chr);
                chrMini.updateLocations(l1, l2);
            }
            oldLeftPercentage = leftPercentage;
        }else if (rightHand && rightHand.isPressingTrackpad) //right
        {
            float rightPercentage = worldPosToPercentageAlongCylinder(rightHand.transform.position);
            if (oldRightPercentage != float.NegativeInfinity)
            {
                float diff = rightPercentage - oldRightPercentage;
                int numberOfBases = (int)((float)(l2 - l1) * diff);
                l1 -= numberOfBases;
                l2 -= numberOfBases;
                //if (l1 < 0)
                //    l1 = 0;
                //if (l2 > maxLoc)
                //    l2 = maxLoc;
                if (l1 < 0 || l2 > maxLoc)
                {
                    l1 += numberOfBases;
                    l2 += numberOfBases;
                }
                chromOverviewScript.updateLists(chr);
                chromOverviewScript.drawLines(chr);
                chrMini.updateLocations(l1, l2);
            }
            oldRightPercentage = rightPercentage;
        }

        if(rightHand && !rightHand.isPressingTrackpad)
            oldRightPercentage = float.NegativeInfinity;
        if (leftHand && !leftHand.isPressingTrackpad)
            oldLeftPercentage  = float.NegativeInfinity;
    }

    public void updateTexture(Color[] colorArray)
    {
        currentColorArray = colorArray;
        tex.SetPixels(colorArray);
        tex.Apply();
    }

    public void resetTexture()
    {
        tex.SetPixels(currentColorArray);
        tex.Apply();
    }

    public float getLocationPercentage(int l)
    {
        float a = (float)(l - l1);
        float b = (float)(l2 - l1);
        return a / b;
    }

    public float worldPosToPercentageAlongCylinder(Vector3 pos)
    {
        Vector3 tempUp = chromOverviewScript.transform.InverseTransformDirection(transform.up);

        pos = transform.parent.transform.InverseTransformPoint(pos);
        Vector3 topPos = transform.localPosition + (tempUp * transform.localScale.y);
        Vector3 bottomPos = transform.localPosition + (-tempUp * transform.localScale.y);
        Vector3 dirVec = topPos - bottomPos;
        Vector3 point = Vector3.Project(pos - bottomPos, dirVec);
        return point.magnitude / dirVec.magnitude;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Controller (left)")
            leftHand = other.GetComponent<LeftHandScript>();
        if (other.gameObject.name == "Controller (right)")
            rightHand = other.GetComponent<RightHandScript>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Controller (left)")
        {
            leftHand = null;
            oldLeftPercentage = float.NegativeInfinity;
        }
        if (other.gameObject.name == "Controller (right)")
        {
            rightHand = null;
            oldRightPercentage = float.NegativeInfinity;
        }
    }

    public void Selecting(Vector3 toolPos)
    {
        float newPercentage = WorldPosToCylinderPercentage(toolPos);
        if (lowerPercentage == -1f)
        {
            lowerPercentage = newPercentage;
            upperPercentage = newPercentage;
        }
        else
        {
            int x1 = 0;
            int x2 = 0;
            if (newPercentage < lowerPercentage)
            {
                x1 = TexCoordFromPercentage(newPercentage);
                x2 = TexCoordFromPercentage(lowerPercentage);
                lowerPercentage = newPercentage;
            }
            else if (newPercentage > upperPercentage)
            {
                x1 = TexCoordFromPercentage(upperPercentage);
                x2 = TexCoordFromPercentage(newPercentage);
                upperPercentage = newPercentage;
            }
            for (int i = x1; i < x2; i++)
            {
                for (int j = 0; j < tex.width; j++)
                {
                    tex.SetPixel(j, i, Color.blue);
                }
            }
            tex.Apply();
        }
    }

    public void Deselect(HandScript handScript)
    {
        if (lowerPercentage != -1 && upperPercentage != -1)
        {
            SelectedRangeMenuScript menu = Instantiate(rm.SelectedRangeMenuPrefab,rm.cameraGO.transform.TransformPoint(Vector3.forward*0.4f),Quaternion.identity).GetComponent<SelectedRangeMenuScript>();
            menu.transform.LookAt(rm.cameraGO.transform);
            menu.transform.Rotate(new Vector3(0, 180, 0));
            menu.Initialize(chr, LocationFromPercentage(lowerPercentage), LocationFromPercentage(upperPercentage));

            
            menu.closeDisplay.onClick.AddListener(() => resetTexture());
            menu.closeDisplay.onClick.AddListener(() => handScript.menu.EquipTool(handScript.menu.selectTool));
            handScript.menu.EquipTool(handScript.menu.interactionTool);
        }
        lowerPercentage = -1;
        upperPercentage = -1;
    }

    private int LocationFromPercentage(float percentage)
    {
        return l1 + (int) ((l2 - l1) * percentage);
    }

    private int TexCoordFromPercentage(float percentage)
    {
        return (int)(percentage * (float)tex.height);
    }

    private float WorldPosToCylinderPercentage(Vector3 pos)
    {
        Vector3 dir = CylinderTop() - CylinderBottom();
        Vector3 dir2 = pos - CylinderBottom();
        Vector3 v = Vector3.Project(dir2, dir);

        return Mathf.Clamp01(v.magnitude / dir.magnitude);
    }

    private Vector3 LocalVector3FromLocation(int location)
    {
        return Vector3.Lerp(CylinderTop(), CylinderBottom(), (float)location / (float)(l2 - l1));
    }

    private Vector3 CylinderTop()
    {
        return transform.TransformPoint((Vector3.up * this.gameObject.transform.localScale.y));
    }

    private Vector3 CylinderBottom()
    {
        return transform.TransformPoint((-Vector3.up * this.gameObject.transform.localScale.y));
    }

    public Transform GetTransform()
    {
        return transform;
    }
}
