﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ChromosomeOverviewScript : MonoBehaviour
{

    private ReferenceManager rm;
    private Dictionary<string,List<Interaction>> interactions;
    private DisplayHandler displayHandler;

    //Prefabas
    public GameObject chromosomeOverviewCylinderPrefab;

    //Collider for moving the whole thing
    private CapsuleCollider collider;

    //Data
    private string mainChr;
    private int mainChrInt;

    //The outer cylinders
    private ChromosomeOverviewCylinderScript[] cylinders;
    private ChromosomeOverviewCylinderScript mainCylinder;

    private int resolutionOfHeatMap = 100; //The resolution of the texture of the heatmap;
    private bool[] split;

    private Stack<InteractionHandler> deadPool;
    private Dictionary<string, List<InteractionHandler>> interactionsUsedByChromosome;

    private Dictionary<string, int[]> heatmap;

    public TextMeshPro outgoingInteractions;

    void Awake()
    { 
        //Get handles
        collider = GetComponent<CapsuleCollider>();
        rm = GameObject.Find("ReferenceManager").GetComponent<ReferenceManager>();

        //Pooling
        deadPool = new Stack<InteractionHandler>();
        interactionsUsedByChromosome = new Dictionary<string, List<InteractionHandler>>();
        heatmap = new Dictionary<string, int[]>();
    }

    public void setup(DisplayHandler dis, string chr)
    {
        this.displayHandler = dis;
        this.mainChr = chr;
        this.mainChrInt = rm.chrNameToNumber[chr];
        this.interactions = rm.chromosomeManager.GetInterChromosomalInteractions(chr);

        outgoingInteractions.text = "Reset";
        //init setup
        cylinders = new ChromosomeOverviewCylinderScript[rm.humanChrLength.Length];
        for (int i = 0; i < cylinders.Length; i++)
        {
            GameObject go = Instantiate(chromosomeOverviewCylinderPrefab, transform, false);
            ChromosomeOverviewCylinderScript chrOverviewCylinderScript = go.GetComponent<ChromosomeOverviewCylinderScript>();
            chrOverviewCylinderScript.passOnReference(rm,this, rm.humanChrLength[i], rm.numberToChrName[i], resolutionOfHeatMap);
            chrOverviewCylinderScript.chrMini.setAttributes(rm.numberToChrName[i], rm);

            //Pos, rot and scale of outer cylinders
            cylinders[i] = chrOverviewCylinderScript;
            float y = (float)rm.humanChrLength[i] / (float)200000000;
            cylinders[i].transform.localScale = new Vector3(0.1f, y, 0.1f);
            cylinders[i].transform.localPosition = new Vector3(1.5f * Mathf.Cos(i * (Mathf.PI / 24)), 0, 1.5f * Mathf.Sin(i * (Mathf.PI / 24)));


            cylinders[i].transform.LookAt(transform, transform.up);
            cylinders[i].l1 = 0;
            cylinders[i].l2 = rm.humanChrLength[i];
            cylinders[i].chrMini.updateLocations(0, rm.humanChrLength[i]);

        }
        mainCylinder = cylinders[rm.chrNameToNumber[chr]];
        mainCylinder.transform.localPosition = Vector3.zero;
        mainCylinder.tag = "Untagged";
        collider.height = mainCylinder.transform.localScale.y * 2;
        
        split = new bool[cylinders.Length];
        completeUpdate();
    }

    public void updateInteractionsSearchDepth(int newSearchDepth)
    {
        foreach (string c in rm.chrNameToNumber.Keys)
        {
            if (c == mainChr || !interactions.ContainsKey(c))
                continue;

            bool first = true;
            foreach(Interaction interaction in interactions[c])
            {
                if (first)
                {
                    if (newSearchDepth < interaction.SearchDepth)
                    {
                        rm.print("Less");
                    }
                    else
                    {
                        rm.print("More");
                    }
                    first = false;
                }
                if (newSearchDepth < interaction.SearchDepth)
                {
                    interaction.SearchDepth = newSearchDepth;
                    interaction.removeGenesOutsideRanges();
                }
                else
                {
                    int a1, a2, a3, a4, b1, b2, b3, b4;
                    a1 = interaction.midA - newSearchDepth;
                    a2 = interaction.midA - interaction.SearchDepth;
                    a3 = interaction.midA + interaction.SearchDepth; 
                    a4 = interaction.midA + newSearchDepth;

                    b1 = interaction.midB - newSearchDepth;
                    b2 = interaction.midB - interaction.SearchDepth;
                    b3 = interaction.midB + interaction.SearchDepth;
                    b4 = interaction.midB + newSearchDepth;


                    foreach (Gene g in rm.chromosomeManager.GetGenesInRange(interaction.chrA, a1, a2))
                        interaction.addGeneToRangeA(g);

                    foreach (Gene g in rm.chromosomeManager.GetGenesInRange(interaction.chrA, a3, a4))
                        interaction.addGeneToRangeA(g);

                    foreach (Gene g in rm.chromosomeManager.GetGenesInRange(interaction.chrB, b1, b2))
                        interaction.addGeneToRangeB(g);

                    foreach (Gene g in rm.chromosomeManager.GetGenesInRange(interaction.chrB, b3, b4))
                        interaction.addGeneToRangeB(g);


                    interaction.SearchDepth = newSearchDepth;

                }
             }
        }
        completeUpdate();
    }

    public void hardReset()
    {
        //fysiska grejjer
        for (int i = 0; i < cylinders.Length; i++)
        {
            float y = (float)rm.humanChrLength[i] / (float)200000000;
            cylinders[i].transform.localScale = new Vector3(0.1f, y, 0.1f);
            cylinders[i].transform.localPosition = new Vector3(1.5f * Mathf.Cos(i * (Mathf.PI / 24)), 0, 1.5f * Mathf.Sin(i * (Mathf.PI / 24)));
            cylinders[i].transform.LookAt(transform, transform.up);
            cylinders[i].GetComponent<Rigidbody>().isKinematic = true;

        }
        mainCylinder.transform.localPosition = Vector3.zero;

        //unsplitta alla nät
        for (int i = 0; i < split.Length; i++)
        {
            if (split[i])
            {
                split[i] = false;
                if (rm.numberToChrName[i] != mainChr)
                {
                    updateLists(rm.numberToChrName[i]);
                }
            }
            drawLines(rm.numberToChrName[i]);

        }

    }

    public void completeUpdate()
    {
        //Draw lines
        foreach (string c in rm.chrNameToNumber.Keys)
        {
            if (c != mainChr)
            {
                updateLists(c);
                drawLines(c);
            }
        }
    }

    public void setChrLocations(string chr, int l1, int l2)
    {
        int chrint = rm.chrNameToNumber[chr];
        cylinders[chrint].l1 = l1;
        cylinders[chrint].l2 = l2;
        cylinders[chrint].chrMini.updateLocations(l1, l2);
    }

    //Uppdatera interactions och heatmap samtidigt
    public void updateLists(string chr)
    {

        if (chr == this.mainChr)
        { //If scrolled on the main chromosome update the locations on the display
            if (displayHandler)
                displayHandler.updateLocations(mainCylinder.l1, mainCylinder.l2);

            mainCylinder.DisplayPointsOfInterest();
            drawMainCylinderHeatMap();
            return;
        }
        if (!interactions.ContainsKey(chr))
            return;

        if (!interactionsUsedByChromosome.ContainsKey(chr))
            interactionsUsedByChromosome.Add(chr, new List<InteractionHandler>());

        int totalNumberOfConnectionsInRange = 0;
        int chrint = rm.chrNameToNumber[chr];


        //Update Points Of Interests
        cylinders[chrint].DisplayPointsOfInterest();


        if (!heatmap.ContainsKey(chr))
            heatmap.Add(chr, new int[resolutionOfHeatMap]);
        else
            heatmap[chr] = new int[resolutionOfHeatMap];

        cylinders[chrint].clearHeatmap();

        List<Interaction> currentInteractions = new List<Interaction>();
        foreach (Interaction inter in interactions[chr])
        {
            //In range on main chromosome
            if (inter.midA < mainCylinder.l1 || inter.midA > mainCylinder.l2)
                continue;

            //In range on other chromosome
            if (inter.midB < cylinders[chrint].l1 || inter.midB > cylinders[chrint].l2)
                continue;

            //if (inter.overLappingGenesRangeA == null || inter.overLappingGenesRangeB == null || inter.overLappingGenesRangeA.Count == 0 || inter.overLappingGenesRangeB.Count == 0)
            //    continue;

            //Add to heatmap
            heatmap[chr][(int)(mainCylinder.getLocationPercentage(inter.midA) * (resolutionOfHeatMap - 1))]++;
            cylinders[chrint].addToHeatmap(cylinders[chrint].getLocationPercentage(inter.midB));

            totalNumberOfConnectionsInRange++;
            currentInteractions.Add(inter);
        }

        //Draw the heatmaps 
        
        cylinders[chrint].drawHeatMap();
        drawMainCylinderHeatMap();

        //Dont allow split view if there are too many interactions
        if (totalNumberOfConnectionsInRange > 500)
            split[chrint] = false;
 
        //Update the interactionsHandlers lists accordingly
        int counter = 0;
        if (split[chrint])
        {
            foreach (Interaction inter in currentInteractions)
            {
                InteractionHandler handler = getInteractionHandler(chr, counter++);
                handler.startPercentage = mainCylinder.getLocationPercentage(inter.midA);
                handler.endPercentage = cylinders[chrint].getLocationPercentage(inter.midB);
                int send = chrint;
                handler.interaction = inter;
                handler.transform.Find("Collider").GetComponent<ClickableObject>().onClick.AddListener(handler.spawnInfoDisplay);
            }
        }
        else
        {
            InteractionHandler handler = getInteractionHandler(chr, counter++);
            handler.numberOfConnections = totalNumberOfConnectionsInRange;
            int send = rm.chrNameToNumber[chr];
            handler.transform.Find("Collider").GetComponent<ClickableObject>().onClick.AddListener(() => toggleSplit(send));
        }

        //Return unused handlers to deadpool
        if (counter < interactionsUsedByChromosome[chr].Count)
        {
            for (int i = interactionsUsedByChromosome[chr].Count - 1; i >= counter; i--)
            {
                InteractionHandler handler = interactionsUsedByChromosome[chr][i];
                interactionsUsedByChromosome[chr].RemoveAt(i);
                handler.gameObject.SetActive(false);
                handler.reset();
                deadPool.Push(handler);
            }
        }
    }

    private InteractionHandler getInteractionHandler(string chr, int counter)
    {
        InteractionHandler handler;
        if (counter < interactionsUsedByChromosome[chr].Count)
        {
            handler = interactionsUsedByChromosome[chr][counter];
            handler.reset();
        }
        else if (deadPool.Count > 0)
        {
            handler = deadPool.Pop();
            handler.gameObject.SetActive(true);
            interactionsUsedByChromosome[chr].Add(handler);
        }
        else
        {
            GameObject go = Instantiate(rm.interactionPrefab, transform, false);
            handler = go.GetComponent<InteractionHandler>();
            handler.rm = rm;
            interactionsUsedByChromosome[chr].Add(handler);
        }
        return handler;
    }

    public void drawMainCylinderHeatMap()
    {
         //Main cylinder heatmap
        float max = 0;
        Color[] colorArray = new Color[resolutionOfHeatMap];
        int[] temp = new int[resolutionOfHeatMap];
        for (int i = 0; i < resolutionOfHeatMap; i++)
        {
            foreach (int[] arr in heatmap.Values)
            {
                temp[i] += arr[i];
            }
            if (temp[i] > max)
                max = temp[i];
        }

        for (int i = 0; i < colorArray.Length; i++)
        {
            float percentage = (float)temp[i] / max;
            //colorArray[i] = new Color(1, 0, 0, percentage);
            colorArray[i] = Color.Lerp(Color.clear, Color.red, percentage);

        }
        mainCylinder.updateTexture(colorArray);
    }

    //Flyttar ett object
    public void drawLines(string chr)
    {




        if (!interactions.ContainsKey(chr))
            return;

        if (!rm.chrNameToNumber.ContainsKey(chr))
            return;

        Vector3 mainChromosomeStartPos = mainCylinder.transform.localPosition + (-Vector3.up * mainCylinder.transform.localScale.y);
        Vector3 mainChromosomeEndPos = mainCylinder.transform.localPosition + (Vector3.up * mainCylinder.transform.localScale.y);

        int i = rm.chrNameToNumber[chr];
        Vector3 tempUp = transform.InverseTransformDirection(cylinders[i].transform.up);
        Vector3 otherChromosomeStartPos = cylinders[i].transform.localPosition + (-tempUp * cylinders[i].transform.localScale.y);
        Vector3 otherChromosomeEndPos = cylinders[i].transform.localPosition + (tempUp * cylinders[i].transform.localScale.y);

        Vector3 anchorA;
        Vector3 anchorB;

        


        if (split[rm.chrNameToNumber[chr]])
        {
            foreach (InteractionHandler inter in interactionsUsedByChromosome[chr])
            {
                float percentageA = inter.startPercentage;
                float percentageB = inter.endPercentage;

                anchorA = Vector3.Lerp(mainChromosomeStartPos, mainChromosomeEndPos, percentageA);
                anchorB = Vector3.Lerp(otherChromosomeStartPos, otherChromosomeEndPos, percentageB);
                inter.setLineWithCollider(anchorA, anchorB, transform.up);
            }
        }
        else
        {
            if(interactionsUsedByChromosome[chr].Count > 0)
            {
                anchorA = Vector3.Lerp(mainChromosomeStartPos, mainChromosomeEndPos, 0.5f);
                anchorB = Vector3.Lerp(otherChromosomeStartPos, otherChromosomeEndPos, 0.5f);
                InteractionHandler inter = interactionsUsedByChromosome[chr][0];
                inter.setLineWithCollider(anchorA, anchorB, transform.up);
                inter.setMidText();

            }
        }
    }

    public void updateLocations(int l1, int l2)
    {
        mainCylinder.l1 = l1;
        mainCylinder.l2 = l2;
        mainCylinder.chrMini.updateLocations(l1, 12);
    }

    public void toggleSplit(int i)
    {
        split[i] = !split[i];
        updateLists(rm.numberToChrName[i]);
        drawLines(rm.numberToChrName[i]);
    }
}
