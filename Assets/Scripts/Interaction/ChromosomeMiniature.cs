﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChromosomeMiniature : MonoBehaviour
{
    private int location1, location2;
    private int texHeight = 1000;
    private int texWidth = 1;
    private Texture2D tex;
    private string chr;
    [SerializeField]
    private ReferenceManager rm;
    private int length;


    void Awake()
    {
        tex = new Texture2D(texWidth, texHeight);
        tex.filterMode = FilterMode.Point;
        GetComponent<Renderer>().material.mainTexture = tex;
    }

    public void setAttributes(string chr, ReferenceManager rm)
    {
        this.chr = chr;
        this.rm = rm;
        this.length = rm.humanChrLength[rm.chrNameToNumber[chr]];
        float y = 0.1f;
        transform.localScale = new Vector3(0.5f, y, 0.5f);
    }

    public void updateLocations(int l1, int l2)
    {
        location1 = l1;
        location2 = l2;
        float a = (float)location1 / (float)length;
        float b = (float)location2 / (float)length;
        updateTexture(a, b);
    }


    private void updateTexture(float a, float b)
    {
        Color[] colorArray = new Color[texWidth * texHeight];
        for (int i = texHeight-1; i >=0; i--)
        {
            if (i > a * texHeight && i < b * texHeight)
            {
                colorArray[i] = Color.red;
            }
            else
            {
                colorArray[i] = Color.white;
            }
        }
        tex.SetPixels(colorArray);
        tex.Apply();
    }
}
