﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.Events;

public class InteractionHandler : MonoBehaviour
{
    public ReferenceManager rm;
    public LineRenderer lineRenderer;
    private int lengthOfLineRenderer = 21;
    public TextMeshPro textA, textB, textMid;
    public CapsuleCollider capsuleCollider;
    private Vector3[] points;
    public float width;
    public int numberOfConnections;

    public float startPercentage, endPercentage;

    public Interaction interaction;
    private Hoverable hoverScript;


    void Awake()
    {
        hoverScript = capsuleCollider.GetComponent<Hoverable>();
        hoverScript.onEnter = Enter;
        hoverScript.onExit = Exit;

        textA.text = "";
        textB.text = "";
        textMid.text = "";

        capsuleCollider.enabled = false;
        lineRenderer.positionCount = lengthOfLineRenderer;
        lineRenderer.useWorldSpace = false;
        lineRenderer.widthMultiplier = 0.001f;
        setColor(Color.red);
    }

    public void spawnInfoDisplay()
    {
        InteractionDisplay ID = Instantiate(rm.interactionDisplayPrefab, capsuleCollider.transform.position, capsuleCollider.transform.rotation, transform.parent).GetComponent<InteractionDisplay>();
        ID.transform.LookAt(rm.cameraGO.transform);
        ID.transform.Rotate(new Vector3(0, 180, 0));
        ID.interaction = this.interaction;
        ID.newSearchDepth(0);
        ID.fillInfo();
    }

    private void Enter()
    {
        setColor(Color.green);
        if (interaction != null)
            textMid.text = string.Format("{0:#,0}", interaction.midA) + " -> " + string.Format("{0:#,0}", interaction.midB);
    }

    private void Exit()
    {
        setColor(Color.red);
        if (interaction != null)
            textMid.text = "";

    }

    public void reset()
    {
        textA.text = "";
        textB.text = "";
        textMid.text = "";

        capsuleCollider.GetComponent<ClickableObject>().onClick.RemoveAllListeners();
        capsuleCollider.enabled = false;
        lineRenderer.positionCount = lengthOfLineRenderer;
        lineRenderer.useWorldSpace = false;
        lineRenderer.widthMultiplier = 0.001f;
        setColor(Color.red);

        interaction = null;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setColor(Color color)
    {
        float alpha = 1.0f;
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(color, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
        );
        lineRenderer.colorGradient = gradient;
    }

    public void setLineWithCollider(Vector3 a, Vector3 b, Vector3 up)
    {
        points = new Vector3[lengthOfLineRenderer];
        for (int i = 0; i < lengthOfLineRenderer; i++)
        {
            Vector3 p = Vector3.Lerp(a, b, (float)i / (float)(lengthOfLineRenderer - 1));
            points[i] = p;
        }
        lineRenderer.SetPositions(points);

        capsuleCollider.enabled = true;
        capsuleCollider.transform.localPosition = points[points.Length / 2];
        capsuleCollider.transform.localRotation = Quaternion.LookRotation(b-a);

        lineRenderer.SetPositions(points);
    }

    public void setCurvedLine(Vector3 a, Vector3 b, float length)
    {
        points = new Vector3[lengthOfLineRenderer];
        for (int i = 0; i < lengthOfLineRenderer; i++)
        {
            Vector3 p = Vector3.Lerp(a, b, (float)i / (float)(lengthOfLineRenderer - 1));
            p += length * (Vector3.up) * Mathf.Sin(((float)i / (float)(lengthOfLineRenderer - 1)) * Mathf.PI);
            points[i] = p;
        }
        textA.gameObject.SetActive(false);
        textB.gameObject.SetActive(false);
        lineRenderer.SetPositions(points);
    }

    public void setCurvedLineWithText(Vector3 a, Vector3 b, float length, string ta, string tb)
    {
        points = new Vector3[lengthOfLineRenderer];
        for (int i = 0; i < lengthOfLineRenderer; i++)
        {
            Vector3 p = Vector3.Lerp(a, b, (float)i / (float)(lengthOfLineRenderer - 1));
            p += length * (Vector3.up) * Mathf.Sin(((float)i / (float)(lengthOfLineRenderer - 1)) * Mathf.PI);
            points[i] = p;
        }
        if (textA.gameObject.activeSelf == false)
            textA.gameObject.SetActive(true);
        if (textB.gameObject.activeSelf == false)
            textB.gameObject.SetActive(true);
        textA.transform.localPosition = points[0];
        textB.transform.localPosition = points[points.Length - 1];

        setTextforAnchorA(ta);
        setTextforAnchorB(tb);
        lineRenderer.SetPositions(points);
    }

    public void setMidText(bool setNumber = true)
    {
        if (textMid.gameObject.activeSelf == false)
            textMid.gameObject.SetActive(true);
        string t = "";
        if (numberOfConnections > 10000)
            t = string.Format("{0:#,0, K}", numberOfConnections);
        else
            t = string.Format("{0:#,0}", numberOfConnections);


        textMid.text = setNumber? t:"";
    }

    public void setTextforAnchorA(string s)
    {
        textA.transform.localPosition = points[1];
        textA.text = s;
    }

    public void setTextforAnchorB(string s)
    {
        textB.text = s;
    }

    public void setWidth(float f)
    {
        AnimationCurve curve = new AnimationCurve();
        curve.AddKey(0.0f, 0.1f);
        curve.AddKey(0.5f, 1f);
        curve.AddKey(1.0f, 0.1f);
        lineRenderer.widthCurve = curve;
        lineRenderer.widthMultiplier = f;
    }
}
