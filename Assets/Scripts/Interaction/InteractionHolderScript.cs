﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionHolderScript : MonoBehaviour
{
    public DisplayHandler displayHandler;
    public GameObject interactionPrefab;
    public RectTransform rect;
    
    private Vector3[] corners = new Vector3[4];
    private int counter = 0;

    private List<GameObject> interactionPool = new List<GameObject>();

    public void updateCorners()
    {
        rect.GetLocalCorners(corners);
        counter = 0;
    }

    public void displayInteraction(Interaction i)
    {
        GameObject go = getInteraction(counter);
        InteractionHandler handler = go.GetComponent<InteractionHandler>();
        float a = displayHandler.getLocationPercentage(i.midA);
        float b = displayHandler.getLocationPercentage(i.midB);
        Vector3 anchorA = Vector3.Lerp(corners[0], corners[3], a);
        Vector3 anchorB = Vector3.Lerp(corners[0], corners[3], b);
        Gene geneA = null; //displayHandler.rm.chromosomeManager.getClosestGene(i.chrA, i.midA);
        Gene geneB = null; //displayHandler.rm.chromosomeManager.getClosestGene(i.chrB, i.midB);
        string textA = (geneA != null) ? geneA.name : "none";
        string textB = (geneB != null) ? geneB.name : "none";
        float height = (rect.rect.height * 0.9f) * (Mathf.Abs(b - a) * 0.8f + 0.2f);
        handler.setCurvedLineWithText(anchorA, anchorB, height, textA, textB);
        counter++;
    }

    private GameObject getInteraction(int index)
    {
        GameObject newInteraction;
        if (index < interactionPool.Count)
        {
            newInteraction = interactionPool[index];
            if (newInteraction.activeSelf == false)
            {
                newInteraction.SetActive(true);
            }
        }
        else
        {
            interactionPool.Add(Instantiate(interactionPrefab, transform, false));
            newInteraction = interactionPool[index];
        }
        newInteraction.GetComponent<InteractionHandler>().reset();
        return newInteraction;
    }

    public void deactivateUnusedInteractions()
    {
        for (int i = counter; i < interactionPool.Count; i++)
        {
            interactionPool[i].SetActive(false);
        }
    }

    private void deactivateUnusedInteractions(int index)
    {
        for (int i = index; i < interactionPool.Count; i++)
        {
            interactionPool[i].SetActive(false);
        }
    }
}
