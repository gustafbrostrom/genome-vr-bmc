﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;

public class InteractionDisplay : MonoBehaviour
{
    public ReferenceManager rm;
    public Interaction interaction;
    private Text[] textMeshes;
    private ChromosomeOverviewScript parentOverview;
    private int searchDepth;
    
    // Start is called before the first frame update
    void Awake()
    {
        searchDepth = 0;
        rm = FindObjectOfType<ReferenceManager>();
        textMeshes = GetComponentsInChildren<Text>();
        Transform parent = transform.parent;
        if(parent != null)
            parentOverview = parent.GetComponent<ChromosomeOverviewScript>();
        print(parent + " " + parentOverview);

    }


    public void newSearchDepth(Single value)
    {
        int newSearchDepth = (int) (value * 100000);
        if (newSearchDepth < interaction.SearchDepth)
        {
            interaction.SearchDepth = newSearchDepth;
            interaction.removeGenesOutsideRanges();
        }
        else
        {
            int a1, a2, a3, a4, b1, b2, b3, b4;
            a1 = interaction.midA - newSearchDepth;
            a2 = interaction.midA - interaction.SearchDepth;
            a3 = interaction.midA + interaction.SearchDepth;
            a4 = interaction.midA + newSearchDepth;

            b1 = interaction.midB - newSearchDepth;
            b2 = interaction.midB - interaction.SearchDepth;
            b3 = interaction.midB + interaction.SearchDepth;
            b4 = interaction.midB + newSearchDepth;

            foreach (Gene g in rm.chromosomeManager.GetGenesInRange(interaction.chrA, a1, a2))
                interaction.addGeneToRangeA(g);

            foreach (Gene g in rm.chromosomeManager.GetGenesInRange(interaction.chrA, a3, a4))
                interaction.addGeneToRangeA(g);

            foreach (Gene g in rm.chromosomeManager.GetGenesInRange(interaction.chrB, b1, b2))
                interaction.addGeneToRangeB(g);

            foreach (Gene g in rm.chromosomeManager.GetGenesInRange(interaction.chrB, b3, b4))
                interaction.addGeneToRangeB(g);

            //IEnumerable<Gene> tempGenes;
            //tempGenes = rm.chromosomeManager.getGenesInRange(interaction.chrA, a1, a2);
            //if (tempGenes != null)
            //{
            //    foreach (Gene g in tempGenes)
            //        interaction.addGeneToRangeA(g);
            //}
            //tempGenes = rm.chromosomeManager.getGenesInRange(interaction.chrA, a3, a4);
            //if (tempGenes != null)
            //{
            //    foreach (Gene g in tempGenes)
            //        interaction.addGeneToRangeA(g);
            //}
            //tempGenes = rm.chromosomeManager.getGenesInRange(interaction.chrB, b1, b2);
            //if (tempGenes != null)
            //{
            //    foreach (Gene g in tempGenes)
            //        interaction.addGeneToRangeB(g);
            //}
            //tempGenes = rm.chromosomeManager.getGenesInRange(interaction.chrB, b3, b4);
            //if (tempGenes != null)
            //{
            //    foreach (Gene g in tempGenes)
            //        interaction.addGeneToRangeB(g);
            //}

            interaction.SearchDepth = newSearchDepth;
        }
        searchDepth = newSearchDepth;

        fillInfo();
    }

    public void fillInfo()
    {
        textMeshes[0].text = interaction.signA + " " + interaction.chrA + " -> " + interaction.chrB + " " + interaction.signB;
        textMeshes[0].text  += string.Format("{0:#,0}", interaction.midA) +" -> " + string.Format("{0:#,0}", interaction.midB);


        textMeshes[1].text = "Search: " + searchDepth + "\n";
        textMeshes[2].text = "Search: " + searchDepth + "\n";
        foreach (Gene g in interaction.overLappingGenesRangeA)
            textMeshes[1].text += g.name + "\n";
        foreach (Gene g in interaction.overLappingGenesRangeB)
            textMeshes[2].text += g.name + "\n";

        textMeshes[3].text = interaction.chrA;
        textMeshes[4].text = interaction.chrB;

        if (interaction.chrA == interaction.chrB)
            GameObject.Find("ShowInteraction").SetActive(false);

    }

    public void spawnRange(int i)
    {
        if (i == 0)
            rm.chromosomeManager.addDisplay(interaction.chrA, interaction.midA - 10000, interaction.midA + 10000);
        else
            rm.chromosomeManager.addDisplay(interaction.chrB, interaction.midB - 10000, interaction.midB + 10000);
    }

    public void showInteraction()
    {
        if (parentOverview == null)
        {
            parentOverview = Instantiate(rm.chrOverviewPrefab, rm.rightHand.transform.position, Quaternion.identity).GetComponent<ChromosomeOverviewScript>();
            parentOverview.transform.LookAt(rm.cameraGO.transform);
            parentOverview.transform.Rotate(new Vector3(0, 180, 0));
            parentOverview.transform.localScale = Vector3.one * 0.1f;
            parentOverview.setup(null, interaction.chrA);
            transform.parent = parentOverview.transform;
        }

        parentOverview.setChrLocations(interaction.chrB, interaction.midB - 10000, interaction.midB + 10000);
        parentOverview.updateLocations(interaction.midA - 10000, interaction.midA + 10000);

        parentOverview.completeUpdate();
        parentOverview.updateLists(interaction.chrA);
    }

    public void killDisplay()
    {
        Destroy(this.gameObject);
    }

}
