﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class SelectedRangeMenuScript : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI tmp;
    private ReferenceManager rm;
    public ClickableObject closeDisplay;
    public ClickableObject button1;
    public ClickableObject button2;
    private string chr;
    private int l1, l2;
    private void Start()
    {
        rm = GameObject.Find("ReferenceManager").GetComponent<ReferenceManager>();
        closeDisplay.onClick.AddListener(() => Destroy(gameObject));
        button2.onClick.AddListener(SpawnRangeInformation);
        button1.onClick.AddListener(() => rm.chromosomeManager.addDisplay(chr, l1, l2));
    }

    private void SpawnRangeInformation()
    {
        print("Spawn Range information");
        RangeInformation rangeInfo = new RangeInformation(chr, l1, l2, rm);

        GameObject gem = Instantiate(rm.RangeInformationDisplayPrefab, rm.cameraGO.transform.TransformPoint(Vector3.forward * 0.4f), Quaternion.identity);

        gem.transform.LookAt(rm.cameraGO.transform);
        gem.transform.Rotate(new Vector3(0, 180, 0));
        gem.GetComponent<RangeInformationDisplay>().rangeInformation = rangeInfo;
        gem.GetComponent<RangeInformationDisplay>().FillInfo(rm);
    }

    public void Initialize(string chr, int l1, int l2)
    {
        this.chr = chr;
        this.l1 = l1;
        this.l2 = l2;
        tmp.text = chr + ": " + string.Format("{0:#,0}", l1) + " - " + string.Format("{0:#,0}", l2);


    }

}
