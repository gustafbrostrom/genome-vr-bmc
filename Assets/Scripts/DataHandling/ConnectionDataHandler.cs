﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using IntervalTree;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class ConnectionDataHandler
{
    BinaryFormatter binaryFormatter;
    Dictionary<string, IntervalTree<int, Interaction>> interactionTree;
    string filename;

    public ConnectionDataHandler(string fileName)
    {
        this.filename = fileName;
        interactionTree = new Dictionary<string, IntervalTree<int, Interaction>>();
        binaryFormatter = new BinaryFormatter();
    }

    public IntervalTree<int, Interaction> GetInteractionTree(string chr)
    {
        if (interactionTree.ContainsKey(chr))
            return interactionTree[chr];

        if (Directory.Exists("Data\\" + filename + ".ser"))
        {
            Stream stream = File.Open("Data\\" + filename + ".ser" + "\\" + chr + ".tree", FileMode.Open);
            interactionTree.Add(chr, (IntervalTree<int, Interaction>)binaryFormatter.Deserialize(stream));
            stream.Close();
            return interactionTree[chr];
        }
        else
        {
            BuildTree();
            return interactionTree[chr];
        }

    }

    private void BuildTree()
    {
        
        foreach (string line in File.ReadAllLines(@"Data\" + filename))
        {
            string[] words = line.Split();
            string AChr = words[0];
            int AStart = int.Parse(words[1]);
            int AEnd = int.Parse(words[2]);
            string BChr = words[3];
            int BStart = int.Parse(words[4]);
            int BEnd = int.Parse(words[5]);
            float score = 10f;
            int AMid = (AStart + AEnd) / 2;
            int BMid = (BStart + BEnd) / 2;
            string dir = words[7];
            Interaction conn = new Interaction(AChr,BChr,AStart,AEnd,BStart,BEnd,score);

            if (!interactionTree.ContainsKey(AChr))
                interactionTree.Add(AChr, new IntervalTree<int, Interaction>());
            interactionTree[AChr].Add(AStart, AEnd, conn);

            if (!interactionTree.ContainsKey(BChr))
                interactionTree.Add(BChr, new IntervalTree<int, Interaction>());
            interactionTree[BChr].Add(BStart, BEnd, conn);
        }
        Directory.CreateDirectory("Data\\" + filename + ".ser");
        Stream stream;
        foreach (KeyValuePair<string, IntervalTree<int, Interaction>> keyValuePair in interactionTree)
        {
            stream = File.Open("Data\\" + filename + ".ser" + "\\" + keyValuePair.Key + ".tree", FileMode.Create);
            binaryFormatter.Serialize(stream, keyValuePair.Value);
            stream.Close();
        }
    }
}
