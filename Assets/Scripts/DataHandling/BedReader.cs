using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using IntervalTree;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class BedReader
{
    BinaryFormatter binaryFormatter;
    Dictionary<string, IntervalTree<int, Element>> bedTree;
    string filename;

    public BedReader(string fileName)
    {
        binaryFormatter = new BinaryFormatter();

        this.filename = fileName;
        bedTree = new Dictionary<string, IntervalTree<int, Element>>();
    }

    public IntervalTree<int, Element> GetTree(string chr)
    {

        if (bedTree.ContainsKey(chr))
            return bedTree[chr];

        if (Directory.Exists("Data\\" + filename + ".ser"))
        {
            Stream stream = File.Open("Data\\" + filename + ".ser" + "\\" + chr + ".tree", FileMode.Open);
            bedTree.Add(chr,(IntervalTree<int, Element>)binaryFormatter.Deserialize(stream));
            stream.Close();
            return bedTree[chr];
        }
        else
        {
            BuildTree();
            return bedTree[chr];
        }
    }

    private void BuildTree()
    {
        foreach (string line in File.ReadAllLines(@"Data\" + filename))
        {
        
            string[] words = line.Split();
            string chr = words[0];
            int l1 = int.Parse(words[1]);
            int l2 = int.Parse(words[2]);
            string tag = words[3];
            if (!bedTree.ContainsKey(chr))
                bedTree.Add(chr, new IntervalTree<int, Element>());

            Element e = new Element(chr, l1, l2, tag);
            bedTree[chr].Add(l1,l2,e);
        }

        Directory.CreateDirectory("Data\\" + filename + ".ser");
        Stream stream;
        foreach (KeyValuePair<string, IntervalTree<int, Element>> keyValuePair in bedTree)
        {
            stream = File.Open("Data\\" + filename + ".ser" + "\\" + keyValuePair.Key + ".tree", FileMode.Create);
            binaryFormatter.Serialize(stream, keyValuePair.Value);
            stream.Close();

            
        }
    }

    public override string ToString(){
        return "BigReader("+filename+")";
    }

}
