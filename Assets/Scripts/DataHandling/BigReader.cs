﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using Newtonsoft.Json;
using System.Threading.Tasks;
public class BigReader
{
    Process p;
    StreamWriter writer;
    StreamReader reader;
    Dictionary<string, int> chromosomes;
    public BigReader(string filename)
    {
        p = new Process();
        ProcessStartInfo startInfo = new ProcessStartInfo();
        startInfo.UseShellExecute = false;
        startInfo.RedirectStandardInput = true;
        startInfo.RedirectStandardOutput = true;
        startInfo.RedirectStandardError = true;
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        startInfo.CreateNoWindow = false;

        startInfo.FileName = "bash";
        p.StartInfo = startInfo;
        p.Start();
 
        writer = p.StandardInput;
        writer.AutoFlush = true;
        reader = p.StandardOutput;
        filename = "Data/" + filename;
        writer.Write("python read.py " + filename + "\n");
        writer.Write("bw.chroms()\n");
        chromosomes = JsonConvert.DeserializeObject<Dictionary<string,int>>(reader.ReadLine());
    }

    public void CloseConnection()
    {
        p.Close();
    }

    ~BigReader(){
        CloseConnection();
    }


    public List<float> ReadValues(string chr, int l1, int l2){
        if(!chromosomes.ContainsKey(chr))
            throw new System.ArgumentException("File does not contain chromosome. \nAvailable chromosomes are " + string.Join(",", chromosomes.Keys.ToArray()), "chr=" + chr);
        
        l1 = Math.Max(l1, 0);
        l2 = Math.Min(l2, chromosomes[chr]);
        writer.Write(String.Format("bw.values(\"{0}\", {1}, {2})\n", chr, l1, l2));
        return JsonConvert.DeserializeObject<List<float>>(reader.ReadLine());

    }
    
    public List<float> ReadStats(string chr, int l1, int l2, int bins){
        if(!chromosomes.ContainsKey(chr))
            throw new System.ArgumentException("File does not contain chromosome. \nAvailable chromosomes are " + string.Join(",", chromosomes.Keys.ToArray()), "chr=" + chr);

        if(l2 <= l1)
        {
            l2 = l1 + 1;
        }

        if(l1 < 0)
        {
            l1 = 0;
            
        }
        l2 = Math.Max(1,l2);
        l2 = Math.Min(l2, chromosomes[chr]);

        writer.Write(String.Format("bw.stats(\"{0}\", {1}, {2}, nBins={3})\n", chr, l1, l2, bins));
        List<float> stats = JsonConvert.DeserializeObject<List<float>>(reader.ReadLine());
        return stats;
    }

    public T Read<T>(string pythonString){
        writer.Write(pythonString + "\n");
        return JsonConvert.DeserializeObject<T>(reader.ReadLine());
    }

    public string Read(string pythonString){
        writer.Write(pythonString + "\n");
        return reader.ReadLine();
    }

    public List<string> ReadFromBigWig(string input)
    {
        writer.Write(input + "\n");
        writer.Flush();
        string output = reader.ReadLine();
        output = output.Substring(1, output.Length - 2);
        UnityEngine.Debug.Log(output);
        return output.Split(',').ToList();
    }

    public List<string> ReadFromBigWig(string filename, string chr, int l1, int l2, int bins)
    {
        string args = filename + " " + chr + " " + l1 + " " + l2 + " " + bins + " ";
        string input = "bw.stats('" + chr + "'," + l1 + "," + l2 + ",nBins=" + bins + ")";
        writer.Write(input + "\n");
        writer.Flush();
        string output = reader.ReadLine();
        output = output.Substring(1, output.Length - 2);
        UnityEngine.Debug.Log(output);
        return output.Split(',').ToList();
    }

}
