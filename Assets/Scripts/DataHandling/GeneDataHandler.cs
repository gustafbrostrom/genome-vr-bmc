﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using IntervalTree;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


public class GeneDataHandler
{
    BinaryFormatter binaryFormatter;
    Dictionary<string, IntervalTree<int, GeneV2>> geneTree;
    Dictionary<string, List<GeneV2>> geneDict;
    string filename;

    private static Color[] colorWheel = new Color[]{Color.blue, Color.cyan, Color.red, Color.green, Color.yellow, Color.magenta};

    // Start is called before the first frame update
    public GeneDataHandler(string filename)
    {
        binaryFormatter = new BinaryFormatter();
        this.filename = filename;
        geneTree = new Dictionary<string, IntervalTree<int, GeneV2>>();
        geneDict = new Dictionary<string, List<GeneV2>>();
    }

    public List<GeneV2> GetGenes(string gene)
    {
        if (geneDict.ContainsKey(gene))
            return geneDict[gene];

        if (Directory.Exists("Data\\" + filename + ".ser"))
        {
            Stream stream = File.Open("Data\\" + filename + ".ser" + "\\genes.dict", FileMode.Open);
            geneDict = (Dictionary<string, List<GeneV2>>) binaryFormatter.Deserialize(stream);
            stream.Close();
            return geneDict[gene];
        }
        else
        {
            BuildTree();
            return geneDict[gene];
        }

    }

    public IntervalTree<int, GeneV2> GetGeneTree(string chr)
    {
        if (geneTree.ContainsKey(chr))
            return geneTree[chr];

        if (Directory.Exists("Data\\" + filename + ".ser"))
        {
            Stream stream = File.Open("Data\\" + filename + ".ser" + "\\" + chr + ".tree", FileMode.Open);
            geneTree.Add(chr,(IntervalTree<int, GeneV2>)binaryFormatter.Deserialize(stream));
            stream.Close();
            return geneTree[chr];
        }
        else
        {
            BuildTree();
            return geneTree[chr];
        }
        
    }

    private string getStringBetweenCharachters(string word, string c)
    {
        
        int pFrom = word.IndexOf(c)+1;
        int pTo = word.LastIndexOf(c);

        return word.Substring(pFrom, pTo - pFrom);
    }

    private void BuildTree()
    {
        GeneV2 g = new GeneV2();
        Exon exon;
        foreach (string line in File.ReadAllLines(@"Data\" + filename))
        {
            string[] words = line.Split();
            if (words[2] == "transcript")
            {
                string chr = words[0];
                string name = getStringBetweenCharachters(words[14], "\"");
                int dir = words[6] == "+" ? 1 : -1;
                int l1 = int.Parse(words[3]);
                int l2 = int.Parse(words[4]);
                string transcript_id = words[10];

                Color color = colorWheel[((l1 + l2)/2) % colorWheel.Length];
                

                g = new GeneV2(name, chr, l1, l2, dir, color);

                if (!geneTree.ContainsKey(chr))
                    geneTree.Add(chr, new IntervalTree<int, GeneV2>());

                geneTree[chr].Add(l1, l2, g);
                if (!geneDict.ContainsKey(name))
                    geneDict[name] = new List<GeneV2>();
                geneDict[name].Add(g);
            }
            else if (words[2] == "exon")
            {
                string id = getStringBetweenCharachters(words[15], "\"");
                int l1 = int.Parse(words[3]);
                int l2 = int.Parse(words[4]);
                int dir = words[6] == "+" ? 1 : -1;
                string name = getStringBetweenCharachters(words[17], "\"");

                exon = new Exon(id, l1, l2, dir);
                if (g.Name == name)
                {
                    g.Exons.Add(exon);
                }
                else
                {
                    Debug.Log("nått är fuffens med " + name);
                }

            }
        }
        Directory.CreateDirectory("Data\\" + filename + ".ser");
        Stream stream;
        foreach (KeyValuePair<string, IntervalTree<int, GeneV2>> keyValuePair in geneTree)
        {
            stream = File.Open("Data\\" + filename + ".ser" + "\\" + keyValuePair.Key + ".tree", FileMode.Create);
            binaryFormatter.Serialize(stream, keyValuePair.Value);
            stream.Close();

            
        }
        stream = File.Open("Data\\" + filename + ".ser" + "\\" + "genes.dict", FileMode.Create);
        binaryFormatter.Serialize(stream, geneDict);
        stream.Close();
    }
}
