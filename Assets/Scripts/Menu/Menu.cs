﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;
using System.IO;


public class Menu : MonoBehaviour
{
    [SerializeField]
    private GameObject MenuItemPrefab;
    private List<MenuItemScript> menuItemScripts;
    private int currentSize;
    private Dictionary<string, dynamic> currentMenu;
    private int currentHover = -1;
    private Color standardColor;
    public Color hoverColor;
    private Image image;
    private Dictionary<string, Dictionary<string, dynamic>> menus;
    private ReferenceManager rm;
    
    private Object dataSpherePrefab;
    [SerializeField]private HandScript hand;
    

    private ITool currentTool;

    public SelectTool selectTool;
    public InteractionTool interactionTool;

    private LaserPointer laserPointer;

    private void OnValidate()
    {
        //hand = transform.parent.GetComponent<HandScript>();
    }

    // Start is called before the first frame update
    void Awake()
    {
        if(menus != null)
            return;

        dataSpherePrefab = Resources.Load("DataSphere", typeof(GameObject));
        laserPointer = transform.parent.GetComponentInChildren<LaserPointer>();
        //hand = transform.parent.GetComponent<HandScript>();
        rm = GameObject.Find("ReferenceManager").GetComponent<ReferenceManager>();
        menus = new Dictionary<string, Dictionary<string, dynamic>>();
        image = GetComponent<Image>();
        standardColor = image.color;
        menuItemScripts = new List<MenuItemScript>();
        CreateAllMenus();
        ChangeCurrentMenu("Main");

    }

    public void CustomMenu(Dictionary<string, dynamic> customMenu, string name = "Most Recent \n Object")
    {
        Dictionary<string, dynamic> mainMenu = new Dictionary<string, dynamic>();

        UnityEvent e;

        foreach(KeyValuePair<string, dynamic> kvp in customMenu)
        {
            //SUBMENU
            System.Type t = kvp.Value.GetType();
            if(t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Dictionary<,>))
            {
                if(!menus.ContainsKey(kvp.Key))
                    menus.Add(kvp.Key, kvp.Value);
                else
                    menus[kvp.Key] = kvp.Value;


                e = new UnityEvent();
                e.AddListener(() => ChangeCurrentMenu(kvp.Key));
                mainMenu.Add(kvp.Key, e);
            }
            else
            {
                mainMenu.Add(kvp.Key, kvp.Value);
            }
        }

        if(!menus.ContainsKey(name))
        {
            menus.Add(name, mainMenu); 
            e = new UnityEvent();
            e.AddListener(() => ChangeCurrentMenu(name));
            menus["Main"].Add(name, e);
        }
        else
        {
            menus[name] = mainMenu;
        }

        ChangeCurrentMenu(name);
    }

    private void ChangeCurrentMenu(string key)
    {
        if (key == "Main" && currentMenu == menus["Main"])
        {
            gameObject.SetActive(false);
            return;
        }

        currentMenu = menus[key];
        ClearMenu();
        Populate(currentMenu);
        currentHover = -1;
    }

    private void ClearMenu()
    {
        menuItemScripts.ForEach((MenuItemScript m) => Destroy(m.gameObject));
        menuItemScripts.Clear();
    }

    private void Populate(Dictionary<string, dynamic> menu)
    {
        int size = menu.Count;
        currentSize = size;
        int i = 0;
        foreach(string key in menu.Keys)
        {
            MenuItemScript neu = Instantiate(MenuItemPrefab, transform).GetComponent<MenuItemScript>();
            neu.transform.Rotate(0,0,-i * 360f / size - (360f/size - 360f/(size+1))/2);
            Image image = neu.GetComponent<Image>();
            float fillAmount = 1f / (size + 1);
            image.fillAmount = fillAmount;
            neu.Text = key;
            neu.textMesh.transform.localPosition += new Vector3(-45*Mathf.Sin(fillAmount * Mathf.PI), 45*Mathf.Sin(fillAmount * Mathf.PI), 0);
            neu.textMesh.transform.Rotate(0, 0, i * 360f / size + (360f / size - 360f / (size + 1)) / 2);
            
            if(menu[key].GetType() == typeof(UnityEventFloat))
            {
                neu.onChange = menu[key];
            }
            else
            {
                neu.onClick = menu[key];
            }
            menuItemScripts.Add(neu);
            i++;
        }
    }

    private Vector2 ToPolarCoordinates(Vector2 input)
    {

        float theta = 0f;
        if(input.x > Mathf.Abs(input.y))
        {
            theta = Mathf.Asin(input.y);

        }
        else if (input.y >= Mathf.Abs(input.x))
        {

            theta = Mathf.Acos(input.x);

        }
        else if ((-input.x) > Mathf.Abs(input.y))
        {

            theta = Mathf.Asin(-input.y);
            theta += Mathf.PI;

        }
        else if((-input.y)>= Mathf.Abs(input.x))
        {

            theta = Mathf.Acos(-input.x);
            theta += Mathf.PI;

        }
        theta = 2*Mathf.PI-theta;
        theta += 3*Mathf.PI / 2;
        theta = theta % (Mathf.PI * 2);

        return new Vector2(Mathf.Sqrt(Mathf.Pow(input.x, 2f) + Mathf.Pow(input.y, 2f)), theta);
    }

    private int IndexOf(Vector2 input)
    {
        if (input.x < 0.5)
            return 0;


        return Mathf.FloorToInt((input.y / (2 * Mathf.PI)) * currentSize) + 1;
    }

    public void UnHover()
    {
        //If we were already not hovering
        if (currentHover != -1)
        {
            if (currentHover > 0)
                menuItemScripts[currentHover - 1].UnHover();
            else
                image.color = standardColor;

            currentHover = -1;
        }
    }

    public void Hover(Vector2 input)
    {
        if(image == null)
            return;
            
        input = ToPolarCoordinates(input);
        int index = IndexOf(input);

        if (index != currentHover)// if hovering different index
        {
            //Deselect Previous
            if (currentHover != -1) //if hovering before
            {
                if (currentHover > 0) //if we were not hovering center
                    menuItemScripts[currentHover - 1].UnHover();
                else //if we were hovering center before
                    image.color = standardColor;
            }

            //Select new 
            if (index > 0) //if not hovering center
                menuItemScripts[index-1].Hover();
            else
                image.color = hoverColor;

            currentHover = index;
        }

        if (currentHover > 0){//if not hovering center
            if(menuItemScripts[currentHover - 1].onChange != null)
            {
                float itemsize = 2*Mathf.PI/(currentSize + 1);
                float per = input.y - (currentHover - 1)*itemsize - itemsize/(currentSize + 1);
                per /= itemsize;
                per = Mathf.Clamp01(per);
                menuItemScripts[currentHover - 1].Hover(per, 1f/(currentSize + 1));
            }
        } 
    }

    public void Click(Vector2 input)
    {
        input = ToPolarCoordinates(input);
        int index = IndexOf(input);
        if (index > 0)
        {
            if(!menuItemScripts[index - 1].range)
                menuItemScripts[index - 1].onClick.Invoke();
            
        }
        else
        {
            ChangeCurrentMenu("Main");
        }
    }

    private void CreateAllMenus()
    {
        MainMenu();
        ChromsomeMenu();
        ToolSize();
        ChangeToolMenu();
        DataFiles();
    }

    public void EquipTool(ITool tool)
    {
        if(hand.currentTool!= null)
            hand.currentTool.Deequip();
        hand.currentTool = tool;
        tool.Equip();
    }

    private void ChangeToolSize(int size)
    {
        hand.currentTool.Resize(size);
        //hand.changeColliderSize(0.001f + 0.01f * size * size);
    }

    private void ChromsomeMenu()
    {
        Dictionary<string, dynamic> menu = new Dictionary<string, dynamic>();
        foreach (string key in rm.chrNameToNumber.Keys)
        {
            UnityEvent e = new UnityEvent();
            e.AddListener(() => rm.chromosomeManager.addDisplay(key, transform.position, transform.rotation));
            menu.Add(key, e);
        }
        menus.Add("Chromosomes", menu);
    }

    private void ToolSize()
    {
        Dictionary<string, dynamic> menu = new Dictionary<string, dynamic>();
        for(int i = 0; i < 4; i++)
        {
            UnityEvent e = new UnityEvent();
            int d = i;
            e.AddListener(() => ChangeToolSize(d));
            menu.Add((i + 1).ToString(), e);
        }
        menus.Add("Tool Size", menu);
    }

    private void DataFiles()
    {
        Dictionary<string, dynamic> menu = new Dictionary<string, dynamic>();
        string[] filenames = Directory.GetFiles(Directory.GetCurrentDirectory() + "/Data");
        
        foreach(string filename in filenames)
        {
            
            string ffilename = filename.Split('\\').Last();
            UnityEvent e = new UnityEvent();

            e.AddListener(() =>
            {
                GameObject go = Instantiate(dataSpherePrefab, transform.position + 0.1f * transform.forward, Quaternion.identity) as GameObject;
                DataSphere newDataSphere = go.GetComponent<DataSphere>();

                newDataSphere.Filepath = ffilename;
            });
            menu.Add(ffilename, e);

        }


        menus.Add("Data Files", menu);
    }

    private void ChangeToolMenu()
    {
        Dictionary<string, dynamic> menu = new Dictionary<string, dynamic>();

        UnityEvent e = new UnityEvent();
        e.AddListener(() => EquipTool(selectTool));
        menu.Add("Select Tool", e);

        e = new UnityEvent();
        e.AddListener(() => EquipTool(interactionTool));
        menu.Add("Interaction Tool", e);

        menus.Add("Change Tool", menu);
    }


    private void MainMenu()
    {
        Dictionary<string, dynamic> menu = new Dictionary<string, dynamic>();
        UnityEvent e = new UnityEvent();
        e.AddListener(() => ChangeCurrentMenu("Chromosomes"));
        menu.Add("Chromosomes", e);

        e = new UnityEvent();
        e.AddListener(() => ChangeCurrentMenu("Tool Size"));
        menu.Add("Tool Size", e);

        e = new UnityEvent();
        e.AddListener(() => ChangeCurrentMenu("Change Tool"));
        menu.Add("Change Tool", e);

        e = new UnityEvent();
        e.AddListener(() => ChangeCurrentMenu("Data Files"));
        menu.Add("Data Files", e);

        e = new UnityEvent();
        e.AddListener(() => rm.chromosomeManager.ClearAllDisplays());
        menu.Add("Clear All Displays", e);

        CylinderDisplayHandler cdh = GameObject.FindObjectOfType<CylinderDisplayHandler>();

        e = new UnityEvent();
        e.AddListener(() => cdh.DebugMove());
        menu.Add("M", e);

        e = new UnityEvent();
        e.AddListener(() => laserPointer.Toggle());
        menu.Add("Laser Pointer", e);

        menus.Add("Main", menu);
    }


}
