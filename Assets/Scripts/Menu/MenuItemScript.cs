﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class MenuItemScript : MonoBehaviour
{
    public UnityEvent onClick;
    public UnityEventFloat onChange;
    public Color HoverColor;
    private Color StartColor;
    private Image image;
    public TextMeshPro textMesh;
    private string _text;

    public string Text{
        get {
            return _text;
        }
        set {
            if(textMesh == null)
                textMesh = GetComponentInChildren<TextMeshPro>();

            _text = value;
            textMesh.text = _text;
        }
    }
    public bool range;

    private void Awake()
    {
        range = false;
        image = GetComponent<Image>();
        StartColor = image.color;
        textMesh = GetComponentInChildren<TextMeshPro>();
    }

    public void Hover(float percentage = 1.0f, float maxFill = 1.0f)
    {
        if(onChange != null)
        {
           image.fillAmount = percentage*maxFill;
           onChange.Invoke(percentage); 
           textMesh.text = _text + "\n" + (percentage*100).ToString("000") + "%";
        }
        image.color = HoverColor;
    }

    public void UnHover()
    {
        image.color = StartColor;
    }

}
