﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class MeshGeneratorScript : MonoBehaviour{
    public MeshFilter mf;
    public BoxCollider boxCollider;

    [HideInInspector] public BigReader bigReader;
    [HideInInspector] public BigWigManagerScript manager;

    [HideInInspector] public int binSize;
    [HideInInspector] public string fileName;

    public int nPoints;
    public int resZ;

    public void readPointsFromBigWig()
    {
        ArrayList points = new ArrayList();
        float x = 0;
        points.Add(Vector2.zero);
        foreach (string line in bigReader.ReadFromBigWig("bw.stats('chr1'," + manager.l1 + "," + manager.l2 + ",nBins=" + manager.binSize + ")"))
        {
            points.Add(new Vector2(x, float.Parse(line)));
            points.Add(new Vector2(++x, float.Parse(line)));
        }
        points.Add(new Vector2(x, 0));
        updateMesh(points.OfType<Vector2>().ToArray());
    }

    public void readPointsFromBedGraph()
    {
        ArrayList points = new ArrayList();
        int counter = 0;
        foreach (string line in System.IO.File.ReadLines("Data/" + fileName))
        {
            string[] values = line.Split('\t', ' '); //chr start stop value0 value1.. etc

            if (int.Parse(values[1]) < manager.l1)
                continue;
            if (int.Parse(values[2]) > manager.l2)
                break;
            if (++counter >= nPoints)
                break;
            //points.Add(new Vector2(float.Parse(values[1])/2 + float.Parse(values[2])/2, float.Parse(values[3])));
            points.Add(new Vector2(float.Parse(values[1]), float.Parse(values[3])));
            points.Add(new Vector2(float.Parse(values[2]), float.Parse(values[3])));
        }
        updateMesh(points.OfType<Vector2>().ToArray());
    }

    public void createRandomGraph()
    {
        Vector2[] points = createRandomPoints();
        manager.l1 = (int)points[0].x;
        manager.l2 = (int)points[points.Length - 1].x;
        smoothMesh(points);
    }

    public void createRandomGraph2()
    {
        Vector2[] points = createRandomPoints2();
        manager.l1 = (int)points[0].x;
        manager.l2 = (int)points[points.Length - 1].x;
        updateMesh(points);
    }

    private Vector2[] createRandomPoints()
    {
        int x = 10000;
        float y = 0;
        int size = Random.Range(50, 201);
        ArrayList points = new ArrayList();
        for (int i = 0; i < size; i++)
        {
            y = Mathf.Clamp(y + Random.Range(-0.2f, 0.2f), 0, 1);
            x += Random.Range(100, 501);
            points.Add(new Vector2(x, y));
        }
        return points.OfType<Vector2>().ToArray();
    }

    private Vector2[] createRandomPoints2()
    {
        int x = 10000;
        float y = 0;
        int size = Random.Range(50, 201);
        ArrayList points = new ArrayList();
        points.Add(new Vector2(x, 0));
        for (int i = 0; i < size; i++)
        {
            y = Mathf.Clamp(y + Random.Range(-0.2f, 0.2f), 0, 1);
            points.Add(new Vector2(x, y));
            x += Random.Range(100, 501);
            points.Add(new Vector2(x, y));
        }
        points.Add(new Vector2(x, 0));
        return points.OfType<Vector2>().ToArray();
    }

    public void updateMesh(Vector2[] points) //Creates 2 points for each bin
    {
        Mesh mesh = mf.mesh;
        Vector3[] vertices = new Vector3[6 * (resZ - 1) * (points.Length + 1) + 6];
        Vector3[] normals = new Vector3[6 * (resZ - 1) * (points.Length + 1) + 6];
        int[] tris = new int[6 * (resZ - 1) * (points.Length - 1) + 6];
        int index = 0;
        for (int i = 0; i < points.Length - 1; i++)
        {
            for (int j = 0; j < resZ - 1; j++)
            {
                //first triangle
                float x = ((points[i].x - points[0].x) / (points[points.Length - 1].x - points[0].x)) * manager.length;
                float y = Sine(j) * points[i].y * manager.height;
                float z = ((float)j / (float)(resZ - 1)) * manager.width;
                vertices[index] = new Vector3(x, y, z); 
                tris[index] = index++;

                x = ((points[i].x - points[0].x) / (points[points.Length - 1].x - points[0].x)) * manager.length;
                y = Sine(j+1) * points[i].y * manager.height;
                z = ((float)(j+1) / (float)(resZ - 1)) * manager.width;
                vertices[index] = new Vector3(x, y, z);
                tris[index] = index++;

                x = ((points[i+1].x - points[0].x) / (points[points.Length - 1].x - points[0].x)) * manager.length;
                y = Sine(j+1) * points[i+1].y * manager.height;
                z = ((float)(j+1) / (float)(resZ - 1)) * manager.width;
                vertices[index] = new Vector3(x, y, z);
                tris[index] = index++;

                normals[index-1] = normals[index - 2] = normals[index - 3] = Vector3.Cross(vertices[index - 3] - vertices[index-1], vertices[index - 2] - vertices[index-1]);

                //second triangle
                x = ((points[i].x - points[0].x) / (points[points.Length - 1].x - points[0].x)) * manager.length;
                y = Sine(j) * points[i].y * manager.height;
                z = ((float)j / (float)(resZ - 1)) * manager.width;
                vertices[index] = new Vector3(x, y, z);
                tris[index] = index++;

                x = ((points[i+1].x - points[0].x) / (points[points.Length - 1].x - points[0].x)) * manager.length;
                y = Sine(j+1) * points[i+1].y * manager.height;
                z = ((float)(j+1) / (float)(resZ - 1)) * manager.width;
                vertices[index] = new Vector3(x, y, z);
                tris[index] = index++;

                x = ((points[i+1].x - points[0].x) / (points[points.Length - 1].x - points[0].x)) * manager.length;
                y = Sine(j) * points[i+1].y * manager.height;
                z = ((float)j / (float)(resZ - 1)) * manager.width;
                vertices[index] = new Vector3(x, y, z);
                tris[index] = index++;

                normals[index - 1] = normals[index - 2] = normals[index - 3] = Vector3.Cross(vertices[index - 3] - vertices[index - 1], vertices[index - 2] - vertices[index - 1]);
            }
        }
        //Add floor
        //first triangle
        float xx = 0;
        float yy = 0;
        float zz = 0;
        vertices[index] = new Vector3(xx, yy, zz);
        tris[index] = index++;

        xx = manager.length;
        yy = 0;
        zz = 0;
        vertices[index] = new Vector3(xx, yy, zz);
        tris[index] = index++;

        xx = manager.length;
        yy = 0;
        zz = manager.width;
        vertices[index] = new Vector3(xx, yy, zz);
        tris[index] = index++;

        normals[index - 1] = normals[index - 2] = normals[index - 3] = Vector3.Cross(vertices[index - 3] - vertices[index - 1], vertices[index - 2] - vertices[index - 1]);

        //second triangle
        xx = 0;
        yy = 0;
        zz = 0;
        vertices[index] = new Vector3(xx, yy, zz);
        tris[index] = index++;

        xx = manager.length;
        yy = 0;
        zz = manager.width;
        vertices[index] = new Vector3(xx, yy, zz);
        tris[index] = index++;

        xx = 0;
        yy = 0;
        zz = manager.width;
        vertices[index] = new Vector3(xx, yy, zz);
        tris[index] = index++;

        normals[index - 1] = normals[index - 2] = normals[index - 3] = Vector3.Cross(vertices[index - 3] - vertices[index - 1], vertices[index - 2] - vertices[index - 1]);

        mesh.vertices = vertices;
        mesh.triangles = tris;
        mesh.normals = normals;

    }

    public void smoothMesh(Vector2[] points) //Creates one points for each bin
    {
        Mesh mesh = mf.mesh;
        Vector3[] vertices = new Vector3[resZ * points.Length];
        for (int i = 0; i < points.Length; i++)
        {
            for (int j = 0; j < resZ; j++)
            {
                float x = ((points[i].x - points[0].x) / (points[points.Length - 1].x - points[0].x)) * manager.length;
                float y = Sine(j) * points[i].y * manager.height;
                float z = ((float)j / (float)(resZ - 1)) * manager.width;
                vertices[i * resZ + j] = new Vector3(x,y,z);
            }
        }
        mesh.vertices = vertices;

        var tris = new int[3*2 * (resZ-1) * (points.Length - 1) + 6 + 6 * (resZ-2)];
        int index = 0;
        for (int i = 0; i < points.Length - 1; i++)
        {
            for (int j = 0; j < resZ - 1; j++)
            {
                //first triangle
                tris[index++] = resZ * i + j;
                tris[index++] = resZ * i + j + 1;
                tris[index++] = resZ * i + j + 1 + resZ;

                //second triangle
                tris[index++] = resZ * i + j;
                tris[index++] = resZ * i + j + 1 + resZ;
                tris[index++] = resZ * i + j + resZ;
            }
        }

        //Add floor

        //first triangle
        tris[index++] = 0;
        tris[index++] = resZ * (points.Length-1);
        tris[index++] = resZ-1;

        //second triangle
        tris[index++] = resZ-1;
        tris[index++] = resZ * (points.Length - 1);
        tris[index++] = resZ * (points.Length - 1) + resZ-1;

        //Add sides
        for (int i = 0; i < resZ - 2; i++)
        {
            tris[index++] = i + (points.Length-1)*resZ;
            tris[index++] = i + 1 + (points.Length - 1) * resZ;
            tris[index++] = resZ - 1 + (points.Length - 1) * resZ;
        }
        for (int i = 0; i < resZ - 2; i++)
        {
            tris[index++] = i;
            tris[index++] = resZ - 1;
            tris[index++] = i + 1;
        }

        mesh.triangles = tris;

        Vector3[] normals = Enumerable.Repeat(Vector3.up, vertices.Length).ToArray();
        for (int i = 1; i < points.Length-1; i++)
        {
            for (int j = 1; j < resZ-1; j++)
            {
                normals[i * resZ + j] = (Vector3.Cross((vertices[i * resZ + j] - vertices[i * resZ + j - 1]).normalized,
                                        (vertices[i * resZ + j] - vertices[i * resZ + j - resZ]).normalized)+
                                        Vector3.Cross((vertices[i * resZ + j] - vertices[i * resZ + j + 1]).normalized,
                                        (vertices[i * resZ + j] - vertices[i * resZ + j + resZ]).normalized)).normalized;
            }
        }
        mesh.normals = normals;

        Vector2[] uv = new Vector2[vertices.Length];
        for (int i = 0; i < points.Length; i++)
        {
            for (int j = 0; j < resZ; j++)
            {
                uv[i * resZ + j] = new Vector2((float)i / points.Length-1, (float)j/(resZ-1));
            }
        }
        mesh.uv = uv;
    }


    private float Sine(int input)
    {
        return Mathf.Sin(Mathf.Lerp(0,Mathf.PI, (float)input/(float)(resZ-1)));
    }


}
