﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FilterMenuScript : MonoBehaviour
{
    public List<Toggle> toggles = new List<Toggle>();
    public TMP_Text valueTextBar;
    public int searchDepth = 0;
    public DisplayHandler displayHandler;
    void Awake()
    {
        valueTextBar.text = searchDepth.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateValueOnDisplay(Single value)
    {
        searchDepth = (int)(value * (float) ReferenceManager.MaxSearchDepth);
        valueTextBar.text = searchDepth.ToString();
    }

    public void Apply()
    {
        if(displayHandler)
            displayHandler.updateInteractionSearchDepth(searchDepth);
    }

}
