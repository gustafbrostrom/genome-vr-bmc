﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GeneInfoDisplay : MonoBehaviour
{
    private ReferenceManager rm;
    private GameObject GenomeDisplay;
    public Text interactionsListA;
    private List<Interaction> interactions;

    private void Awake()
    {
        
    }

    public void Setup(ReferenceManager rm, GeneV2 g, GameObject genomeDisp)
    {
        //this.GenomeDisplay = genomeDisp;
        this.rm = rm;
        transform.Find("Text").GetComponent<Text>().text = g.Name;
        interactions = rm.chromosomeManager.GetIntraInteractions(g.Chr, g.L1 , g.L2);
        fillInfo();
    }

    public void fillInfo()
    {
        interactionsListA.text = "";
        foreach (Interaction inter in interactions)
            interactionsListA.text += string.Format("{0:#,0}", inter.midB) + "\n";
    }

    public void analyzeButton()
    {
        print("analyzing");
    }

    public void webButton()
    {
        print("web");
    }

    public void removeDisplay()
    {
        GenomeDisplay.GetComponent<DisplayHandler>().toggled = false;
        Destroy(this.gameObject);
    }
}
