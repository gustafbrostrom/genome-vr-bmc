﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Texture2DHandler))]
public class mapHolderScript : MonoBehaviour
{
    public DisplayHandler displayHandler;
    public RectTransform rect;
    public GameObject cylinder;
    private Texture2D tex;

    public GameObject interactionPrefab;

    private List<GameObject> interactionPool = new List<GameObject>();

    private Vector3[] corners = new Vector3[4];
    private Vector3 start, end;
    private int counter = 0;

    private Color[] tempArray;

    void Start()
    {
        tex = GetComponentInChildren<Texture2DHandler>().texture;
        //tex = new Texture2D(texWidth, texHeight);
        //tex.filterMode = FilterMode.Point;
        //cylinder.GetComponent<Renderer>().material.mainTexture = tex;
        tempArray = new Color[tex.width * tex.height];
    }

    //IEnumerator Initialize()
    //{
    //    yield return new WaitUntil(() => GetComponentInChildren<Texture2DHandler>().texture != null);
    //    tex = GetComponentInChildren<Texture2DHandler>().texture;
    //    //tex = new Texture2D(texWidth, texHeight);
    //    //tex.filterMode = FilterMode.Point;
    //    //cylinder.GetComponent<Renderer>().material.mainTexture = tex;
    //    tempArray = new Color[tex.width * tex.height];
    //}


    public void updateTexture(float a, float b) {
        Color[] colorArray = new Color[tex.width * tex.height];
        for (int i = 0; i < tex.height; i++)
        {
            if(i>a* tex.height && i < b * tex.height)
            {
                colorArray[i] = Color.red;
            }
            else
            {
                colorArray[i] = Color.white;
            }
        }
        tempArray = colorArray;
        tex.SetPixels(colorArray);
        tex.Apply();
    }

    public void updateTexture(float a, float b, Color color)
    {
        Color[] colorArray = new Color[tex.width * tex.height];
        for (int i = 0; i < tex.height; i++)
        {
            if (i > a * tex.height && i < b * tex.height)
            {
                colorArray[i] = color;
            } else
            {
                colorArray[i] = tempArray[i];
            }
        }
        tex.SetPixels(colorArray);
        tex.Apply();
    }

    public void resetTexture()
    {
        tex.SetPixels(tempArray);
        tex.Apply();
    }

    public void updateCorners()
    {
        rect.GetLocalCorners(corners);
        start = Vector3.Lerp(corners[0], corners[1], 0.66f);
        end = Vector3.Lerp(corners[3], corners[2], 0.66f);
        counter = 0;
    }

    public void displayInteraction(Interaction i)
    {
        GameObject go = getInteraction(counter);
        InteractionHandler handler = go.GetComponent<InteractionHandler>();
        float a = (float)i.midA / (float)displayHandler.maxLoc;
        float b = (float)i.midB / (float)displayHandler.maxLoc;
        Vector3 anchorA = Vector3.Lerp(start, end, a);
        Vector3 anchorB = Vector3.Lerp(start, end, b);
        handler.setCurvedLine(anchorA, anchorB, 40f);
        counter++;
    }

    private GameObject getInteraction(int index)
    {
        GameObject newInteraction;
        if (index < interactionPool.Count)
        {
            newInteraction = interactionPool[index];
            if (newInteraction.activeSelf == false)
            {
                newInteraction.SetActive(true);
            }
        }
        else
        {
            interactionPool.Add(Instantiate(interactionPrefab, transform, false));
            newInteraction = interactionPool[index];
        }
        newInteraction.GetComponent<InteractionHandler>().reset();
        return newInteraction;
    }

    public void deactivateUnusedInteractions()
    {
        for (int i = counter; i < interactionPool.Count; i++)
        {
            interactionPool[i].SetActive(false);
        }
    }

    public Vector3 locationPosToCylinderPos(float l)
    {
        Vector3[] corners = new Vector3[4];
        rect.GetLocalCorners(corners);
        print(corners[0] + " " + corners[1] + " " + corners[2] + " " + corners[3]);
        Vector3 start = Vector3.Lerp(corners[0], corners[1],1);
        Vector3 end = Vector3.Lerp(corners[3], corners[2],1);
        return Vector3.Lerp(start, end, l);

    }
}
