﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneRowHandler : MonoBehaviour
{
    public ReferenceManager rm;
    public DisplayHandler displayHandler;

    public GameObject geneDisplayPrefab;
    public GameObject proteinBindingsDisplayPrefab;

    private Queue<GameObject> geneDisplayPool = new Queue<GameObject>();
    private Queue<GameObject> proteinBindingDisplayPool = new Queue<GameObject>();

    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject getProteinBindingDisplay()
    {
        GameObject newProteinBindingDisplay = null;
        if (proteinBindingDisplayPool.Count != 0)
        {
            newProteinBindingDisplay = proteinBindingDisplayPool.Dequeue();
            if (!newProteinBindingDisplay.activeSelf)
                newProteinBindingDisplay.SetActive(true);
        }
        else
        {
            newProteinBindingDisplay = Instantiate(proteinBindingsDisplayPrefab, transform);
        }
        return newProteinBindingDisplay;
    }

    public void returnProteinBindingDisplay(GameObject display)
    {
        proteinBindingDisplayPool.Enqueue(display);
    }

    public void deactivateUnusedProteinBindingDisplays()
    {
        foreach (GameObject g in proteinBindingDisplayPool){
            if (g.activeSelf){
                g.SetActive(false);
            }
        }
            
                
    }

    public GameObject getGeneDisplay(){
        GameObject newGeneDisplay = null;
        if(geneDisplayPool.Count != 0)
        {
            newGeneDisplay = geneDisplayPool.Dequeue();
            if(!newGeneDisplay.activeSelf)
                newGeneDisplay.SetActive(true);
        }
        else
        {
            newGeneDisplay = Instantiate(geneDisplayPrefab,transform);
            newGeneDisplay.GetComponent<GeneDisplayHandler>().displayHandler = displayHandler;
        }
        return newGeneDisplay;
    }

    public void returnGeneDisplay(GameObject geneDisplay){
        geneDisplayPool.Enqueue(geneDisplay);
    }

    public void deactivateUnusedGeneDisplays(){
        foreach(GameObject g in geneDisplayPool)
            if(g.activeSelf)
                g.SetActive(false);
    }

}
