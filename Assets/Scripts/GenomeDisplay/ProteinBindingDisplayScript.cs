﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProteinBindingDisplayScript : MonoBehaviour
{
    public Image image;
    public RectTransform rectTransform;
    public TextMeshPro tmp;

    public void setTextandColor(string s, Color color)
    {
        tmp.text = "<color=#" + ColorUtility.ToHtmlStringRGB(Color.black) + ">" + s + "</color>";
        image.color = color;
    }
}
