﻿using UnityEngine;

public class FrameHandler : MonoBehaviour
{
    public float width, height, thickness;

    [SerializeField]
    private GameObject[] cylinders;

    [SerializeField]
    private GameObject[] spheres;

    private void OnValidate()
    {
        updateSizeAndLocation();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            updateWidthAndHeight();
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            updateSizeAndLocation();
        }
    }

    public void updateSizeAndLocation()
    {
        cylinders[0].transform.localScale = new Vector3(thickness, height / 2, thickness);
        cylinders[1].transform.localScale = new Vector3(thickness, width / 2, thickness);
        cylinders[2].transform.localScale = new Vector3(thickness, height / 2, thickness);
        cylinders[3].transform.localScale = new Vector3(thickness, width / 2, thickness);

        cylinders[0].transform.localPosition = new Vector3(width / 2, 0, 0);
        cylinders[1].transform.localPosition = new Vector3(0, height / 2, 0);
        cylinders[2].transform.localPosition = new Vector3(-width / 2, 0, 0);
        cylinders[3].transform.localPosition = new Vector3(0, -height / 2, 0);

        spheres[0].transform.localScale = new Vector3(thickness, thickness, thickness);
        spheres[1].transform.localScale = new Vector3(thickness, thickness, thickness);
        spheres[2].transform.localScale = new Vector3(thickness, thickness, thickness);
        spheres[3].transform.localScale = new Vector3(thickness, thickness, thickness);

        spheres[0].transform.localPosition = new Vector3(width / 2, height / 2, 0);
        spheres[1].transform.localPosition = new Vector3(-width / 2, height / 2, 0);
        spheres[2].transform.localPosition = new Vector3(-width / 2, -height / 2, 0);
        spheres[3].transform.localPosition = new Vector3(width / 2, -height / 2, 0);
    }

    public void setWidthAndHeight(float width, float height)
    {
        this.width = width;
        this.height = height;
        updateSizeAndLocation();
    }

    public void addWidthAndHeight(float width, float height)
    {
        this.width += width;
        this.height += height;
        updateSizeAndLocation();
    }

    public void updateWidthAndHeight()
    {
        this.width = Vector3.Distance(cylinders[0].transform.localPosition, cylinders[2].transform.localPosition);
        this.height = Vector3.Distance(cylinders[1].transform.localPosition, cylinders[3].transform.localPosition);
        Vector3 mid1 = Vector3.Lerp(cylinders[0].transform.position, cylinders[2].transform.position, 0.5f);
        Vector3 mid2 = Vector3.Lerp(cylinders[1].transform.position, cylinders[3].transform.position, 0.5f);
        transform.parent.position = new Vector3(mid1.x,mid2.y,mid1.z);
        updateSizeAndLocation();
    }

    public void setThickness(float t)
    {
        this.thickness = t;
        updateSizeAndLocation();
    }
}
