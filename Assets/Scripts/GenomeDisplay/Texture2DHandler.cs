﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Texture2DHandler : MonoBehaviour
{ 
    [HideInInspector]public Texture2D texture;
    [Range(1,1000)]public int width;
    [Range(1,1000)]public int height;

    // Start is called before the first frame update
    void Awake()
    {
        texture = new Texture2D(width, height);
        texture.filterMode = FilterMode.Point;
        GetComponent<Renderer>().material.mainTexture = texture;
    }

}
