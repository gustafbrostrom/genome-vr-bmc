﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleScript : MonoBehaviour
{
    private Color normalColor;
    public Color hoverColor;
  
    private Material material;
    private bool isConnected;
    
    private InteractionTool tool;

    private Vector3 oldToolPos = Vector3.positiveInfinity;

    [SerializeField]
    private DisplayHandler displayHandler;
    private FrameHandler frameHandler;

    private bool connected = false;


    // Start is called before the first frame update
    void Awake()
    {
        material = GetComponent<Renderer>().material;
        normalColor = material.color;
        frameHandler = transform.parent.GetComponent<FrameHandler>();

    }

    // Update is called once per frame
    void Update()
    {
        if (tool && tool.isPinching)
        {
            Vector3 toolPosInHandleSpace = transform.InverseTransformPoint(tool.transform.position);
            Vector3 projectedPos = Vector3.Project(toolPosInHandleSpace, Vector3.right);
            transform.position = transform.TransformPoint(projectedPos);
            frameHandler.updateWidthAndHeight();
            if(displayHandler)
                displayHandler.resizeCanvas();
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("InteractionTool"))
        {
            material.color = hoverColor;
            tool = other.GetComponent<InteractionTool>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("InteractionTool"))
        {
            material.color = normalColor;
            tool = null;
        }
    }


}
