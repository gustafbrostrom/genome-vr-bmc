﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.Events;


public class GeneDisplayHandler : MonoBehaviour
{

    public ReferenceManager rm;
    public DisplayHandler displayHandler;
    public RectTransform rectTransform;

    public Image image;
    private ClickableObject button;
    public TextMeshPro tmp;
    public RectTransform tmpRectTransform;

    [SerializeField]
    private Material defaultMaterialHandle;
    private Material material;
    private Texture2D tex;
    private int texHeight, texWidth;

    [SerializeField]
    private new BoxCollider collider;

    [SerializeField]
    private GameObject geneinfodisplay;

    [SerializeField]
    private GameObject GenomeDisplay;

    [SerializeField]
    private Button extraDisplay;

    public static Color[] colorwheel = new Color[] { Color.blue, Color.red, Color.yellow, Color.magenta, Color.green, Color.cyan };

    // Data
    private Button[] geneInfoButtons;
    private TextMeshPro geneInfoText;

    public GeneV2 gene;

    // Start is called before the first frame update
    void Awake()
    {
        image = GetComponent<Image>();
        material = new Material(defaultMaterialHandle);
        image.material = material;
        texHeight = 15;
        texWidth = 1000;
        tex = new Texture2D(texWidth, texHeight);
        material.mainTexture = tex;
        material.shader = Shader.Find("UI/Default");
        button = GetComponent<ClickableObject>();
        tmp = GetComponentInChildren<TextMeshPro>();

    }

    // Update is called once per frame
    void Update()
    {
        // Makes sure the collider is the right size 
        collider.size = new Vector3(rectTransform.rect.width, 50.0f, 0.0f);
    }

    public void setGeneData(GeneV2 g)
    {
        this.gene = g;
        tmp.text = "<color=#" + ColorUtility.ToHtmlStringRGB(Color.black) + ">" + g.Name + "</color>";
    }

    public void applyTexture(int location1, int location2){
        tex.SetPixels(calcColorArrayForGene(location1, location2));
        tex.Apply();
    }

    private Color[] calcColorArrayForGene(int location1, int location2)
    {
        int l1 = location1 < gene.L1 ? gene.L1 : location1;
        int l2 = location2 > gene.L2 ? gene.L2 : location2;

        Color color = colorwheel[Math.Abs(gene.Name.GetHashCode()) % colorwheel.Length];
        Color exonColor = Color.Lerp(color, Color.black, 0.5f);
        Color trans = new Color(0, 0, 0, 0);

        bool[] inExon = new bool[texWidth];
        for (int k = 0; k < gene.Exons.Count; k++)
        {
            float lowerPercentage = (float)(gene.Exons[k].L1 - l1) / (float)(l2 - l1);
            float upperPercentage = (float)(gene.Exons[k].L2 - l1) / (float)(l2 - l1);
            int lower = (int)(lowerPercentage * texWidth);
            int upper = (int)(upperPercentage * texWidth);
            if (upper < 0 || lower > texWidth) //if this exon is outside the the part of the gene currently displayed, skip it
                continue;
            if (lower < 0)
                lower = 0;
            if (upper > texWidth)
                upper = texWidth;
            for (int i = lower; i < upper; i++)
                inExon[i] = true;
        }
        Color[] colorArray = new Color[texWidth * texHeight];
        for (int i = 0; i < texWidth; i++)
        {
            for (int j = 0; j < texHeight; j++)
            {
                if (inExon[i])
                {
                    colorArray[j * texWidth + i] = exonColor;
                }
                else
                {
                    colorArray[j * texWidth + i] = j<5 || j>9 ? trans : color;
                }
            }
        }
        return colorArray;
    }

    public void setInfoDisplay()
    {
        // Could need to add positioning
        //GameObject display = Instantiate(geneinfodisplay, rectTransform);

        if (!GenomeDisplay.GetComponent<DisplayHandler>().toggled)
        {
            GameObject display = Instantiate(geneinfodisplay, displayHandler.transform);
            display.GetComponent<GeneInfoDisplay>().Setup(rm, gene, GenomeDisplay);
            print("wat");
            GenomeDisplay.GetComponent<DisplayHandler>().toggled = true;
        }
    }

}
