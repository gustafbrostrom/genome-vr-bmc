﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Mono.Data.Sqlite;
using System.Data;
using System.IO;
using System.Diagnostics;
using IntervalTree;

public class DisplayHandler : MonoBehaviour
{
    //Prefabs
    public GameObject geneDisplayPrefab;
    public GameObject geneDisplayRowPrefab;

   
    private ChromosomeOverviewScript chrOverview;
    public FilterMenuScript filterMenu;
      
    public ReferenceManager rm;

    public Canvas canvas;
    public RectTransform canvasRectTransform;
    public BoxCollider canvasCollider;

    public FrameHandler frame;

    //Title Row
    [SerializeField]
    private RectTransform titleHolderRect;
    [SerializeField]
    private TextMeshPro rangeDisplay;


    //The interactionHolderRect 
    [SerializeField]
    private RectTransform interactionHolderRect;

    public InteractionHolderScript interactionHolderScript;

    public RectTransform interactionDensityMapRect;

    public InteractionDensityMapScript interactionDensityMapScript;

    //The sequenceRowHolderRect
    [SerializeField]
    private RectTransform sequenceHolderRect;
    //Basepairhandler 
    [SerializeField]
    private RectTransform basepairHolderRect;

    [SerializeField]
    private TextMeshPro basepairText;

    //The geneRowHolderRect
    [SerializeField]
    private RectTransform geneRowHolderRect;
    [SerializeField]
    private BoxCollider geneRowHolderBoxCollider;
    [SerializeField]
    private List<GeneRowHandler> geneRowHandlers = new List<GeneRowHandler>();
    //The buttonRowHolderRect
    [SerializeField]
    private RectTransform mapHolderRect;
    public mapHolderScript mapHolderScript;

    //The buttonRowHolderRect
    [SerializeField]
    private RectTransform buttonHolderRect;

    public Image loadingBar;
    public Texture2D loadingBarTexture;
    public Material defaultMaterialHandle;

    //Sizes
    public float displayWidth;
    private float displayHeight;
    public float titleHolderHeight;
    public float interactionHolderHeight;
    public float interactionDensityMapHeight;
    public float sequenceHolderHeight;
    public float geneRowHeight;
    public float geneRowHolderHeight;
    public float mapHolderHeight;
    public float buttonHolderHeight;

    // Cylinder handler
    public GameObject cylinder;

    //Data
    public string chr = "chr1";
    private float oldFontSize;
    private bool zoom;
    private int basepairInterval = 100;
    public bool toggled = false; 
    public Vector3 cylStart;
    public Vector3 cylEnd;

    public int minLoc = 0;
    public int maxLoc = 0;

    private LinkedList<Gene> currentGenes = new LinkedList<Gene>();
    private Stack<Gene> rightStack = new Stack<Gene>();
    private Stack<Gene> leftStack = new Stack<Gene>();
    private Stack<Gene> bouncerStack = new Stack<Gene>();

    private List<Interaction> intraInteractions = new List<Interaction>();
    private List<Interaction> interInteractions = new List<Interaction>();

    public int location1, location2; //The current range being displayed

    //old input data used for calculating scaling and translation
    float oldh1 = float.NegativeInfinity;
    float oldh2 = float.NegativeInfinity;
    private List<Transform> poiTransforms = new List<Transform>();

    public void SetupAndPassOnReferences(string chr, ReferenceManager rm, int l1 = -1, int l2 = -1)
    {
        this.rm = rm;
        this.chr = chr;
        this.maxLoc = rm.humanChrLength[rm.chrNameToNumber[chr]];

        location1 = l1 == -1 ? minLoc : l1;
        location2 = l2 == -1 ? maxLoc : l2;

        //loadEntireChromosome(rm.chromosomeManager.GetGenes(chr));
        StartCoroutine(setupAndPassOnReferencesCoroutine());
        
    }

    IEnumerator setupAndPassOnReferencesCoroutine()
    {
        yield return new WaitForEndOfFrame();
        DisplayData();
    }

    private void OnDestroy()
    {
        if(chrOverview)
            Destroy(chrOverview.gameObject);
    }

    private void DisplayPointsOfInterest()
    {
        //List with all points of interest in the range displayed
        List<PointOfInterest> pois = rm.chromosomeManager.GetPointsOfInterest(chr);
        int id = GetInstanceID();
        foreach(PointOfInterest poi in pois)
        {
            //Outside
            if (poi.location < location1 || poi.location > location2)
            {
                if (poi.representations.ContainsKey(id))
                {
                    Destroy(poi.representations[id].gameObject);
                    poi.representations.Remove(id);
                }
                continue;
            }

            Transform t;            
            if (!poi.representations.ContainsKey(id))
            {
                t = Instantiate(rm.GameObjectPrefab, transform).transform;
                poi.representations.Add(id, t);
                poiTransforms.Add(t);
            }
            else
            {
                t = poi.representations[id];
            }

            

            t.position = transform.TransformPoint(locationPosToWorldPos(poi.location) + Vector3.up*200f);

        }


        
        
    }


    public void SaveCurrentState()
    {
        File.AppendAllText("boi.csv", chr + "," + location1 + "," + location2 + Environment.NewLine);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Navigate("up");
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Navigate("down");
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Navigate("left");
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Navigate("right");
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            addRow();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            removeRow();
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            resizeCanvas();
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            updateGeneDisplays();
        }
    }

    public void updateLocations(int l1, int l2)
    {

        if (Mathf.Abs(l1 - l2) < Mathf.Abs(location1 - location2))
        {
            zoom = true;
        } else
        {
            zoom = false; 
        }

        if (l2 < l1)//Check for valid input and reverse if needed
        {
            print("Was given a reverse range");
            int temp = l1;
            l1 = l2;
            l2 = l1;
        }

        if (l1 < minLoc)
            l1 = minLoc;
        if (l2 > maxLoc)
            l2 = maxLoc;

        location1 = l1;
        location2 = l2;
        DisplayData();
    }

    public void DisplayData()
    {
        if (chrOverview)
            chrOverview.updateLocations(location1, location2);

        updateGeneDisplays();
        if (location2 - location1 < 50000000)
            displayInteractions();
        DisplayPointsOfInterest();
        if(chrOverview)
            chrOverview.completeUpdate();

        if (location2 - location1 < 1000)
            displayProteinBindings();


        float a = (float)location1 / (float)maxLoc;
        float b = (float)location2 / (float)maxLoc;
        mapHolderScript.updateTexture(a, b);

        if (location2 - location1 < 100)
        {
            if (!sequenceHolderRect.gameObject.activeSelf)
            {
                sequenceHolderRect.gameObject.SetActive(true);
                resizeCanvas();
            }
            displayBasePairs();
        }
        else
        {
            if (sequenceHolderRect.gameObject.activeSelf)
            {
                sequenceHolderRect.gameObject.SetActive(false);
                resizeCanvas();
            }
        }

        //Update the range display
        rangeDisplay.text = chr + ": " + string.Format("{0:#,0}", location1) + " - " + string.Format("{0:#,0}", location2);
    }

    public void Navigate(string s){
        int l1;
        int l2;
        switch(s){
            case "up": //Zoom in
                l1 = location1 + (location2 - location1) / 8;
                l2 = location2 - (location2 - location1) / 8;
            break;
            case "down": //Zoom out
                l1 = location1 - (location2 - location1) / 6;
                l2 = location2 + (location2 - location1) / 6;
            break;
            case "left":
                l1 = location1 - (location2 - location1) / 8;
                l2 = location2 - (location2 - location1) / 8;
            break;
            case "right":
                l1 = location1 + (location2 - location1) / 8;
                l2 = location2 + (location2 - location1) / 8;
            break;
            default:
                return;
        }
        updateLocations(l1, l2);    
    }

    public void scaleLocations(Vector3 leftHand, Vector3 rightHand)
    {
        float h1 = worldCoordToDisplayWidthPercentage(leftHand);
        float h2 = worldCoordToDisplayWidthPercentage(rightHand);
        if (oldh1 != float.NegativeInfinity && oldh2 != float.NegativeInfinity)
        {
            //Redo so it doesnt do two updateLocations calls

            //translate first
            float scale = Mathf.Abs(h2 - h1) / Mathf.Abs(oldh2 - oldh1);
            float oldMid = (oldh2 + oldh1) / 2;
            float mid = (h2 + h1) / 2;
            int transDist = (int)((mid - oldMid) * (location2 - location1));
            updateLocations(location1 - transDist, location2 - transDist);

            //then scale
            int t1 = (int)(mid * (location2 - location1));
            int left = (int)(t1 * scale) - t1;

            int t2 = (int)((1 - mid) * (location2 - location1));
            int right = (int)(t2 * scale) - t2;
            updateLocations(location1 + left, location2 - right);
        }
        oldh1 = h1;
        oldh2 = h2;
    }

    public void translateLocationsWithLeftHand(Vector3 hand)
    {
        float h1 = worldCoordToDisplayWidthPercentage(hand);
        if (oldh1 != float.NegativeInfinity)
        {
            float percentage = h1 - oldh1;
            int distToMove = (int)(percentage * (location2 - location1));
            if (distToMove < 1 && -1 < distToMove)
                return;
            updateLocations(location1 - distToMove, location2 - distToMove);
        }
        oldh1 = h1;
    }

    public void translateLocationsWithRightHand(Vector3 hand)
    {
        float h2 = worldCoordToDisplayWidthPercentage(hand);
        if (oldh2 != float.NegativeInfinity)
        {
            float percentage = h2 - oldh2;
            int distToMove = (int)(percentage * (location2 - location1));
            if (distToMove < 1 && -1 < distToMove)
                return;
            updateLocations(location1 - distToMove, location2 - distToMove);
        }
        oldh2 = h2;
    }

    public void resetOldHandPosData(string hand)
    {
        if (hand == "left")
            oldh1 = float.NegativeInfinity;

        if (hand == "right")
            oldh2 = float.NegativeInfinity;
    }

    public float worldCoordToDisplayWidthPercentage(Vector3 pos) {
        Vector3[] corners = new Vector3[4];
        geneRowHolderRect.GetWorldCorners(corners); //Clockwise orientation with start in lower left
        Vector3 v1 = pos - corners[0];
        Vector3 v2 = corners[3] - corners[0];
        Vector3 v3 = Vector3.Project(v1, v2);
        return v3.magnitude/v2.magnitude;
    }

    public void resizeCanvas(){
        float extraHeight = frame.height - calculateDisplayHeight();
        float extraRows = extraHeight / geneRowHeight;
        while (extraRows >= 1)
        {
            addRow();
            extraRows -= 1f;
        }
        while (extraRows < 0 && geneRowHandlers.Count>1)
        {
            removeRow();
            extraRows += 1f;
        }

        float tempHeightCounter = 0;
        displayWidth = frame.width;
        displayHeight = frame.height;

        //Title holder
        if (titleHolderRect.gameObject.activeInHierarchy)
        {
            titleHolderRect.sizeDelta = new Vector2(0, titleHolderHeight);
            tempHeightCounter += titleHolderHeight;
        }

        if (interactionHolderRect.gameObject.activeInHierarchy)
        {
            interactionHolderRect.offsetMax = new Vector2(0, -tempHeightCounter);
            interactionHolderRect.sizeDelta = new Vector2(0, interactionHolderHeight);
            tempHeightCounter += interactionHolderHeight;
        }

        if (interactionDensityMapRect.gameObject.activeInHierarchy)
        {
            interactionDensityMapRect.offsetMax = new Vector2(0, -tempHeightCounter);
            interactionDensityMapRect.sizeDelta = new Vector2(0, interactionDensityMapHeight);
            tempHeightCounter += interactionDensityMapHeight;
        }

        //Sequence holder
        if (sequenceHolderRect.gameObject.activeInHierarchy)
        {
            sequenceHolderRect.offsetMax = new Vector2(0, -(tempHeightCounter));
            sequenceHolderRect.sizeDelta = new Vector2(0, sequenceHolderHeight);
            tempHeightCounter += sequenceHolderHeight;
        }

        //GeneRowHolder
        if (geneRowHolderRect.gameObject.activeInHierarchy)
        {
            geneRowHolderHeight = geneRowHeight * geneRowHandlers.Count;
            geneRowHolderRect.offsetMax = new Vector2(0, -(tempHeightCounter));
            geneRowHolderRect.sizeDelta = new Vector2(0, geneRowHolderHeight);
            geneRowHolderBoxCollider.center = geneRowHolderRect.rect.center;
            geneRowHolderBoxCollider.size = new Vector3(displayWidth, geneRowHolderHeight, 0);

            //Resize the gene rows
            resizeGeneRowPanels();
            tempHeightCounter += geneRowHolderHeight;
        }

        //Map Holder
        if (mapHolderRect.gameObject.activeInHierarchy)
        {
            mapHolderRect.offsetMax = new Vector2(0, -(tempHeightCounter));
            mapHolderRect.sizeDelta = new Vector2(0, mapHolderHeight);
            mapHolderScript.cylinder.transform.localScale = new Vector3(50, displayWidth / 2, 50);
            tempHeightCounter += mapHolderHeight;
        }

        //Button Holder
        if (buttonHolderRect.gameObject.activeInHierarchy)
        {
            buttonHolderRect.sizeDelta = new Vector2(0, buttonHolderHeight);
            tempHeightCounter += buttonHolderHeight;
        }

        canvasCollider.size = new Vector3(displayWidth, displayHeight, 0);
        canvasRectTransform.sizeDelta = new Vector2(displayWidth, displayHeight);

        DisplayData();
    }

    private float calculateDisplayHeight()
    {
        float ret = 0;
        if (titleHolderRect.gameObject.activeSelf)
            ret += titleHolderHeight;
        if (interactionHolderRect.gameObject.activeSelf)
            ret += interactionHolderHeight;
        if (interactionDensityMapRect.gameObject.activeInHierarchy)
            ret += interactionDensityMapHeight;
        if (sequenceHolderRect.gameObject.activeSelf)
            ret += sequenceHolderHeight;
        if (geneRowHolderRect.gameObject.activeSelf)
            ret += geneRowHeight * geneRowHandlers.Count;
        if (mapHolderRect.gameObject.activeSelf)
            ret += mapHolderHeight;
        if (buttonHolderRect.gameObject.activeSelf)
            ret += buttonHolderHeight;
        return ret;
    }

    public void resizeGeneRowPanels()
    {
        for(int i = 0; i< geneRowHandlers.Count; i++)
        {
            RectTransform rectTransform = geneRowHandlers[i].GetComponent<RectTransform>();
            rectTransform.offsetMin = new Vector2(0, (geneRowHandlers.Count - i - 1) * geneRowHeight);
            rectTransform.offsetMax = new Vector2(0, -i*geneRowHeight);
        }
    }

    public void addRow(){
        GameObject newRow = Instantiate(geneDisplayRowPrefab, geneRowHolderRect, false);
        GeneRowHandler newGeneRowHandler = newRow.GetComponent<GeneRowHandler>();
        newGeneRowHandler.rm = rm;
        newGeneRowHandler.displayHandler = this;
        geneRowHandlers.Add(newGeneRowHandler);
        updateGeneDisplays();
    }

    public void removeRow()
    {
        if (geneRowHandlers.Count <= 1)
            return;

        if (Application.isPlaying)
            Destroy(geneRowHandlers[geneRowHandlers.Count - 1].gameObject);

        geneRowHandlers.RemoveAt(geneRowHandlers.Count - 1);
        updateGeneDisplays();
    }

    public void loadEntireChromosome(List<Gene> inGenes)
    {
        foreach (Gene g in inGenes)
            currentGenes.AddLast(g);
        intraInteractions = rm.chromosomeManager.GetIntraInteractions(chr);
    }

    //Shows the currentGenes on the display
    public void updateGeneDisplays() {
        float[] rowSpaceController = new float[geneRowHandlers.Count];
        for (int i = 0; i < rowSpaceController.Length; i++)
        {
            rowSpaceController[i] = float.NegativeInfinity;
        }

        foreach (GeneRowHandler rowHandler in geneRowHandlers)
        {
            foreach (ProteinBindingDisplayScript proteinBindingDisplayScript in rowHandler.GetComponentsInChildren<ProteinBindingDisplayScript>())
                rowHandler.returnProteinBindingDisplay(proteinBindingDisplayScript.gameObject);

            foreach (GeneDisplayHandler geneDisplayHandler in  rowHandler.GetComponentsInChildren<GeneDisplayHandler>())
                rowHandler.returnGeneDisplay(geneDisplayHandler.gameObject);   
        }
        if(location1 >= location2)
        {
            print(location1 + " " + location2);
            return;
        }
        foreach (KeyValuePair<IntervalTree.Interval<int>,GeneV2> kvp in rm.RefGene("mm10.refGene.gtf").GetGeneTree(chr).GetIntervalsOverlappingWith(new IntervalTree.Interval<int>(location1,location2)))
        {

            int l1 = location1 < kvp.Value.L1 ? kvp.Value.L1 : location1;
            int l2 = location2 > kvp.Value.L2 ? kvp.Value.L2 : location2;

            //Calculate the location of the Gene Display inside the row
            float l1percentage = (float)(l1 - location1) / (float)(location2 - location1);
            float l2percentage = (float)(l2 - location1) / (float)(location2 - location1);

            //Find a row for the geneDisplay to be placed on
            int rowToBePlacedOn = -1;
            for (int j = 0; j < rowSpaceController.Length; j++)
            {
                if (rowSpaceController[j] < l1percentage)
                {
                    rowToBePlacedOn = j;
                    break;
                }
            }
            if (rowToBePlacedOn == -1 || l2percentage - l1percentage < 0.0025f) //If its too small or there was no free row, skip this gene
                continue;

            //Remember how far along this row the righthandside of this gene is
            rowSpaceController[rowToBePlacedOn] = l2percentage;

            //Get and setup the display for the gene
            GameObject geneDisplay = geneRowHandlers[rowToBePlacedOn].getGeneDisplay();
            GeneDisplayHandler geneDisplayHandler = geneDisplay.GetComponent<GeneDisplayHandler>();
            geneDisplayHandler.rm = rm;
            geneDisplayHandler.setGeneData(kvp.Value);
            geneDisplayHandler.applyTexture(location1, location2);

            //Correct its position
            geneDisplayHandler.rectTransform.anchorMin = new Vector2(l1percentage, 0);
            geneDisplayHandler.rectTransform.anchorMax = new Vector2(l2percentage, 1);
            geneDisplayHandler.rectTransform.offsetMin = Vector2.zero;
            geneDisplayHandler.rectTransform.offsetMax = Vector2.zero;

            //Adjust the textmeshpro anchorpoints
            geneDisplayHandler.tmpRectTransform.anchorMin = l1 < location1 ? new Vector2((float)(location1 - l1) / (float)(l2 - l1), 0) : Vector2.zero;
            geneDisplayHandler.tmpRectTransform.anchorMax = l2 > location2 ? new Vector2((float)(location2 - l1) / (float)(l2 - l1), 1) : Vector2.one;
        }

        //Disable unused displays
        foreach (GeneRowHandler rowHandler in geneRowHandlers)
        {
            rowHandler.deactivateUnusedGeneDisplays();
            rowHandler.deactivateUnusedProteinBindingDisplays();
        }
    }

    public void displayProteinBindings()
    {
        foreach (ProteinBinding p in rm.chromosomeManager.GetProteins(chr, location1, location2))
        {
            int l1 = location1 < p.l1 ? p.l1 : location1;
            int l2 = location2 > p.l2 ? p.l2 : location2;

            float l1percentage = (float)(l1 - location1) / (float)(location2 - location1);
            float l2percentage = (float)(l2 - location1) / (float)(location2 - location1);

            //Get and setup the display for the protein binding
            GameObject display = geneRowHandlers[0].getProteinBindingDisplay();
            ProteinBindingDisplayScript proteinBindingDisplayScript = display.GetComponent<ProteinBindingDisplayScript>();

            //Correct its position
            proteinBindingDisplayScript.rectTransform.anchorMin = new Vector2(l1percentage, 0);
            proteinBindingDisplayScript.rectTransform.anchorMax = new Vector2(l2percentage, 1);
            proteinBindingDisplayScript.rectTransform.offsetMin = Vector2.zero;
            proteinBindingDisplayScript.rectTransform.offsetMax = Vector2.zero;

            //Set the text
            proteinBindingDisplayScript.setTextandColor(p.name,Color.red);
        }
    }
    
    public float locationPosToDisplayPos(int l){
        return -displayWidth/2 + displayWidth*getLocationPercentage(l);
    }

    private float getDisplayPercentage(int l1, int l2){
        if(l2<l1){
            int temp = l2;
            l2 = l1;
            l1 = temp;
        }
        return (float) (l2-l1)/(float) (location2 - location1);
    }

    public void toggleChromosomeOverview()
    {
        if (chrOverview == null)
        {
            GameObject go = Instantiate(rm.chrOverviewPrefab);
            chrOverview = go.GetComponent<ChromosomeOverviewScript>();
            chrOverview.setup(this, chr);
        }
        else
        {
            chrOverview.gameObject.SetActive(!chrOverview.gameObject.activeSelf);
        }

        if (chrOverview.gameObject.activeSelf)
        {
            chrOverview.transform.position = transform.position - transform.right * transform.lossyScale.x*700f;
            chrOverview.transform.localScale = Vector3.one * 0.2f;

            //displayInterChromosomalInteractions(true);

        }
    }

    public void updateInteractionSearchDepth(int value)
    {
        if (chrOverview)
        {
            chrOverview.updateInteractionsSearchDepth(value);
        }
    }

    public void displayInteractions()
    {
         displayIntraChromosomalInteractions();
    }
    public void toggleFilterMenu()
    {
        if (filterMenu.gameObject.activeSelf)
        {
            filterMenu.gameObject.SetActive(false);
        }
        else
        {
            filterMenu.gameObject.SetActive(true);
            filterMenu.transform.localPosition = -Vector3.forward * 100;
        }

    }

    public void displayIntraChromosomalInteractions()
    {
        List<Interaction> allIntercationsInTheRange = new List<Interaction>();
        mapHolderScript.updateCorners();
        interactionHolderScript.updateCorners();
        foreach (KeyValuePair<IntervalTree.Interval<int>,Interaction> kvp in rm.connectionDataHandler.GetInteractionTree(chr).GetIntervalsOverlappingWith(new IntervalTree.Interval<int>(location1,location2)))
        {
            Interaction i = kvp.Value;
            if ((i.midA > location1 && i.midA < location2) || (i.midB > location1 && i.midB < location2))
            {


                if (((i.midA > location1 && i.midA < location2) && (i.midB > location1 && i.midB < location2)))
                {
                    allIntercationsInTheRange.Add(i);
                    if (getDisplayPercentage(i.midA, i.midB) > 0.01f)
                    {
                        interactionHolderScript.displayInteraction(i);
                    }
         
                }

                if (!(i.midA > location1 && i.midA < location2) || !(i.midB > location1 && i.midB < location2))
                {
                    mapHolderScript.displayInteraction(i);
                }

            }
        }
        mapHolderScript.deactivateUnusedInteractions();
        interactionHolderScript.deactivateUnusedInteractions();
        interactionDensityMapScript.update(allIntercationsInTheRange);
    }

    public void displayBasePairs()
    {
        if (!zoom)
        {
            basepairText.characterSpacing = 20;
        }

        basepairText.text = rm.database.GetDNA(chr, location1, location2);

        oldFontSize = basepairText.fontSize;

        while (oldFontSize == basepairText.fontSize)
        {
            basepairText.characterSpacing += 3;
            basepairText.ForceMeshUpdate();
        }

        basepairText.characterSpacing -= 3;
    }

    public float getLocationPercentage(int l)
    {
        return (float)(l - location1) / ((float)(location2 - location1));
    }

    public Vector3 locationPosToWorldPos(int l)
    {
        float percentage = (float)(l - location1) / (float)(location2 - location1);
        Vector3[] corners = new Vector3[4];
        titleHolderRect.GetLocalCorners(corners);
        return Vector3.Lerp(corners[0], corners[3], percentage);
    }
}
