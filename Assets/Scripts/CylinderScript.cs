﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylinderScript : MonoBehaviour
{

    public string chrName;

    private void Awake()
    {
        // Used when we can spawn any chr 
        //chrName = this.gameObject.transform.parent.parent.parent.gameObject.GetComponent<DisplayHandler>().chr;

        chrName = "chr1";
    }

    public Vector3 cylinderStart()
    {

        return transform.parent.parent.parent.transform.TransformPoint(this.gameObject.transform.localPosition + (-Vector3.right * this.gameObject.transform.localScale.y));
    }

    public Vector3 cylinderStop()
    {
        return transform.parent.parent.parent.transform.TransformPoint(this.gameObject.transform.localPosition + (Vector3.right * this.gameObject.transform.localScale.y));
    }


}
