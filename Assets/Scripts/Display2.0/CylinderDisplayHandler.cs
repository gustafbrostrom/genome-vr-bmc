﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using System.Linq;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class CylinderDisplayHandler : MonoBehaviour
{
    [Header("Settings")]

    public string chr;
    [Range(-2_500_000, 250_000_000)] public int l1;
    [Range(-2_500_000, 250_000_000)] public int l2;

    public bool ZoomIn;

    public bool ZoomOut;

    [Range(-2,2)] public float displayMode = 0;

    [HideInInspector] public float _displayMode = 0;

    public float DisplayMode{
        get {
            return this._displayMode;
        }
        set {
            if(this._displayMode != value){
                this._displayMode = value;
                updateMesh();
                redoMeshes();
                DisplaySlots();
                UpdatePositionForL1L2Text();
            }
        }
    }

    [Range(-1,1)] public int displayMode2 = 0;

    [HideInInspector] public int _displayMode2 = 0;

    public int DisplayMode2{
        get {
            return this._displayMode2;
        }
        set {
            if(this._displayMode2 != value){
                this._displayMode2 = value;
                updateMesh();
                redoMeshes();
                DisplaySlots();
                UpdatePositionForL1L2Text();
            }
        }
    }

    [Space]

    [Header("Dimensions")]
    [Range (0.1f, 1f)]public float radius = 0.5f;
    [Range (0.1f, 5f)]public float length = 0.5f;

    [Range (0f, 1f)]public float wrapLength = 0.5f;

    private float _wrapLength;

    public float WrapLength{
        get {
            return this._wrapLength;
        }
        set {
            if(this._wrapLength != value){
                this._wrapLength = value;
                updateMesh();
                //redoMeshes();
                UpdatePositionForL1L2Text();
            }
        }
    }
    private int nTracks;
    [Range(0,360)] public float rotation = 0;

    [Range(0.01f,0.2f)] public float handleSize = 0.05f;

    [Space]

    [Header("Handles")]
    public MeshFilter mf;

    public SlotComponent[] slotComponents;

    private bool settingsUpdated;

    private float[] latestHandPercentages = new float[]{float.NaN, float.NaN};
    public float xOffset = 0.05f;
    public float yOffset = 0.05f;

    ReferenceManager rm;

    TextMeshPro l1Text, l2Text;

    public bool redo_meshes = true;

    public ComputeShader computeShader;

    private ComputeBuffer inVertices;


    void Awake()
    {
        rm = FindObjectOfType<ReferenceManager>();
        UpdateComponentList();
        CreateTextMeshProForL1L2();
    }

    private void CreateTextMeshProForL1L2(){
        //Text
        Vector3 p1 = slotComponents[0].transform.TransformPoint(0,-0.15f,0.2f);
        Vector3 p2 = slotComponents[0].transform.TransformPoint(1,-0.15f,0.2f);
        p1 = transform.InverseTransformPoint(p1);
        p2 = transform.InverseTransformPoint(p2);

        GameObject go = new GameObject("L1 Text");
        go.transform.parent = transform;
        go.transform.localPosition = p1;
        go.transform.localScale = new Vector3(0.01f, 0.01f, 1);
        go.transform.localRotation = Quaternion.Euler(0,0,0);
        l1Text = go.AddComponent<TextMeshPro>();
        l1Text.enableAutoSizing = true;
        l1Text.fontSizeMax = 36;
        go = new GameObject("L2 Text");
        go.transform.parent = transform;
        go.transform.localPosition = p2;
        go.transform.localScale = new Vector3(0.01f, 0.01f, 1);
        go.transform.localRotation = Quaternion.Euler(0,0,0);
        l2Text = go.AddComponent<TextMeshPro>();
        l2Text.enableAutoSizing = true;
        l2Text.fontSizeMax = 36;
    }

    private void UpdatePositionForL1L2Text(){
        if(slotComponents.Length == 0)
            return;
        Vector3 p1 = slotComponents[0].convertVertex(0,-0.15f,0);
        Vector3 p2 = slotComponents[0].convertVertex(1,-0.15f,0);
        p1 = transform.InverseTransformPoint(p1);
        p2 = transform.InverseTransformPoint(p2);
        l1Text.transform.localPosition = p1;
        l2Text.transform.localPosition = p2;
    }

    private void AddEmptySlot()
    {
        GameObject go = new GameObject("EmptySlot");
        go.transform.parent = transform;
        
        EmptySlot slotComponent = go.AddComponent<EmptySlot>();
        UpdateComponentList();
        DisplaySlots();
    }

    public void UpdateComponentList(SlotComponent skip = null){

        List<SlotComponent> list = new List<SlotComponent>();
        int index = 0;
        foreach(SlotComponent s in GetComponentsInChildren<SlotComponent>()){
            if(ReferenceEquals(skip, s))
                continue;
            list.Add(s);
            s.slotIndex = index++;
        }
        slotComponents = list.ToArray();
        nTracks = slotComponents.Length;
        float step = 0;
        foreach(SlotComponent s in list)
        {
            s.wrapStart = step;
            step+=1f/nTracks;
            s.wrapEnd = step;
        }
        updateMesh();
    }


    public void OnValidate(){

        settingsUpdated = true;
        this.WrapLength = wrapLength;
        this.DisplayMode = displayMode;
        this.DisplayMode2 = displayMode2;
    }

    public void DebugMove(){
        transform.position = Camera.main.transform.position - Camera.main.transform.forward*0.25f;

    }

    // Update is called once per frame
    void Update()
    {
        if(redo_meshes){
            redo_meshes = false;
            redoMeshes();
        }

        if(Input.GetKeyDown(KeyCode.M))
            DebugMove();
            
        if(Application.isPlaying && settingsUpdated){
            Zoom();

            // if(DisplayMode == 2){
            //     transform.localScale = new Vector3(radius,length,radius);
            // }else{
            //     transform.localScale = new Vector3(length,radius,radius);
            // }
            

            DisplaySlots();
            settingsUpdated = false;
        }
    }

    public void redoMeshes(){
        foreach(SlotComponent s in slotComponents){
            s.CreateMesh();
        }
    }
    
    private void DisplaySlots(){
        l1Text.text = l1.ToString("N0");
        l2Text.text = l2.ToString("N0");
        foreach(SlotComponent s in slotComponents)
            s.Display();
    }


    //Kolla på för olika modes
    public float Percentage(Vector3 worldPosition)
    {
        Vector3 localPositioon = transform.InverseTransformPoint(worldPosition);
        return localPositioon.x/transform.localScale.x;
    }

    public void Zoom(Vector3 worldPosition0, Vector3 worldPosition1, int id = 0)
    {
        float h1 = Percentage(worldPosition0);
        float h2 = Percentage(worldPosition1);
        float oldh1 = latestHandPercentages[id];
        float oldh2 = latestHandPercentages[id + 1];

        if (!float.IsNaN(oldh1) && !float.IsNaN(oldh2))
        {
            //Redo so it doesnt do two updateLocations calls

            //translate first
            float scale = Mathf.Abs(h2 - h1) / Mathf.Abs(oldh2 - oldh1);
            float oldMid = (oldh2 + oldh1) / 2;
            float mid = (h2 + h1) / 2;
            int transDist = (int)((mid - oldMid) * (l2 - l1));
            l1 -= transDist;
            l2 -= transDist;

            //then scale
            int t1 = (int)(mid * (l2 - l1));
            int left = (int)(t1 * scale) - t1;

            int t2 = (int)((1 - mid) * (l2 - l1));
            int right = (int)(t2 * scale) - t2;
            l1 += left;
            l2 -= right;
        }
        latestHandPercentages[id] = h1;
        latestHandPercentages[id + 1] = h2;
        DisplaySlots();
    }

    public void Translation(Vector3 worldPosition, int id){        
        float percentage = Percentage(worldPosition);
        if(!float.IsNaN(latestHandPercentages[id])){
            float diff = percentage - latestHandPercentages[id];
            int distToMove = (int)(diff * (l2 - l1));
            l1 -= distToMove;
            l2 -= distToMove;
        }
        
        latestHandPercentages[id] = percentage;
        DisplaySlots();
    }

    public void ResetHand(int id){
        latestHandPercentages[id] = float.NaN;
    }

    private void Zoom(){
        if(ZoomIn){
            l1 += (l2-l1)/5;
            l2 -= (l2-l1)/5;
            ZoomIn = false;
        }
        if(ZoomOut){
            l1 -= (l2-l1)/7;
            l2 += (l2-l1)/7;
            ZoomOut = false;
        }
    }

    public void adjustWrap(int i){
        if(i>0){
            slotComponents[i-1].wrapEnd = slotComponents[i].wrapStart;
        }
        if(i<slotComponents.Length-1){
            slotComponents[i+1].wrapStart = slotComponents[i].wrapEnd;
        }
    }

    public void updateMesh(){
        Mesh mesh = mf.sharedMesh;
        mesh.Clear();

        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<int> tris = new List<int>();
        int vertIndex = 0;

        for(int trackIndex = 0; trackIndex <nTracks; trackIndex++)
        {
            Vector3 vv1 = new Vector3(-1,1 - (trackIndex/(float)nTracks)*2,0);
            Vector3 vv2 = new Vector3(-1,1 - ((trackIndex+1)/(float)nTracks)*2,0);

            int resX = 10; int resAng = 10;
            for (int x = 0; x < resX-1; x++)
            {   
                for (int y = 0; y < resAng-1; y++)
                {   
                    float yt1 = yOffset+((y-1)/(float)(resAng-3))*(1-2*yOffset);
                    float yt2 = yOffset+((y)/(float)(resAng-3))*(1-2*yOffset);
                    float xt1 = xOffset+((x-1)/(float)(resX-3))*(1-2*xOffset);
                    float xt2 = xOffset+((x)/(float)(resX-3))*(1-2*xOffset);
                    float uxt1 = 0.5f, uxt2 = 0.5f, uyt1 = 0.5f, uyt2 = 0.5f; 
                    if(y==0){
                        yt1 = 0;
                        yt2 = yOffset;
                        uyt1 = 0;
                    }else if(y == resAng-2){
                        yt1 = 1f-yOffset;
                        yt2 = 1f;
                        uyt2 = 1f;
                    }
                    if(x==0){
                        xt1 = 0;
                        xt2 = xOffset;
                        uxt1 = 0f;
                    }else if(x == resX-2){
                        xt1 = 1f-xOffset;
                        xt2 = 1f;
                        uxt2 = 1f;
                    }

                    Vector3 v1,v2,v3,v4;
                    v1 = Vector3.Lerp(vv1,vv2,yt1) + 2*Vector3.right*xt1;
                    v2 = Vector3.Lerp(vv1,vv2,yt2) + 2*Vector3.right*xt1;
                    v3 = Vector3.Lerp(vv1,vv2,yt1) + 2*Vector3.right*xt2;
                    v4 = Vector3.Lerp(vv1,vv2,yt2) + 2*Vector3.right*xt2;
                    
                    Vector2 u1 = new Vector2(uxt1,uyt1);
                    Vector2 u2 = new Vector2(uxt1,uyt2);
                    Vector2 u3 = new Vector2(uxt2,uyt1);
                    Vector2 u4 = new Vector2(uxt2,uyt2);
                   
                    //first triangle
                    //v1
                    uvs.Add(u1);
                    vertices.Add(v1);
                    vertIndex++;
                    //v4
                    uvs.Add(u4);
                    vertices.Add(v4);
                    vertIndex++;
                    //v2
                    uvs.Add(u2);
                    vertices.Add(v2);
                    vertIndex++;

                    tris.Add(vertIndex-3);
                    tris.Add(vertIndex-2);
                    tris.Add(vertIndex-1);

                    //second triangle
                    //v1
                    uvs.Add(u1);
                    vertices.Add(v1);
                    vertIndex++;
                    //v3
                    uvs.Add(u3);
                    vertices.Add(v3);
                    vertIndex++;
                    //v4
                    uvs.Add(u4);
                    vertices.Add(v4);
                    vertIndex++;

                    tris.Add(vertIndex-3);
                    tris.Add(vertIndex-2);
                    tris.Add(vertIndex-1);
                }
            }
        }
        mesh.vertices = transformVertices(vertices.ToArray());
        mesh.triangles = tris.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.RecalculateNormals();
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }

    public Vector3[] transformVertices(Vector3[] list){
        if(list.Length == 0 || DisplayMode == 0){
            return list;
        }
        int workGroupSize = Mathf.CeilToInt((float)list.Length/(8*8*8));
        if(workGroupSize>16){
            //print("workGroupSizeTooBig: " + workGroupSize + " " +  list.Length + " vertices");
            Vector3[] newList, list1, list2;
            list1 = transformVertices(list.Take(list.Length/2).ToArray());
            list2 = transformVertices(list.Skip(list.Length/2).ToArray());
            newList = new Vector3[list1.Length + list2.Length];
            list1.CopyTo(newList,0);
            list2.CopyTo(newList,list1.Length);
            return newList;
        }  
        inVertices = new ComputeBuffer(list.Length, sizeof(float)*3, ComputeBufferType.Default );
        inVertices.SetData(list, 0,0,list.Length);
        int kernel = computeShader.FindKernel("CSMain");
        computeShader.SetBuffer(kernel,"inVertices", inVertices);
        computeShader.SetInt("nWorkGroups", workGroupSize);
        computeShader.SetFloat("mode", DisplayMode);
        computeShader.SetInt("mode2", DisplayMode2);
        computeShader.Dispatch(kernel, workGroupSize,workGroupSize,workGroupSize);
        inVertices.GetData(list);
        inVertices.Dispose();
        return list;
    }

    public static void Swap<T> (ref T lhs, ref T rhs) {
        T temp = lhs;
        lhs = rhs;
        rhs = temp;
    }

    public Dictionary<string, dynamic> Menu()
    {
        UnityEventFloat ef;
        UnityEvent e;
        Dictionary<string, dynamic> subMenu; 

        Dictionary<string, dynamic> topMenu = new Dictionary<string, dynamic>();

        subMenu = new Dictionary<string, dynamic>();
        void ChangeDisplayMode(float percentage){
            float newDisplayMode = -1f + 2f*percentage;
            if(Mathf.Abs(newDisplayMode - DisplayMode) > 0.001)
                DisplayMode = newDisplayMode;
        }
        ef = new UnityEventFloat();
        ef.AddListener(ChangeDisplayMode);
        subMenu.Add("Displaymode", ef);

        void ChangeRadius(float percentage){
            float newRadius = percentage;
            if(Mathf.Abs(newRadius - radius) > 0.01)
                radius = newRadius;
        }

        ef = new UnityEventFloat();
        ef.AddListener(ChangeRadius);
        //subMenu.Add("Radius", ef);


        topMenu.Add("Display Mode", subMenu);

        e = new UnityEvent();
        e.AddListener(() => AddEmptySlot());

        topMenu.Add("Add Slot", e);

        return topMenu;
    }


}
