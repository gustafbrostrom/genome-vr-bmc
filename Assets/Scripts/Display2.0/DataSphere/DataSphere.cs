﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class DataSphere : MonoBehaviour
{
    public enum filetype
    {
        bigwig = 0,
        bed = 1,
        bedpe = 2,
        gtf = 3
    }
    TextMeshPro text;
    [SerializeField]
    private string filepath;
    public string Filepath
    {
        get { return filepath; }
        set
        {
            filepath = value;
            switch (filepath.Substring(filepath.LastIndexOf('.')))
            {
                case ".bw":
                    fileType = filetype.bigwig;
                    break;
                case ".bed":
                    fileType = filetype.bed;
                    break;
                case ".bedpe":
                    fileType = filetype.bedpe;
                    break;
                case ".gtf":
                    fileType = filetype.gtf;
                    break;
            }
            text.text = filepath;
            
        }
    }
    public filetype fileType;

    private Dictionary<filetype, System.Type> componentType;

    private bool alreadyEnteredComponent = false;

    // Start is called before the first frame update
    void Awake()
    {
        text = GetComponentInChildren<TextMeshPro>();
        text.text = filepath;
        componentType = new Dictionary<filetype, System.Type>(){
            {filetype.bigwig, typeof(BigWigDisplayScript)},
            {filetype.bed, typeof(Elements)},
            {filetype.bedpe, typeof(ConnectionHeatMapScript)},
            {filetype.gtf, typeof(Genes)}

        };
        

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collider)
    {
        if(alreadyEnteredComponent)
            return;

        SlotComponent slot = collider.GetComponent<SlotComponent>();
        if (slot != null)
        {
            alreadyEnteredComponent = true;
            print("popping " + collider.gameObject);
            slot.ReplaceComponent(componentType[fileType], filepath);
            Destroy(gameObject);
        }
    }
}
