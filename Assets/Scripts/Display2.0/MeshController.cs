﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshController : MonoBehaviour
{

    [Range (10, 1000)]public int resX = 100;
    [Range (0.1f, 1f)]public float maxWidth = 0.5f;

    [Range (0.1f, 5f)]public float length = 1f;
    [Range (12, 24)]public int resAng = 12;
    [Range (2, 30)]public int logFloat= 10;

    private float maxValue;
    public MeshFilter mf;

    public BigReader bigReader;

    bool settingsUpdated;
    void Start()
    {
        bigReader = new BigReader("ASCH_treat_pileup.srt.bw");
    }

    public void updateMesh(){
        List<float> b = (bigReader.ReadStats("chr1", 0, 100000000, resX));
        this.maxValue = 0f;
        foreach(float bb in b){
            if(bb>this.maxValue)
                this.maxValue = bb;
        }

        int nTriangles = (resX-1)*resAng*2;
        int nVertices = nTriangles*3;

        Mesh mesh = mf.mesh;
        mesh.Clear();
        Vector3[] vertices = new Vector3[nVertices];
        Vector3[] normals = new Vector3[nVertices];
        Vector2[] uvs = new Vector2[nVertices];
        int[] tris = new int[nTriangles*3];

        int vertIndex = 0;
        int trisIndex = 0;

        for (int x = 0; x < resX-1; x++)
        {
            for (int ang = 0; ang < resAng; ang++)
            {   
                Vector3 v1 = calcVec(x, ang, b[x]);
                Vector3 v2 = calcVec(x, ang+1, b[x]);
                Vector3 v3 = calcVec(x+1, ang, b[x+1]);
                Vector3 v4 = calcVec(x+1, ang+1, b[x+1]);

                Vector3 n1 = Vector3.Cross(v4-v1,v2-v1);
                Vector3 n2 = Vector3.Cross(v3-v1,v4-v1);

                Vector2 u1 = new Vector2(x/(float)(resX-1), ang/(float)(resAng));
                Vector2 u2 = new Vector2(x/(float)(resX-1), (ang+1)/(float)(resAng));
                Vector2 u3 = new Vector2((x+1)/(float)(resX-1), ang/(float)(resAng));
                Vector2 u4 = new Vector2((x+1)/(float)(resX-1), (ang+1)/(float)(resAng));

                //first triangle
                //v1
                normals[vertIndex] = n1;
                uvs[vertIndex] = u1;
                vertices[vertIndex++] = v1;
                //v4
                uvs[vertIndex] = u4;
                normals[vertIndex] = n1;
                vertices[vertIndex++] = v4;
                //v2
                uvs[vertIndex] = u2;
                normals[vertIndex] = n1;
                vertices[vertIndex++] = v2;

                tris[trisIndex++] = vertIndex-3;
                tris[trisIndex++] = vertIndex-2;
                tris[trisIndex++] = vertIndex-1;

                //second triangle
                //v1
                uvs[vertIndex] = u1;
                normals[vertIndex] = n2;
                vertices[vertIndex++] = v1;
                //v3
                uvs[vertIndex] = u3;
                normals[vertIndex] = n2;
                vertices[vertIndex++] = v3;
                //v4
                uvs[vertIndex] = u4;
                normals[vertIndex] = n2;
                vertices[vertIndex++] = v4;

                tris[trisIndex++] = vertIndex-3;
                tris[trisIndex++] = vertIndex-2;
                tris[trisIndex++] = vertIndex-1;
            }
        }
        mesh.vertices = vertices;
        mesh.triangles = tris;
        mesh.normals = normals;
        mesh.uv = uvs;
    }

    private Vector3 calcVec(int x, int ang, float b){
        float width = Mathf.Log((b/maxValue)*(logFloat-1f) + 1f,logFloat) + 0.001f;
        float xx = (float)x/(float)(resX-1);
        float yy = Mathf.Sin(Mathf.PI*2*(ang/(float)resAng))*width;
        float zz = Mathf.Cos(Mathf.PI*2*(ang/(float)resAng))*width;
        if(b == maxValue && ang == 0)
            print(width);
        return new Vector3(xx,yy,zz);
    }

    public void OnValidate(){
        settingsUpdated = true;
    }

    public void run(){
        
    }

    void Update()
    {
        if(Application.isPlaying && settingsUpdated){
            transform.localScale = new Vector3(length,maxWidth,maxWidth);
            updateMesh();
            settingsUpdated = false;
        }
    }

    // Mesh mesh = mf.mesh;
    // Vector3[] vertices = new Vector3[resX*resAng];
    // Vector3[] normals = new Vector3[resX*resAng];
    // int[] tris = new int[(resX-1)*(resAng)*2*3];

    // int vertIndex = 0;
    // int trisIndex = 0;

    // for (int x = 0; x < resX; x++)
    // {
    //     for (int ang = 0; ang < resAng; ang++)
    //     {
    //         float height= (Mathf.Log(b[x]+1,logFloat)/maxValue)*maxWidth;
    //         float y = Mathf.Sin(Mathf.PI*2*(ang/(float)resAng));
    //         float z = Mathf.Cos(Mathf.PI*2*(ang/(float)resAng));
    //         vertices[vertIndex] = new Vector3((x/(float)resX)*length,y*height,z*height);
            
    //         if(x<resX-1){
    //             float dh = (Mathf.Log(b[x+1]+1,logFloat)/maxValue)*maxWidth - height;
    //             normals[vertIndex++] = new Vector3(1/dh,z/dx,y/dx).normalized;
    //         }else{
    //             normals[vertIndex++] = new Vector3(0,z,y).normalized;
    //         }
    //         //normals[vertIndex++] = new Vector3(1,0,0);

    //         if(x<resX-1 && ang <resAng-1){
    //             //first triangle
    //             tris[trisIndex++] = resAng * x + ang;
    //             tris[trisIndex++] = resAng * x + ang + 1 + resAng;
    //             tris[trisIndex++] = resAng * x + ang + 1;

    //             //second triangle
    //             tris[trisIndex++] = resAng * x + ang;
    //             tris[trisIndex++] = resAng * x + ang + resAng;
    //             tris[trisIndex++] = resAng * x + ang + 1 + resAng;

    //         }else if(x<resX-1 && ang == resAng-1){
    //             //first triangle
    //             tris[trisIndex++] = resAng * x + ang;
    //             tris[trisIndex++] = resAng * x + ang + 1;
    //             tris[trisIndex++] = resAng * x + ang - (resAng-1);
                
    //             //second triangle
    //             tris[trisIndex++] = resAng * x + ang;
    //             tris[trisIndex++] = resAng * x + ang + resAng;
    //             tris[trisIndex++] = resAng * x + ang + 1;
                
    //         }
    //     }
    // }

    // mesh.vertices = vertices;
    // mesh.triangles = tris;
    // mesh.normals = normals;
}
