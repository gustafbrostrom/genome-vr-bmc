﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntervalTree;

[RequireComponent(typeof(Heatmap))]
public class Connections : MonoBehaviour
{
    ReferenceManager rm;
    string chr;
    int l1, l2;
    IntervalTree<int, Interaction> connections;
    Stack<LineRenderer> usedLines;
    Stack<LineRenderer> unusedLines;

    int pointsPerCurve = 21;
    Mesh mesh;
    Object linePrefab;
    Heatmap heatmap;
    [Range(0,190_000_000)]
    public int cutoff = 190_000_000;
    [Range(0.001f, 1f)]
    public float scale = 0.1f;

    public Vector3 direction = Vector3.right;

    void Start()
    {
        linePrefab = Resources.Load("Line", typeof(GameObject));
        usedLines = new Stack<LineRenderer>();
        unusedLines = new Stack<LineRenderer>();
        heatmap = GetComponent<Heatmap>();
        mesh = GetComponent<MeshFilter>().mesh;
        chr = "chr1";
        l1 = 0;
        l2 = 10_000_000;
        rm = GameObject.Find("ReferenceManager").GetComponent<ReferenceManager>();
        connections = rm.ConnectionData("ASCC.conns.bedpe").GetInteractionTree(chr);
        DisplayConnections();
    }

    void OnValidate(){
        //if(Application.isPlaying)
            //DisplayConnections();
    }
    
    Vector3[] CurvedLine(Vector3 a, Vector3 b){
        Vector3 up =  Vector3.forward;//Quaternion.Euler(0, -90, 0) * direction;
        Vector3[] points = new Vector3[pointsPerCurve];
        for (int i = 0; i < pointsPerCurve; i++)
        {
            Vector3 p = Vector3.Lerp(a, b, (float)i / (float)(pointsPerCurve - 1));
            p += (0.0f*up) + scale * (up) * Mathf.Sin(((float)i / (float)(pointsPerCurve - 1)) * Mathf.PI);
            points[i] = p;
        }
        return points;
    }
    void DisplayConnections()
    {

        while(usedLines.Count > 0){
            LineRenderer line = usedLines.Pop();
            line.widthMultiplier = 0f;
            unusedLines.Push(line);
        }

        Vector3 top = direction;
        Vector3 bottom = Vector3.zero;

        int nbases = l2 - l1;

        void Line(KeyValuePair<Interval<int>,Interaction> kvp){
            Interval<int> interval = kvp.Key;
            Interaction interaction = kvp.Value;
            LineRenderer line;
            if(unusedLines.Count > 0){
                line = unusedLines.Pop();
                line.widthMultiplier = 0.001f;

            }else{
                GameObject go = Instantiate(linePrefab, transform) as GameObject;
                line = go.GetComponent<LineRenderer>();
                line.positionCount = pointsPerCurve;
                line.useWorldSpace = false;
                line.widthMultiplier = 0.001f;
            }
            Vector3 a = Vector3.Lerp(bottom, top, (float)(interaction.midA - l1)/(float)nbases);
            Vector3 b = Vector3.Lerp(bottom, top, (float)(interaction.midB - l1)/(float)nbases);
            line.SetPositions(CurvedLine(a,b));
            usedLines.Push(line);

        }

        IEnumerable<KeyValuePair<Interval<int>, Interaction>> connectionsToDisplay = connections.GetIntervalsOverlappingWith(new Interval<int>(l1,l2));
        if(nbases < cutoff){
            heatmap.ClearTexture();
            foreach(KeyValuePair<Interval<int>,Interaction> kvp in connectionsToDisplay){
                Line(kvp);
            }
        }else{
            heatmap.UpdateTexture<Interaction>(connectionsToDisplay, l1, l2);
        }
        
    }
 
}
