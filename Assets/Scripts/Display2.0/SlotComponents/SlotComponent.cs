﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
public class SlotComponent : MonoBehaviour
{
    [HideInInspector] public int slotIndex;
    [Range(0, 1)] public float wrapStart;

    [Range(0, 1)] public float wrapEnd;

    protected CylinderDisplayHandler cylinderDisplayHandler;
    protected Renderer meshRenderer;
    [HideInInspector]
    public MeshFilter mf;

    private Object dataSpherePrefab;

    [Range(2, 1000)] public int resX = 10;

    [Range(2, 30)] public int resY = 10;

    protected TextMeshPro infoText;

    protected MeshCollider meshCollider;

    protected ReferenceManager rm;

    protected string dataPath = "";

    public virtual void Awake()
    {

        rm = FindObjectOfType<ReferenceManager>();
        dataSpherePrefab = Resources.Load("DataSphere", typeof(GameObject));
        cylinderDisplayHandler = GetComponentInParent<CylinderDisplayHandler>();

        //INFO TEXT
        GameObject go = new GameObject("Info");
        go.transform.parent = transform;
        go.transform.localRotation = Quaternion.Euler(180, 180, 180);
        go.transform.localScale = new Vector3(0.01f, 0.03f, 0.03f);
        infoText = go.AddComponent<TextMeshPro>();
        infoText.enableAutoSizing = true;
        infoText.fontSizeMax = 36;
        infoText.fontSizeMin = 12;
        infoText.alignment = TextAlignmentOptions.MidlineRight;
        infoText.outlineColor = Color.black;
        infoText.outlineWidth = 0.2f;

        BoxCollider bc = go.AddComponent<BoxCollider>();
        bc.size = new Vector3(20, 5, 0.1f);
        bc.isTrigger = true;
        Hoverable hoverable = go.AddComponent<Hoverable>();
        hoverable.onEnter = (() => infoText.color = Color.yellow);
        hoverable.onExit = (() => infoText.color = Color.white);

        ClickableObject clickable = go.AddComponent<ClickableObject>();
        clickable.onClick.AddListener(() => ReplaceComponent(typeof(EmptySlot)));

        meshRenderer = GetComponent<MeshRenderer>();
        mf = GetComponent<MeshFilter>();
        mf.sharedMesh = new Mesh();

        meshCollider = GetComponent<MeshCollider>();
    }

    public virtual void Setup(string datapath)
    {
        

        infoText.transform.localPosition = convertVertex(new Vector3(-0.2f, 0.5f, 0.01f));
        this.dataPath = datapath;
        infoText.text = dataPath;
    }

    public virtual void Update()
    {

    }

    void OnValidate()
    {

    }

    public Vector3 convertVertex(float x, float y, float z)
    {
        return convertVertex(new Vector3(x,y,z));
    }

    public Vector3 convertVertex(Vector3 v){
        float nSlots = (float)cylinderDisplayHandler.slotComponents.Length;
        float x = v.x*2 - 1;
        x = x*(1-2*cylinderDisplayHandler.xOffset);
        float y = v.y;
        y = cylinderDisplayHandler.yOffset+y*(1-2*cylinderDisplayHandler.yOffset);
        float z = -v.z;

        y = 1 - ((slotIndex + y)/nSlots)*2;
        return new Vector3(x,y,z);
    }

  

    public virtual void CreateMesh()
    {
        meshRenderer.sharedMaterial = new Material(Shader.Find("Standard"));

        Mesh mesh = mf.sharedMesh;
        mesh.Clear();

        int nVertices = resX * resY;
        int nTriangles = (resX - 1) * (resY - 1) * 2;
        Vector3[] vertices = new Vector3[nVertices];
        List<int> tris = new List<int>();
        Vector2[] uv = new Vector2[nVertices];
        float yt = 0;
        float xt = 0;

        for (int y = 0; y < resY; y++)
        {
            yt = (float)y / (resY - 1);
            for (int x = 0; x < resX; x++)
            {
                xt = (float)x / (resX - 1);
                vertices[y * resX + x] = convertVertex(new Vector3(xt, yt, 0));
                uv[y * resX + x] = new Vector3(xt, yt);
                if (x < resX - 1 && y < resY - 1)
                {
                    //Top triangle
                    tris.Add((y) * resX + (x));
                    tris.Add((y) * resX + (x + 1));
                    tris.Add((y + 1) * resX + (x + 1));

                    //Bot triangle
                    tris.Add((y) * resX + (x));
                    tris.Add((y + 1) * resX + (x + 1));
                    tris.Add((y + 1) * resX + (x));
                }
            }
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = tris.ToArray();
        mesh.RecalculateNormals();

        TransformMesh(mf);
        Display();
        meshCollider.sharedMesh = mf.mesh;
    }


    public virtual void Display()
    {

    }

    public void TransformMesh(MeshFilter mf){
        if(mf.GetComponent<TextMeshPro>() != null)
            return;
        Mesh mesh = mf.mesh;
        mesh.vertices = cylinderDisplayHandler.transformVertices(mesh.vertices);
    }

    public void TransformMesh(MeshFilter mf, Transform t){
        Mesh mesh = mf.sharedMesh;
        Vector3[] vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 v = vertices[i];
            v = t.TransformPoint(v);
            v = transform.InverseTransformPoint(v);
            vertices[i] = v;
        }
        vertices = cylinderDisplayHandler.transformVertices(vertices);
        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 v = vertices[i];
            v = transform.TransformPoint(v);
            v = t.InverseTransformPoint(v);
            vertices[i] = v;
        }
        mesh.vertices = vertices;
    }

    public virtual int getResX()
    {
        return resX;
    }

    public virtual int getResY()
    {
        return resY;
    }

    public virtual void Hover(Vector3 pos)
    {
        return;
    }

    public virtual void ReplaceComponent(System.Type replacement, string newDataPath = "")
    {

        if (replacement == typeof(Elements))
            transform.name = "BedReader";
        else if (replacement == typeof(Genes))
            transform.name = "GeneDisplay";
        else if (replacement == typeof(BigWigDisplayScript))
            transform.name = "BigWigDisplay";
        else if (replacement == typeof(ConnectionHeatMapScript))
            transform.name = "ConnectionDisplay";
        else if (replacement == typeof(EmptySlot))
            transform.name = "Empty Slot";
        else
            transform.name = "Error";

        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        //Instatiate the new slot component with all its children
        SlotComponent newSlotComponent = (SlotComponent)gameObject.AddComponent(replacement);
        newSlotComponent.wrapStart = wrapStart;
        newSlotComponent.wrapEnd = wrapEnd;
        newSlotComponent.slotIndex = slotIndex;
        newSlotComponent.Setup(newDataPath);

        if (this.dataPath != "")
        {
            Vector3 spawnPos = transform.TransformPoint(convertVertex(new Vector3(-0.2f, 0.2f, 0.2f)));
            GameObject go = Instantiate(dataSpherePrefab, spawnPos, Quaternion.identity) as GameObject;

            DataSphere newDataSphere = go.GetComponent<DataSphere>();
            newDataSphere.Filepath = dataPath;
        }
        
        Destroy(this);
        newSlotComponent.CreateMesh();
        newSlotComponent.Display();
        cylinderDisplayHandler.UpdateComponentList(this);

    }

}

