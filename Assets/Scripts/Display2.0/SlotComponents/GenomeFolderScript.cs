﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntervalTree;
using System.IO;

[RequireComponent(typeof(CurvedLineRenderer))]
public class GenomeFolderScript : MonoBehaviour
{
    public GameObject sphere1;

    public GameObject sphere2;

    private List<Vector3> points;

    private bool hovered = false;
    public CurvedLineRenderer lineRenderer;
    private GradientColorKey[] colorKey;

    private ConnectionHeatMapScript connectionHeatMapScript;
    private int orgl1 = 50335564, orgl2 = 55117448;
    void Awake()
    {
        sphere1 = GameObject.Find("Sphere");
        if(sphere1 == null)
        {
            sphere1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere1.transform.parent = transform;
            sphere1.transform.localScale = Vector3.one*0.05f;
        }
        sphere2 = GameObject.Find("Sphere (1)");
        if(sphere2 == null)
        {
            sphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere2.transform.parent = transform;
            sphere2.transform.localScale = Vector3.one*0.05f;
        }
        colorKey = new GradientColorKey[3];
        colorKey[0].color = Color.white;
        colorKey[0].time = 0.0f;
        colorKey[1].color = Color.red;
        colorKey[1].time = 0.0f;
        colorKey[2].color = Color.white;
        colorKey[2].time = 1.0f;

        connectionHeatMapScript = GetComponentInParent<ConnectionHeatMapScript>();
        lineRenderer = GetComponent<CurvedLineRenderer>();
        string filename = "chr11_10kb_gm12878_list_125mb_135mb_1596123314507.gss";
        points = new List<Vector3>();
        foreach (string line in File.ReadAllLines(@"FakeData\" + filename))
        {   
            if(!line.Contains("</un>")){
                continue;
            }
            int pFrom = line.IndexOf(">") + ">".Length;
            int pTo = line.LastIndexOf("</un>");

            string result = line.Substring(pFrom, pTo - pFrom);
            string[] coords = result.Split();
            
            points.Add(new Vector3(float.Parse(coords[0]),float.Parse(coords[1]),float.Parse(coords[2])));
        }
        //lineRenderer.positionCount = points.Count;
        lineRenderer.SetPoints(points.ToArray());
        //GetComponent<LineRenderer>().BakeMesh(connectionHeatMapScript.mf.mesh);
    }

    public void UpdateLocations(int l1, int l2){
        Gradient gradient = new Gradient();
        float dist = orgl2 - orgl1;
        if(dist == 0)
            return;

        if(colorKey == null)
            return;

        colorKey[0].time = Mathf.Clamp01((float)(l1 - orgl1)/dist);
        colorKey[1].time = Mathf.Clamp01((float)(l2 - orgl1)/dist);

        gradient.colorKeys = colorKey;
        gradient.mode = GradientMode.Fixed;
        GetComponent<LineRenderer>().colorGradient = gradient;


    }

    public void Update(){

    }

    public void Hover(int loc1, int loc2, float xt, float yt){
        int x = Mathf.FloorToInt(xt*(points.Count-1));
        int y = Mathf.FloorToInt(yt*(points.Count-1));
        float xRest = xt*(points.Count-1) - x; 
        float yRest = yt*(points.Count-1) - y; 
        Vector3 x1 = points[x];
        Vector3 x2 = points[Mathf.Min(x+1,points.Count-1)];
        Vector3 y1 = points[y];
        Vector3 y2 = points[Mathf.Min(y+1,points.Count-1)];
        //sphere1.transform.localPosition = Vector3.Lerp(x1,x2,xRest);
        //sphere2.transform.localPosition = Vector3.Lerp(y1,y2,yRest);
    }

    public void displayStructure(List<Vector3> points){
        // print("displaying");
        // lineRenderer.positionCount = points.Count;
        // lineRenderer.SetPositions(points.ToArray());
    }
}
