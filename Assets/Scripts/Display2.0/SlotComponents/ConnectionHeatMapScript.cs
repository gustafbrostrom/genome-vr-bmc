﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntervalTree;
using System.Threading.Tasks;
using System.IO;
using TMPro;

public class ConnectionHeatMapScript : SlotComponent
{

    [Range (1, 8)]public int squareSize = 2;

    [Range (0f, 1f)]public float separation = 0.1f;

    [Range (0f, 10f)]public float par0 = 0.1f;

    [Range (0f, 10f)]public float par1 = 0.1f;


    public bool update3DStructure = true;

    IntervalTree<int, Interaction> connections;

    private GenomeFolderScript genomeFolder;

    private Texture2D texture;

    private Color[] colorArray;

    public Transform sphere;


    // Awake is called before the first frame update
    public override void Awake()
    {
        base.Awake();
        genomeFolder = GetComponentInChildren<GenomeFolderScript>();
        if(false && genomeFolder == null)
        {
            GameObject go = new GameObject("GenomeFolder");
            go.transform.parent = transform;
            go.transform.localScale = Vector3.one*0.05f;
            go.transform.localPosition = Vector3.one*0.5f;
            genomeFolder = go.AddComponent<GenomeFolderScript>();
        }

        Setup("ASCH.conns.bedpe");

    }

    public override void Setup(string dataPath)
    {
        connections = rm.ConnectionData(dataPath).GetInteractionTree(cylinderDisplayHandler.chr);
        base.Setup(dataPath);  
    }

    public override void Hover(Vector3 pos)
    {
        if(colorArray != null)
            TintRowAndColumnInTexture(transform.InverseTransformPoint(pos));
    }


    public override void CreateMesh(){
        Mesh mesh = mf.sharedMesh;
        
        mesh.Clear();

        if(cylinderDisplayHandler.l1 >= cylinderDisplayHandler.l2)
            return;

        if(genomeFolder != null)
            genomeFolder.UpdateLocations(cylinderDisplayHandler.l1,cylinderDisplayHandler.l2);

        IEnumerable<KeyValuePair<Interval<int>, Interaction>> connectionsToDisplay = connections.GetIntervalsOverlappingWith(new Interval<int>(cylinderDisplayHandler.l1,cylinderDisplayHandler.l2));
        
        int ToMatrixCoordinates(int l){
            return Mathf.RoundToInt( (float)(l - cylinderDisplayHandler.l1)/(float)(cylinderDisplayHandler.l2 - cylinderDisplayHandler.l1)*(resX-1) );
        }
        
        float[,] densityMatrix = new float[resX,resX];
        float maxDensity = 1f;
        int nConnections = 0;
        foreach(KeyValuePair<Interval<int>, Interaction> kvp in connectionsToDisplay){
            Interaction i = kvp.Value;
            nConnections++;
            int AA = ToMatrixCoordinates(Mathf.Max(i.startA,cylinderDisplayHandler.l1));
            int AB = ToMatrixCoordinates(Mathf.Min(i.endA,cylinderDisplayHandler.l2));
            int BA = ToMatrixCoordinates(Mathf.Max(i.startB,cylinderDisplayHandler.l1));
            int BB = ToMatrixCoordinates(Mathf.Min(i.endB,cylinderDisplayHandler.l2));
            
            for(int y = AA; y<=AB; y++){
                for(int x = BA; x<=BB; x++){
                    densityMatrix[Mathf.Max(x,y),Mathf.Min(x,y)] += 1f;
                    if(densityMatrix[x,y]>maxDensity)
                        maxDensity = densityMatrix[x,y];
                } 
            }
        }

        Vector3 vv1 = new Vector3(0,1,0);
        Vector3 vv2 = new Vector3(0.5f,0,0);
        Vector3 vv3 = new Vector3(1,1,0);
        Vector3 yDir = vv3-vv2;

        Vector2 uv1 = new Vector2(0,1);
        Vector2 uv2 = new Vector2(1,1);
        Vector2 uv3 = new Vector2(1,0);

        int nPointsAlongAxis = resX*squareSize + 2 + squareSize;
        int nVertices = (nPointsAlongAxis*nPointsAlongAxis + nPointsAlongAxis)/2;
        int nTriangles = (nPointsAlongAxis-1)*(nPointsAlongAxis-1);
        Vector3[] vertices = new Vector3[nVertices];
        Vector2[] uvs = new Vector2[nVertices];
        Color[] colors = new Color[nVertices];
        int[] tris = new int[nTriangles*3];
        int vertexIndex = 0;
        int trisIndex = 0;
        float t = 0;
        float dx = 0;
        float dy = 0;
        int xx = 0;
        int yy = 0;

        float step = 1f / ( separation * (resX+2) + (2f-separation) * (resX+1) );
        for(int y = 0; y<nPointsAlongAxis; y++){
            dx = dy;
            for(int x = y; x<nPointsAlongAxis; x++){
                Vector3 vertex = Vector3.Lerp(vv1,vv2,dx) + yDir * dy;

                
                
                if(x<nPointsAlongAxis-1){
                    //Top triangle
                    tris[trisIndex++] = vertexIndex;
                    tris[trisIndex++] = vertexIndex+1;
                    tris[trisIndex++] = vertexIndex+nPointsAlongAxis-y;

                    if(x>y){
                        //Bottom triangle
                        tris[trisIndex++] = vertexIndex;
                        tris[trisIndex++] = vertexIndex+nPointsAlongAxis-y;
                        tris[trisIndex++] = vertexIndex+nPointsAlongAxis-1-y;
                    }
                }
                
                if( ( y > 0 ) && ( x > squareSize + Mathf.FloorToInt((y-1)/squareSize)*squareSize ) && ( x < nPointsAlongAxis-1 ) ){
                    xx = x-1-squareSize;
                    yy = y-1;
                    t = densityMatrix[Mathf.FloorToInt(xx/squareSize),Mathf.FloorToInt(yy/squareSize)]/maxDensity;
                    t = Mathf.Log(1f+t*par0,par1);
                    vertex += new Vector3(0,0,t/10f);
                }
                
                colors[vertexIndex] = Color.cyan;

                uvs[vertexIndex] = new Vector2((x)/((float)(nPointsAlongAxis-1)),(y)/((float)(nPointsAlongAxis-1)));
                vertices[vertexIndex++] = convertVertex(vertex);

                if((y+x+y)%2 == 0){
                    dx += step*separation;
                }else{
                    dx += step*(2f-separation);
                }

            }

            if(y%2 == 0){
                dy += step*separation;
            }else{
                dy += step*(2f-separation);
            }
        }
        mesh.vertices = vertices;
        mesh.triangles = tris;
        mesh.RecalculateNormals();
        mesh.colors = colors;
        mesh.uv = uvs;
        
        colorArray = new Color[(nPointsAlongAxis-1)*(nPointsAlongAxis-1)];
        t = 0;

        for(int y= 0; y<nPointsAlongAxis-1; y++){
            for(int x = 0; x<nPointsAlongAxis-1; x++){
                Color c = new Color(0,0,0,0);
                if( (y+1)%2==0 && (x+1)%2==0 && x>=y+2 ){
                    xx = x-1-squareSize;
                    yy = y-1;
                    t = densityMatrix[Mathf.FloorToInt(xx/squareSize),Mathf.FloorToInt(yy/squareSize)]/maxDensity;
                    c = new Color(t,1f-t,0,1f);
                }
                colorArray[(nPointsAlongAxis-1)*y+x] = c;
            } 
        }
        
        texture = new Texture2D(nPointsAlongAxis-1, nPointsAlongAxis-1);
        texture.SetPixels(colorArray);
        texture.Apply();
        texture.filterMode = FilterMode.Point;
        GetComponent<Renderer>().material.mainTexture = texture;
        /*
        if(update3DStructure){
            StartCoroutine(structure(densityMatrix));
            update3DStructure = false;
        }
        */
        TransformMesh(mf);
        meshCollider.sharedMesh = mf.sharedMesh;
    }

    public void TintRowAndColumnInTexture(Vector3 pos){

        Vector3 vv1 = new Vector3(0,1,0);
        Vector3 vv2 = new Vector3(0.5f,0,0);
        Vector3 vv3 = new Vector3(1,1,0);

        Vector3 xDir = vv2-vv1;
        Vector3 yDir = vv3-vv2;
        Vector3 v = pos-vv1;

        xDir.Scale(new Vector3(2,1,1));
        yDir.Scale(new Vector3(2,1,1));

        v.Scale(new Vector3(2,1,1));

        int nPointsAlongAxis = resX*squareSize + 2 + squareSize;


        float xt = Mathf.Clamp01(Vector3.Project(v,xDir).magnitude/xDir.magnitude);
        float yt = Mathf.Clamp01(Vector3.Project(v,yDir).magnitude/yDir.magnitude);

        int xx = 1 + Mathf.RoundToInt(resX*xt)*squareSize; 
        int yy = 1 + Mathf.RoundToInt(resX*yt)*squareSize;
        
        Color[] tempColors = (Color[]) colorArray.Clone();

        for(int y = 1 ; y<=xx-1; y++){
            for(int x = xx; x<xx+squareSize; x++){
                if(y%2==1 && x%2==1){
                    Color c = colorArray[(nPointsAlongAxis-1)*y+x];
                    tempColors[(nPointsAlongAxis-1)*y+x] = Color.Lerp(c,Color.blue,0.5f);
                }

            }
        }
        for(int x = yy+1; x<(nPointsAlongAxis-1)-1; x++){
            for(int y = yy; y<yy+squareSize; y++){
                if(y%2==1 && x%2==1){
                    Color c = colorArray[(nPointsAlongAxis-1)*y+x];
                    tempColors[(nPointsAlongAxis-1)*y+x] = Color.Lerp(c,Color.blue,0.5f);
                }
            }
        }

        texture.SetPixels(tempColors);
        texture.Apply();
        GetComponent<Renderer>().material.mainTexture = texture;

        int loc1 = Mathf.RoundToInt(cylinderDisplayHandler.l1 + xt * (cylinderDisplayHandler.l2-cylinderDisplayHandler.l1));
        int loc2 = Mathf.RoundToInt(cylinderDisplayHandler.l1 + yt * (cylinderDisplayHandler.l2-cylinderDisplayHandler.l1));
        if(genomeFolder != null)
            genomeFolder.Hover(loc1, loc2, xt, yt);
    }
    public IEnumerator structure(float[,] densityMatrix){
            Controller3DMax m = new Controller3DMax(densityMatrix, cylinderDisplayHandler.l1, cylinderDisplayHandler.l2);
            Task<List<Vector3>> t = m.Generate3DStructure();
            print("done");
            
            while(!t.IsCompleted) 
                yield return null;
            genomeFolder.displayStructure(t.Result);

    }

    public override void Display(){
        CreateMesh();
    }

    public static void Swap<T> (ref T lhs, ref T rhs) {
        T temp = lhs;
        lhs = rhs;
        rhs = temp;
    }

    public override int getResY(){
        return resX;
    }
}
