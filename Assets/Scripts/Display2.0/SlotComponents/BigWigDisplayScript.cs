﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BigWigDisplayScript : SlotComponent
{
    [Range (0.1f, 1f)]public float maxHeight = 0.1f;
    [Range (2, 30)]public int logFloat = 30;
    private BigReader bigReader;

    // Awake is called before the first frame update
    public override void Awake()
    {
        base.Awake();

        Setup("ASCH_treat_pileup.srt.bw");
    }

    public override void Setup(string datapath)
    {
        if(bigReader != null)
        {
            bigReader.CloseConnection();
        }
        bigReader = new BigReader(datapath);
        resX = 30;
        resY = 30;
        meshRenderer.sharedMaterial = Resources.Load<Material>("ButtonHighlight");
        base.Setup(datapath);
    }

    public override void Update(){
        base.Update();
    }
    public override void Hover(Vector3 pos)
    {
        return;
    }
    public override void CreateMesh(){
        List<float> b = bigReader.ReadStats(cylinderDisplayHandler.chr,cylinderDisplayHandler.l1,cylinderDisplayHandler.l2,resX);
        float maxValue = 0f;
        foreach(float bb in b){
            if(bb>maxValue)
                maxValue = bb;
        }

        int nTriangles = (resX-1)*(resY-1)*2;
        int nVertices = resX*resY;

        Mesh mesh = mf.sharedMesh;
        mesh.Clear();
        Vector3[] vertices = new Vector3[nVertices];
        Vector2[] uvs = new Vector2[nVertices];
        int[] tris = new int[nTriangles*3];

        int vertIndex = 0;
        int trisIndex = 0;

        for (int x = 0; x < resX; x++)
        {
            for (int y = 0; y < resY; y++)
            {   
                Vector3 v1 = calcVec(x, y, b[x],maxValue);
                Vector2 u1 = new Vector2(x/(float)(resX-1), (resY-1-y)/(float)(resY-1));

                uvs[vertIndex] = u1;
                vertices[vertIndex++] = convertVertex(v1);//transformVertex(transform, v1);

                if(x<resX-1 && y < resY-1){
                //first triangle
                tris[trisIndex++] = (x)*resY + y;
                tris[trisIndex++] = (x+1)*resY + y+1;
                tris[trisIndex++] = (x)*resY + y+1;

                //second triangle
                tris[trisIndex++] = (x)*resY + y;
                tris[trisIndex++] = (x+1)*resY + y;
                tris[trisIndex++] = (x+1)*resY + y+1;
                }
            }
        }
        mesh.vertices = vertices;
        mesh.triangles = tris;
        mesh.RecalculateNormals();
        mesh.uv = uvs;

        TransformMesh(mf);
        meshCollider.sharedMesh = mf.sharedMesh;
    }

    private Vector3 calcVec(int x, int y, float b, float maxValue){
        float height;
        if(maxValue > 0)
            height = Mathf.Log((b/maxValue)*(logFloat-1f) + 1f,logFloat);
        else
            height = 0;
        float xx = x/(float)(resX-1);
        float yy = y/(float)(resY-1);
        float zz = (Mathf.Sin(Mathf.PI*2*(y/(float)(resY-1)-0.25f))+1f)* height*maxHeight;
        return new Vector3(xx,yy,zz);
    }

    public override void Display(){
        CreateMesh();
    }



    public override void ReplaceComponent(System.Type replacement, string newDataPath = "")
    {
        meshRenderer.sharedMaterial = new Material(Shader.Find("Standard"));
        base.ReplaceComponent(replacement, newDataPath);

    }
}
