﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntervalTree;
using TMPro;

public class Genes : SlotComponent
{
    IntervalTree<int, GeneV2> genes;
    string currentchr;
    private Texture2D texture;
    [Range(1, 1000)] public int textureWidth = 1000;
    [Range(1, 1000)] public int textureHeight = 1;


    public Color baseColor = Color.black;
    private Color[] baseArray;
    Object textPrefab;
    Stack<TextMeshPro> usedText;
    Stack<TextMeshPro> unusedText;


    public override void Awake()
    {
        base.Awake();
        usedText = new Stack<TextMeshPro>();
        unusedText = new Stack<TextMeshPro>();
        textPrefab = Resources.Load("FloatingText", typeof(GameObject));

        texture = new Texture2D(textureWidth, textureHeight);
        texture.filterMode = FilterMode.Point;
        baseArray = new Color[textureWidth * textureHeight];
        for (int i = 0; i < textureWidth * textureHeight; ++i)
        {
            baseArray[i] = baseColor;
        }

        Setup("mm10.refGene.gtf");
    }

    public override void CreateMesh()
    {
        base.CreateMesh();
        meshRenderer.material.mainTexture = texture;
 
    }

    public override void Display()
    {
        if(this == null)
            return;

        int l1 = cylinderDisplayHandler.l1;
        int l2 = cylinderDisplayHandler.l2;
        if (l1 >= l2)
            return;

        string chr = cylinderDisplayHandler.chr;

        float percentage(int l)
        {
            return (((float)(l - l1)) / ((float)(l2 - l1)));
        }
        if(usedText == null)
            return;
        while (usedText.Count > 0)
        {
            TextMeshPro text = usedText.Pop();
            text.text = "";
            unusedText.Push(text);
        }

        if (chr != currentchr)
        {
            currentchr = chr;
            genes = rm.RefGene(dataPath).GetGeneTree(currentchr);
        }

        Color[] colorArray = (Color[])baseArray.Clone();
        int lastl2 = 0;
        int row = 0;
        bool displayExons = true;
        foreach (var kvp in genes.GetIntervalsOverlappingWith(new Interval<int>(l1, l2)))
        {
            GeneV2 gene = kvp.Value;
            Interval<int> interval = kvp.Key;

            float perl1 = percentage(Mathf.Max(l1, interval.Start));
            float perl2 = percentage(Mathf.Min(l2, interval.End));

            float percentageOfDisplay = perl2 - perl1;


            int texl1 = (int)(perl1 * textureWidth);
            int texl2 = (int)(perl2 * textureWidth);



            if (interval.Start < lastl2)
            {
                row++;
                row %= (textureHeight);
            }
            else
            {
                row = 0;
            }

            if (percentageOfDisplay > 0.01)
            {
                TextMeshPro text;
                if (unusedText.Count == 0)
                {
                    GameObject go = Instantiate(textPrefab, transform) as GameObject;
                    go.transform.localScale *= 0.005f;
                    text = go.GetComponent<TextMeshPro>();
                }
                else
                {
                    text = unusedText.Pop();
                }
                float textBoxHeight = text.rectTransform.rect.height * text.transform.localScale.y;
                Vector3 textPos = convertVertex(new Vector3((perl2 + perl1) / 2f, (float)(2 * row + 1) * (1f / (float)(2 * textureHeight)), 0.01f));
                text.transform.localPosition = textPos;
                text.text = gene.Name;

                text.ForceMeshUpdate();

                TransformMesh(text.GetComponent<MeshFilter>(),text.transform);
                

                text.rectTransform.sizeDelta = new Vector2(percentageOfDisplay*(1f/0.0005f), 5);
                usedText.Push(text);

            }
            Color geneColor = new Color(gene.R, gene.G, gene.B, 1f);
            lastl2 = interval.End;

            //EXONS
            int exonIndex = 0;
            int exonTexl1 = 0, exonTexl2 = 0;
            if (exonIndex < gene.Exons.Count)
            {
                exonTexl1 = (int)(percentage(gene.Exons[exonIndex].L1) * textureWidth);
                exonTexl2 = (int)(percentage(gene.Exons[exonIndex].L2) * textureWidth);
            }
            Color exonColor = Color.Lerp(geneColor, Color.black, 0.1f);

            for (int i = texl1; i <= texl2 && i < textureWidth; ++i)
            {
                //Förutsätter att exoner är sorterade 
                if (displayExons && exonIndex < gene.Exons.Count && exonTexl1 <= i)
                {
                    colorArray[i + textureWidth * row] = exonColor;

                    if (exonTexl2 >= i)
                    {
                        exonIndex++;
                        if (exonIndex >= gene.Exons.Count)
                            continue;
                        exonTexl1 = (int)(percentage(gene.Exons[exonIndex].L1) * textureWidth);
                        exonTexl2 = (int)(percentage(gene.Exons[exonIndex].L2) * textureWidth);
                    }
                }
                else
                {
                    colorArray[i + textureWidth * row] = geneColor;
                }
            }

            
        }
        texture.SetPixels(colorArray);
        texture.Apply();
    }

    
    public override void ReplaceComponent(System.Type replacement, string newDataPath = "")
    {
        texture.SetPixels(baseArray);
        texture.Apply();
        base.ReplaceComponent(replacement, newDataPath);

    }


}
