﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntervalTree;
using TMPro;

public class Elements : SlotComponent
{
    IntervalTree<int, Element> elements;
    string currentchr;
    private Texture2D texture;
    [Range(1, 1000)] public int textureWidth = 1000;
    [Range(1, 1000)] public int textureHeight = 1;


    public Color baseColor = Color.yellow;
    private Color[] baseArray;
    Object textPrefab;
    Stack<TextMeshPro> usedText;
    Stack<TextMeshPro> unusedText;


    public override void Awake()
    {
        base.Awake();

        usedText = new Stack<TextMeshPro>();
        unusedText = new Stack<TextMeshPro>();
        textPrefab = Resources.Load("FloatingText", typeof(GameObject));

        //cylinderDisplayHandler.transformsThatLookAtCamera.Add(go.transform);

        texture = new Texture2D(textureWidth, textureHeight);
        texture.filterMode = FilterMode.Point;
        meshRenderer.material.mainTexture = texture;
        baseArray = new Color[textureWidth * textureHeight];
        for (int i = 0; i < textureWidth * textureHeight; ++i)
        {
            baseArray[i] = baseColor;
        }

        Setup("ASCH.cCRE.bed");

    }
    public override void CreateMesh()
    {
        base.CreateMesh();
        meshRenderer.material.mainTexture = texture;
 
    }


    public override void Display()
    {
        int l1 = cylinderDisplayHandler.l1;
        int l2 = cylinderDisplayHandler.l2;
        if (l1 >= l2)
            return;

        string chr = cylinderDisplayHandler.chr;

        float percentage(int l)
        {
            return (((float)(l - l1)) / ((float)(l2 - l1)));
        }

        while (usedText.Count > 0)
        {
            TextMeshPro text = usedText.Pop();
            text.text = "";
            unusedText.Push(text);
        }

        if (chr != currentchr)
        {
            currentchr = chr;
            elements = rm.BedReader(dataPath).GetTree(currentchr);
        }

        Color[] colorArray = (Color[])baseArray.Clone();
        int lastl2 = 0;
        int row = 0;
        foreach (var kvp in elements.GetIntervalsOverlappingWith(new Interval<int>(l1, l2)))
        {
            Element gene = kvp.Value;
            Interval<int> interval = kvp.Key;

            if (interval.Start < lastl2)
            {
                row++;
                row %= (textureHeight);
            }
            else
            {
                row = 0;
            }

            float perl1 = percentage(Mathf.Max(l1, interval.Start));
            float perl2 = percentage(Mathf.Min(l2, interval.End));

            float percentageOfDisplay = perl2 - perl1;
            int texl1 = (int)(perl1 * textureWidth);
            int texl2 = (int)(perl2 * textureWidth);

            if (percentageOfDisplay > 0.01)
            {
                TextMeshPro text;
                if (unusedText.Count == 0)
                {
                    GameObject go = Instantiate(textPrefab, transform) as GameObject;
                    go.transform.localScale *= 0.005f;
                    text = go.GetComponent<TextMeshPro>();
                }
                else
                {
                    text = unusedText.Pop();
                }
                float textBoxHeight = text.rectTransform.rect.height * text.transform.localScale.y;
                Vector3 textPos = new Vector3((perl2 + perl1) / 2f, (float)(2 * row + 1) * (1f / (float)(2 * textureHeight)) + Mathf.RoundToInt(2 * Random.value - 1) * textBoxHeight, 0.0001f);
                text.transform.localPosition = textPos;
                text.text = gene.tag;

                text.rectTransform.sizeDelta = new Vector2(percentageOfDisplay*(1f/0.0005f), 5);
                usedText.Push(text);
            }
            lastl2 = interval.End;



            for (int i = texl1; i <= texl2 && i < textureWidth; ++i)
            {

                colorArray[i + textureWidth * row] = Color.blue;

            }
        }
        texture.SetPixels(colorArray);
        texture.Apply();
    }
    public override void ReplaceComponent(System.Type replacement, string newDataPath = "")
    {
        texture.SetPixels(baseArray);
        texture.Apply();
        base.ReplaceComponent(replacement, newDataPath);

    }


}
