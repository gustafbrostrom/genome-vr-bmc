﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Globalization;

public class Controller3DMax 
{
    public List<Vector3> result;
    private string parameters_path;
    private string hashcode;
    private string work_folder = @"External_tools\3DMax\temp\";
    private string output_folder;
    Process p;
    public Controller3DMax(float[,] IF, int l1, int l2){
        

        int interval = l2 - l1;
        int bin_size = interval/IF.GetLength(0);

        if(!Directory.Exists(work_folder))
            Directory.CreateDirectory(work_folder);
        
        foreach(string s in Directory.EnumerateDirectories(work_folder))
            Directory.Delete(s, true);
            
            
        hashcode = GetHashCode().ToString();
        work_folder += (hashcode + @"\");

        if(!Directory.Exists(work_folder))
            Directory.CreateDirectory(work_folder);

        parameters_path = Path.GetFullPath(work_folder + "parameters.txt");
        string matrix_path = Path.GetFullPath(work_folder + "matrix.gbmat");
        using (StreamWriter writer = new StreamWriter(matrix_path))  {
            for(int j = 0; j < IF.GetLength(1); j++){

            for(int i = 0; i < IF.GetLength(0); i++){
                    if(IF[i,j] > 0){
                        writer.WriteLine((l1 + bin_size*i) + "\t" + (l1 + bin_size*j) + "\t" + IF[i,j]);
                    }

                }
            }
        }

        output_folder = Path.GetFullPath(work_folder + "output");

        if(!Directory.Exists(output_folder))
            Directory.CreateDirectory(output_folder);

        using (StreamWriter writer = new StreamWriter(parameters_path)) {
            writer.WriteLine("NUM = 1");
            writer.WriteLine("OUTPUT_FOLDER = " + output_folder);
            writer.WriteLine("INPUT_FILE = " + matrix_path);
            //writer.WriteLine("CONVERT_FACTOR = 0.6");
            writer.WriteLine("VERBOSE = true");
            writer.WriteLine("LEARNING_RATE = 1");
            writer.WriteLine("MAX_ITERATION = 1000");
        }
    }
    public async Task<List<Vector3>> Generate3DStructure(){
        string jarPath = Path.GetFullPath(@"External_tools\3DMax\3Dmax.jar");
        UnityEngine.Debug.Log(jarPath);
        p = new Process();
        ProcessStartInfo startInfo = new ProcessStartInfo();
        startInfo.UseShellExecute = false;
        startInfo.RedirectStandardInput = false;
        startInfo.RedirectStandardOutput = false;
        startInfo.RedirectStandardError = false;
        startInfo.WindowStyle = ProcessWindowStyle.Normal;
        startInfo.Arguments = @"-jar "+ jarPath + " " + parameters_path;

        startInfo.FileName = "java";
        p.StartInfo = startInfo;
        p.Start();
        p.WaitForExit();
  
        return ParseCoordinates();
    }

    public List<Vector3> ParseCoordinates(){
        string[] files = Directory.GetFiles(output_folder);
        string pdb_path = "";
        foreach(string s in files){
            if(s.EndsWith(".pdb")){
                pdb_path = s;
                break;
            }
        }

        List<Vector3> points = new List<Vector3>();
        using(StreamReader file = new StreamReader(pdb_path)) {  
            string ln = file.ReadLine();  
            float x,y,z;
            while ((ln = file.ReadLine()).StartsWith("ATOM")) {  

                x = float.Parse(ln.Substring(30,8).Replace(',','.'))/10f;
                y = float.Parse(ln.Substring(38,8).Replace(',','.'))/10f;
                z = float.Parse(ln.Substring(46,8).Replace(',','.'))/10f;
                points.Add(new Vector3(x,y,z));
            }  
            file.Close();  
        } 
        return points;
    }


}
