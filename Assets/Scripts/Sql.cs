﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using System;
using System.Linq;

using Mono.Data.Sqlite;
using RangeTree;
using System.Diagnostics;
using System.Data.SqlClient;

public class Sql : MonoBehaviour
{
    private string dbPath = "URI=file:Data/boi.db";
    private string interactionsPath = "URI=file:Data/interactions_1000.db";

    private SqliteConnection proteinBindingConnection;
    private SqliteConnection genesConnection;
    private SqliteConnection intraInteractionsConnection;
    private ReferenceManager rm;

    void Awake()
    {
        rm = GameObject.Find("ReferenceManager").GetComponent<ReferenceManager>();

        proteinBindingConnection = new SqliteConnection("URI=file:Data/ProteinBindingsSortedByChromosome_split.db");
        proteinBindingConnection.Open();

        genesConnection = new SqliteConnection("URI=file:Data/genes.db");
        genesConnection.Open();

        intraInteractionsConnection = new SqliteConnection("URI=file:Data/interactions_1000.db");
        intraInteractionsConnection.Open();
    }

    void OnDisable()
    {
        proteinBindingConnection.Close();
        genesConnection.Close();
        intraInteractionsConnection.Close();
    }

    public string GetDNA(string chr, int l1, int l2) {
        if (l2 < 1)
            return "";
        string ret = "";
        using (var conn = new SqliteConnection(dbPath)) {
            conn.Open();
            using (var cmd = conn.CreateCommand()) {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM " + chr + "_DNA WHERE id >= " + (((l1 - 1) / 50) + 1) + " and id <= " + (((l2 - 1) / 50) + 1) + ";";

                var reader = cmd.ExecuteReader();
                while (reader.Read()) {

                    int id = reader.GetInt32(0);
                    string basepair = reader.GetString(1);
                    ret += basepair;

                }

            }
        }
        return ret.Substring(((l1 - 1) % 50), (l2 - l1) + 1);
    }

    public List<Gene> GetGenes(string chr, int l1 = -1, int l2 = -1) {
        List<Gene> ret = new List<Gene>();
        using (var conn = new SqliteConnection(dbPath)) {
            conn.Open();
            using (var cmd = conn.CreateCommand()) {
                cmd.CommandType = CommandType.Text;
                if (l1 == -1 && l2 == -1)
                    cmd.CommandText = "SELECT * FROM " + chr + " ;";
                else
                    cmd.CommandText = "SELECT * FROM " + chr + " WHERE l1 >= " + l1 + " AND l2 <= " + l2 + ";";
                var reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    int id = reader.GetInt32(0);
                    string url = reader.GetString(1);

                    string name = reader.GetString(2);
                    string dir = reader.GetString(3);
                    int loc1 = reader.GetInt32(4);
                    int loc2 = reader.GetInt32(5);
                    int dirint = (dir == "+") ? 1 : 0;
                    int[] lowerex = Array.ConvertAll(reader.GetString(8).Split(',').Take(reader.GetString(8).Split(',').Count() - 1).ToArray(), int.Parse);
                    int[] upperex = Array.ConvertAll(reader.GetString(9).Split(',').Take(reader.GetString(9).Split(',').Count() - 1).ToArray(), int.Parse);
                    int[,] boi = new int[lowerex.Count(), 2];
                    for (int i = 0; i < lowerex.Count(); i++) {
                        boi[i, 0] = lowerex[i];
                        boi[i, 1] = upperex[i];
                    }


                    ret.Add(new Gene(id, chr,url, name, loc1, loc2, dirint, boi));

                }
                
            }
        }
        return ret;
    }

    public Dictionary<string, List<Interaction>> GetInterInteractions(string chr, int l1 = -1, int l2 = -1)
    {
        Dictionary<string, List<Interaction>> interactions = new Dictionary<string, List<Interaction>>();

        foreach (string c in rm.chrNameToNumber.Keys)
        {
            using (var cmd = intraInteractionsConnection.CreateCommand())
        {
                List<Interaction> list = new List<Interaction>();
                cmd.CommandType = CommandType.Text;
                if(l1 != -1 && l2 != -1)
                    cmd.CommandText = "SELECT * FROM " + chr + "_inter_" + c + " WHERE MidA BETWEEN "+l1+" AND "+l2+" ORDER BY MidB ASC;";
                else
                    cmd.CommandText = "SELECT * FROM " + chr + "_inter_" + c + " ORDER BY MidB ASC;";

                try
                {
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {

                        int id = reader.GetInt32(0);
                        string rangeAChr = reader.GetString(1);
                        int rangeAMid = reader.GetInt32(2);
                        string signA = reader.GetString(3);
                        string rangeBChr = reader.GetString(4);
                        int rangeBMid = reader.GetInt32(5);
                        string signB = reader.GetString(6);
                        list.Add(new Interaction(rangeAChr, rangeAMid, signA, rangeBChr, rangeBMid, signB));
                    }
                    interactions.Add(c, list);
                }
                catch
                {
                    print("No database named " + chr + "_inter_" + c);
                }

            }
            }
        
        return interactions;
    }

  
    public List<Interaction> GetIntraInteractions(string chr, int l1 = -1, int l2 = -1)
    {
        List<Interaction> list = new List<Interaction>();

            using (var cmd = intraInteractionsConnection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                if(l1 == -1 && l2 == -1)
                    cmd.CommandText = "SELECT * FROM " + chr + " ORDER BY MidB ASC;";
                else
                    cmd.CommandText = "SELECT * FROM " + chr + "WHERE midA BETWEEN " + l1 + " AND" + l2 + " OR midB BETWEEN " + l1 + " AND " + l2 + "  ORDER BY MidB ASC;";
                
            try
                {
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string rangeAChr = reader.GetString(1);
                        int rangeAMid = reader.GetInt32(2);
                        string signA = reader.GetString(3);
                        string rangeBChr = reader.GetString(4);
                        int rangeBMid = reader.GetInt32(5);
                        string signB = reader.GetString(6);
                        list.Add(new Interaction(rangeAChr, rangeAMid, signA, rangeBChr, rangeBMid, signB));
                    }
                }
                catch
                {
                    print("No database named " + chr);
                }
            }

        return list;
    }


    public List<ProteinBinding> GetProteinBindings(string chr, int l1, int l2)
    {
        List<ProteinBinding> list = new List<ProteinBinding>();
        int table1 = l1 / 100000;
        int table2 = l2 / 100000;
        int counter = 0;

        using (var cmd = proteinBindingConnection.CreateCommand())
        {
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM " + chr + "_" + table1 + " WHERE l2 >= " + l1 + " AND l1 <= " + l2 + ";";
            SqliteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string name = reader.GetString(1);
                int a = reader.GetInt32(2);
                int b = reader.GetInt32(3);
                ProteinBinding p = new ProteinBinding(name, a, b);
                counter++;
                list.Add(p);
            }
            reader.Close();

        }
        if (table1 != table2)
        {
            using (var cmd = proteinBindingConnection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM " + chr + "_" + table2 + " WHERE l2>" + l1 + " AND l1<" + l2 + ";";
                SqliteDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string name = reader.GetString(1).Split('.')[0];
                    int a = reader.GetInt32(2);
                    int b = reader.GetInt32(3);
                    ProteinBinding p = new ProteinBinding(name, a, b);
                    counter++;
                    list.Add(p);
                }
                reader.Close();
            }
        }
        return list;
    }


}
