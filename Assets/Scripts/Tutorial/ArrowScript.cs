﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowScript : MonoBehaviour
{
    private int old = -1;
    public int stage = -1;

    public GameObject arrow;
    public GameObject cube;
    public GameObject display;


    public AudioClip[] clips;
    public AudioSource source;

    public Button[] buttons;

    // Arrow positions controller 
    private Vector3 firstPos = new Vector3(0f, 0.04f, -0.02f);
    private Vector3 secondPos = new Vector3(0.15f, 0.045f, -0.02f);
    private Vector3 thirdPos = new Vector3(0.22f, 0.04f, -0.07f);
    private Vector3 forthPos = new Vector3(-0.05f, -0.03f, -0.08f);
    private Vector3 forthRot = new Vector3(0f, 0f, 270f);
    private Vector3 fifthPos = new Vector3(0f, 0.04f, -0.05f);
    private Vector3 fifthRot = new Vector3(0f, 0f, -270f);
    private Vector3 sixthPos = new Vector3(0f, -0.055f, -0.03f);
    private Vector3 sixthRot = new Vector3(140f, 0f, 0f);



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        if (stage == 4)
        {
            if (1000000 > Mathf.Abs(display.GetComponent<DisplayHandler>().location1 - display.GetComponent<DisplayHandler>().location2))
            {
                nextStage();
            }
        }
        else if (stage == 5)
        {
            if (GameObject.Find("ChrOverview(Clone)"))
            {
                nextStage();
            }
        }
    }

    public void startTutorial()
    {
        arrow.SetActive(true);
        nextStage();
        buttons[0].gameObject.SetActive(false);
        buttons[1].gameObject.SetActive(true);

    }
    public void stopTutorial()
    {
        arrow.SetActive(false);
        stage = -1;
        buttons[0].gameObject.SetActive(true);
        buttons[1].gameObject.SetActive(false);
    }

    public void nextStage()
    {
        stage += 1;
        print(stage);

        if (stage == 0)
        {
            source.Stop();
            arrow.GetComponent<TutorialScript>().nextStage(firstPos);
            source.PlayOneShot(clips[0], 1f);

        }
        else if (stage == 1)
        {
            source.Stop();

            arrow.GetComponent<TutorialScript>().nextStage(secondPos);
            source.PlayOneShot(clips[1], 1f);

        }
        else if (stage == 2)
        {
            source.Stop();

            arrow.GetComponent<TutorialScript>().nextStage(thirdPos);
            source.PlayOneShot(clips[2], 1f);

        }
        else if (stage == 3)
        {
            source.Stop();
            arrow.GetComponent<TutorialScript>().nextStage(forthPos, forthRot);
            cube.SetActive(true);
            source.PlayOneShot(clips[3], 1f);

        }
        else if (stage == 4)
        {
            source.Stop();
            display = GameObject.Find("GenomeDisplay(Clone)");
            arrow.GetComponent<TutorialScript>().nextStage(fifthPos, fifthRot);
            cube.SetActive(false);
            source.PlayOneShot(clips[4], 1f);

        }
        else if (stage == 5)
        {
            source.Stop();
            arrow.GetComponent<TutorialScript>().nextStage(sixthPos, sixthRot);
            cube.gameObject.SetActive(false);
            source.PlayOneShot(clips[5], 1f);

        }
        else if (stage == 6)
        {
            source.Stop();
            arrow.gameObject.SetActive(false);
            //arrow.GetComponent<TutorialScript>().nextStage(fifthPos, fifthRot);
            source.PlayOneShot(clips[6], 1f);

        }
    }

}
