﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScript : MonoBehaviour
{

    private Transform arrowTrans;
    private Vector3 initPos;

    private float current = 1f;
    private float counter = 0f;
    private bool downDir = true;
    private bool exp = true;

    public GameObject touchpadHighlight;

        

    // Start is called before the first frame update
    void Start()
    {
        arrowTrans = this.gameObject.transform;
        initPos = this.gameObject.transform.position;


    }

    // Update is called once per frame
    void Update()
    {

        Vector3 dirr = new Vector3(0f, 1f, 0f);

        if (downDir)
        {
            //animateObject(touchpadHighlight);
            arrowTrans.Translate(dirr * 0.0002f);
            current -= 0.02f;
        }
        else
        {
            arrowTrans.Translate(dirr * -0.0002f);
            current += 0.02f;
        }
        //print(this.transform.parent.gameObject.transform.forward);
        //print(this.transform.forward);

        if (current < 0f)
        {
            downDir = false;
        }
        else if (current > 1f)
        {
            downDir = true;
        }

    }


    private void animateObject(GameObject obj)
    {
        if (exp)
        {
            obj.transform.localScale += new Vector3(0.0005f, 0f, 0.0005f);
            counter += 0.02f;

        } else
        {
            obj.transform.localScale += new Vector3(-0.0005f, 0f, -0.0005f);
            counter -= 0.02f;

        }


        if (counter < 0f)
        {
            exp = true;
        }
        else if (counter > 1f)
        {
            exp = false;
        }

    }

    public void nextStage(Vector3 newPos)
    {
        this.gameObject.transform.localPosition = newPos;
        current = 1f;
        counter = 0f;
    }

    public void nextStage(Vector3 newPos, Vector3 rot)
    {
        this.gameObject.transform.localPosition = newPos;
        this.gameObject.transform.Rotate(rot);
        
        current = 1f;
        counter = 0f;
    }


}
