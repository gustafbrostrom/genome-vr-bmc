﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{

    public GameObject tutorial;

    private void OnTriggerEnter(Collider other)
    {
        tutorial.GetComponent<ArrowScript>().nextStage();
    }

}
