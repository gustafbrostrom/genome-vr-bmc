﻿using System.Collections.Generic;
using UnityEngine;

public class PointOfInterest : MonoBehaviour
{
    public string chr;
    public int location;
    public Dictionary<int, Transform> representations = new Dictionary<int,Transform>();
    private LineRenderer line;
    
    


    public PointOfInterest()
    {
    }

    public void Setup(string chr, int location)
    {
        this.chr = chr;
        this.location = location;
        line = GetComponent<LineRenderer>();
        line.startWidth = line.endWidth = 0.01f;
    }

    private void Update()
    {

        //TRANSFORM.TRANSFORMCHANGED????
        line.positionCount = representations.Count;
        int i = 0;

        foreach(Transform t in representations.Values)
        {
            if(t.hasChanged)
                line.SetPosition(i, t.position);
            i++;
        }

    }


}
