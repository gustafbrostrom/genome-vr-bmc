﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IZoomable
{
    void zoom(float r, float or, float l, float ol);

    void translate(float n, float o);

    float percentage(Vector3 pos);
}
