﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RangeInformationDisplay : MonoBehaviour
{
    public TextMeshPro rangeText;
    public RangeInformation rangeInformation;
    public GameObject clickableTextPrefab;
    public Transform geneStart;
    public Transform intraStart;
    public Transform proteinStart;
    public ReferenceManager rm;


    private void OnValidate()
    {
        rangeText = transform.Find("Range").GetComponent<TextMeshPro>();
        geneStart = transform.Find("Genes").transform;
        intraStart = transform.Find("IntraInteraction").transform;
        proteinStart = transform.Find("Protein").transform;


    }

    private void SpawnInteractionDisplay(Interaction inter)
    {
        InteractionDisplay ID = Instantiate(rm.interactionDisplayPrefab, rm.rightHand.transform.position, Quaternion.identity).GetComponent<InteractionDisplay>();
        ID.transform.LookAt(rm.cameraGO.transform);
        ID.transform.Rotate(new Vector3(0, 180, 0));

        ID.interaction = inter;
        ID.newSearchDepth(0);

        ID.fillInfo();
    }


    public void FillInfo(ReferenceManager rm)
    {
        rangeText.text = rangeInformation.GetToString();
        this.rm = rm;
        List<Gene> genes = rangeInformation.GetSortedGenes();
        GameObject g;
        print(genes.Count);
        int i = 0;
        for (i = 0; i < 10 && i < genes.Count; i++)
        {
            Gene curr = genes[i];
            g = Instantiate(clickableTextPrefab, this.transform);
            g.transform.localPosition = geneStart.localPosition + Vector3.down*5*(i + 1);
            g.GetComponent<TextMeshPro>().text = curr.name + " " + (curr.l2 - curr.l1) + "bp";
            g.GetComponent<ClickableObject>().onClick.AddListener(() => rm.chromosomeManager.addDisplay(curr, rm.rightHand.transform.position));
        }

        g = Instantiate(clickableTextPrefab, this.transform);
        g.transform.localPosition = geneStart.localPosition + Vector3.down * 5 * (i + 1);
        g.GetComponent<TextMeshPro>().text = genes.Count - i + " more..";

        List<Interaction> intraChromosomalInteractions = rangeInformation.GetIntraChromosomalInteractions();

        for (i = 0; i < 10 && i < intraChromosomalInteractions.Count; i++)
        {
            Interaction curr = intraChromosomalInteractions[i];
            g = Instantiate(clickableTextPrefab, this.transform);
            g.transform.localPosition = intraStart.localPosition + Vector3.down * 5 * (i + 1);
            g.GetComponent<TextMeshPro>().text = curr.midA + " -> " + curr.midB;
            g.GetComponent<ClickableObject>().onClick.AddListener(() => SpawnInteractionDisplay(curr));
        }

        g = Instantiate(clickableTextPrefab, this.transform);
        g.transform.localPosition = intraStart.localPosition + Vector3.down * 5 * (i + 1);
        g.GetComponent<TextMeshPro>().text = intraChromosomalInteractions.Count - i + " more..";


        Dictionary<string, int> proteins = rangeInformation.GetProteinFrequency();
        if (proteins == null)
            return;
        i = 0;
        foreach(KeyValuePair<string,int> kvp in proteins)
        {
            g = Instantiate(clickableTextPrefab, this.transform);
            g.transform.localPosition = proteinStart.localPosition + Vector3.down * 5 * (i + 1);
            g.GetComponent<TextMeshPro>().text = kvp.Key + " " + kvp.Value;
            i++;
            if (i >= 10)
                break;
        }
        g = Instantiate(clickableTextPrefab, this.transform);
        g.transform.localPosition = proteinStart.localPosition + Vector3.down * 5 * (i + 1);
        g.GetComponent<TextMeshPro>().text = proteins.Count - i + " more..";
        
    }


    public void Close()
    {
        Destroy(gameObject);
    }


}
