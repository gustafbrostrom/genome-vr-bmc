﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RangeInformation : MonoBehaviour
{
    private ChromosomeManager chromosomeManager;
    private List<Interaction> intraChromosomalInteractions;
    private List<Gene> genes;
    private List<ProteinBinding> proteins;
    private string chr;
    private int l1, l2;


    public RangeInformation(string chr, int l1, int l2, ReferenceManager rm)
    {
        this.chr = chr;
        this.l1 = l1;
        this.l2 = l2;

        chromosomeManager = rm.chromosomeManager;
        genes = chromosomeManager.GetGenesInRange(chr, l1, l2).ToList<Gene>();
        genes.Sort((k, g) => (g.l2 - g.l1) - (k.l2 - k.l1));

        intraChromosomalInteractions = chromosomeManager.GetIntraInteractions(chr, l1, l2);
        //proteins = chromosomeManager.GetProteins(chr, l1, l2);
    }


    public string GetToString()
    {
        return chr + " " + string.Format("{0:#,0}", l1) + " - " + string.Format("{0:#,0}", l2);
    }

    public List<Gene> GetSortedGenes()
    {
        return genes;
    }

    public List<Interaction> GetIntraChromosomalInteractions()
    {
        return intraChromosomalInteractions;
    }
    
    public Dictionary<string, int> GetProteinFrequency()
    {
        if (proteins == null)
            return null;

        Dictionary<string, int> freq = new Dictionary<string, int>();
        var query = proteins.GroupBy(x => x.name).OrderByDescending(x => x.Count());

        foreach (var v in query)
        {
            freq.Add(v.Key, v.Count());
        }
        return freq;
    }

    public List<ProteinBinding> GetProteins()
    {
        return proteins;
    }
}
