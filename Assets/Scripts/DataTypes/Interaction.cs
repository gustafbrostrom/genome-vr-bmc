﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable()]
public class Interaction
{
    public string chrA, chrB;
    public string signA, signB;
    public int startA, endA, midA, startB, endB, midB;
    public float score;
    public HashSet<Gene> overLappingGenesRangeA = new HashSet<Gene>();
    public HashSet<Gene> overLappingGenesRangeB = new HashSet<Gene>();

    public int SearchDepth;

    public Interaction(string AChr, string BChr, int AStart, int AEnd, int AMid, int BStart, int BEnd, int BMid, float score)
    {
        this.chrA = AChr;
        this.chrB = BChr;
        this.startA = AStart;
        this.endA = AEnd;
        this.midA = AMid;
        this.startB = BStart;
        this.endB = BEnd;
        this.midB = BMid;
        this.score = score;
        this.SearchDepth = (int)(ReferenceManager.MaxSearchDepth * 0.1f);
    }

    public Interaction(string chrA, int midA, string signA, string chrB, int midB, string signB)
    {
        this.chrA = chrA;
        this.midA = midA;
        this.signA = signA;
        this.chrB = chrB;
        this.midB = midB;
        this.signB = signB;
        this.SearchDepth = (int)(ReferenceManager.MaxSearchDepth * 0.1f);
    }

    public Interaction(string AChr, string BChr, int AStart, int AEnd, int BStart, int BEnd, float score)
    {
        this.chrA = AChr;
        this.chrB = BChr;
        this.startA = AStart;
        this.endA = AEnd;
        this.midA = (AStart + AEnd) / 2;
        this.startB = BStart;
        this.endB = BEnd;
        this.midB = (BStart + BEnd) / 2;
        this.score = score;

        this.SearchDepth = (int)(ReferenceManager.MaxSearchDepth * 0.1f);
    }

    public bool isAnchorAOverlappingWithOtherAnchorA(Interaction other) {
        if (chrA != other.chrA)
            return false;
        return (startA < other.endA && endA > other.startA);
    }

    public bool isAnchorBOverlappingWithOtherAnchorB(Interaction other)
    {
        if (chrB != other.chrB)
            return false;
        return (startB < other.endB && endB > other.startB);
    }

    public bool isAnchorAOverlappingWithOtherAnchorB(Interaction other)
    {
        if (chrA != other.chrB)
            return false;
        return (startA < other.endB && endA > other.startB);
    }

    public bool isAnchorBOverlappingWithOtherAnchorA(Interaction other)
    {
        if (chrB != other.chrA)
            return false;
        return (startB < other.endA && endB > other.startA);
    }

    public bool areBothAnchorsOverlapping(Interaction other)
    {
        return (isAnchorAOverlappingWithOtherAnchorA(other) && isAnchorBOverlappingWithOtherAnchorB(other)) || isAnchorAOverlappingWithOtherAnchorB(other) && isAnchorBOverlappingWithOtherAnchorA(other);
    }

    public void addGeneToRangeA(Gene g)
    {
        overLappingGenesRangeA.Add(g);
    }

    public void addGeneToRangeB(Gene g)
    {
        overLappingGenesRangeB.Add(g);
    }

    public void removeGenesOutsideRanges()
    {
        overLappingGenesRangeA.RemoveWhere(isOutsideRangeA);
        overLappingGenesRangeB.RemoveWhere(isOutsideRangeB);
    }

    public override string ToString()
    {
        return chrA + " " + midA + " -> " + chrB + " " + midB;
    }

    private bool isOutsideRangeA(Gene g)
    {
        return g.l2 < midA - SearchDepth || g.l1 > midA + SearchDepth;
    }

    private bool isOutsideRangeB(Gene g)
    {
        return g.l2 < midB - SearchDepth || g.l1 > midB + SearchDepth;
    }


}
