﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable()]
public struct GeneV2
{
    public string Name { get; }
    public string Chr { get; }
    public int L1 { get; }
    public int L2 { get; }
    public int Dir { get; }
    public List<Exon> Exons { get; }
    public float R,G,B;

    public GeneV2(string name, string chr, int l1, int l2, int dir, Color color)
    {
        Name = name;
        Chr = chr;
        L1 = l1;
        L2 = l2;
        Dir = dir;
        Exons = new List<Exon>();
        R = color.r;
        G = color.g;
        B = color.b;

    }

    public override string ToString()
    {
        return Name + " " + L1 + " " + L2;
    }
}
