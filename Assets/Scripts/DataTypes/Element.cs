using System;

[Serializable()]

public class Element{

    public string chr;
    public int l1;
    public int l2;
    public string tag;

    public Element(string chr, int l1, int l2, string tag){
        this.chr = chr;
        this.l1 = l1;
        this.l2 = l2;
        this.tag = tag;
    }

    public override string ToString()
    {
        return chr + " " + l1 + " " + l2 + " "+ tag;
    }
}