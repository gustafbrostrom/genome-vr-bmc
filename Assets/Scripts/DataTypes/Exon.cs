﻿using System;

[Serializable()]
public struct Exon
{
    public string Id { get;  }
    public int L1 { get;  }
    public int L2 { get; }
    public int Dir { get; }

    public Exon(string id, int l1, int l2, int dir)
    {
        Id = id;
        L1 = l1;
        L2 = l2;
        Dir = dir;
    }
}
