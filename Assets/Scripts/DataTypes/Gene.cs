﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable()]
public class Gene
{
    public int id;
    public string chr;
    public string name;
    public int l1;
    public int l2;
    public int mid;
    public int dir;
    public string url;
    public int[,] exon;
    public Color color;
    public Color exonColor;
    public static Color[] colorwheel = new Color[] { Color.blue, Color.red, Color.yellow, Color.magenta, Color.green, Color.cyan };
    public Texture2D tex;
    private int texWidth = 1000;
    private int texHeight = 1;


    public Gene(int id, string chr,string url, string name, int l1, int l2, int dir, int[,] exon) {
        this.id = id;
        this.chr = chr;
        this.name = name;
        this.l1 = l1;
        this.l2 = l2;
        this.mid = (l2 - l1) / 2;
        this.dir = dir;
        this.exon = exon;
        color = colorwheel[id % colorwheel.Length];
        exonColor = Color.Lerp(color, Color.black,0.5f);
        this.url = url;
    }

    public Color[] getTexture(int location1, int location2){
        int l1 = location1<this.l1 ? this.l1 : location1;
        int l2 = location2>this.l2 ? this.l2 : location2;

        bool[] inExon = new bool[texWidth];
        for (int k = 0; k < exon.GetLength(0); k++)
        {
            float lowerPercentage = (float)(exon[k, 0] - l1) / (float)(l2 - l1);
            float upperPercentage = (float)(exon[k, 1] - l1) / (float)(l2 - l1);
            int lower = (int)(lowerPercentage * texWidth);
            int upper = (int)(upperPercentage * texWidth);
            if(upper<0 || lower >texWidth) //if this exon is outside the the part of the gene currently displayed, skip it
                continue;
            if(lower<0)
                lower = 0;
            if(upper>texWidth)
                upper = texWidth;
            for (int i = lower; i<upper; i++)
                inExon[i] = true;
        }
        Color[] colorArray = new Color[texWidth * texHeight];
        for (int i = 0; i < texWidth; i++)
        {
            for (int j = 0; j < texHeight; j++)
            {
                colorArray[j * texWidth + i] = inExon[i] ? exonColor : color;
            }
        }
        return colorArray;
    }

    public override string ToString() {
        return name + " " + l1 + " " + l2;
    }
}
