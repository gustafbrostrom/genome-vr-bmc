﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BigWigManagerScript : MonoBehaviour, IZoomable
{
    [SerializeField]
    private GameObject bigWigVisualisationPrefab;

    [SerializeField]
    private ReferenceManager rm;

    [SerializeField]
    private RectTransform canvas;

    private List<MeshGeneratorScript> tracks;

    public int l1;
    public int l2;
    public int binSize;
    public int length;
    public int height;
    public int width;

    public BoxCollider boxCollider;

    private Vector3 endPoint;

    private RightHandScript rightHand;
    private LeftHandScript leftHand;
    private float oldLeftHandPos = float.NegativeInfinity;
    private float oldRightHandPos = float.NegativeInfinity;
    private float leftHandPos = float.NegativeInfinity;
    private float rightHandPos = float.NegativeInfinity;

    
    [SerializeField] private TextMeshPro l1t;
    [SerializeField] private TextMeshPro l2t;

    // Start is called before the first frame update
    void Start()
    {
        tracks = new List<MeshGeneratorScript>();
        endPoint = Vector3.right * length;
        canvas.sizeDelta = new Vector2(length, height);
        l1t.text = ""+l1;
        l2t.text = "" + l2;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            MeshGeneratorScript mgs = Instantiate(bigWigVisualisationPrefab, transform, false).GetComponent<MeshGeneratorScript>();
            mgs.fileName = "FLEKO.short.bw";
            mgs.manager = this;
            mgs.bigReader = new BigReader(mgs.fileName);
            mgs.binSize = binSize;
            mgs.readPointsFromBigWig();
            mgs.transform.localPosition = Vector3.forward * tracks.Count;
            tracks.Add(mgs);
            updateBoxCollider();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            MeshGeneratorScript mgs = Instantiate(bigWigVisualisationPrefab, transform, false).GetComponent<MeshGeneratorScript>();
            mgs.fileName = "NALM6_ATAC_hg19_index1.bed";
            mgs.manager = this;
            mgs.bigReader = new BigReader(mgs.fileName);
            mgs.binSize = binSize;
            mgs.createRandomGraph();
            mgs.transform.localPosition = Vector3.forward * tracks.Count;
            tracks.Add(mgs);
            updateBoxCollider();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            MeshGeneratorScript mgs = Instantiate(bigWigVisualisationPrefab, transform, false).GetComponent<MeshGeneratorScript>();
            mgs.fileName = "NALM6_ATAC_hg19_index1.bed";
            mgs.manager = this;
            mgs.bigReader = new BigReader(mgs.fileName);
            mgs.binSize = binSize;
            mgs.createRandomGraph2();
            mgs.transform.localPosition = Vector3.forward * tracks.Count;
            tracks.Add(mgs);
            updateBoxCollider();
        }

       if (rightHand && rightHand.isPressingTrackpad)
        {
            rightHandPos = percentage(rightHand.transform.position);
        }
        else
        {
            rightHandPos = float.NegativeInfinity;
        }
        if (leftHand && leftHand.isPressingTrackpad)
        {
            leftHandPos = percentage(leftHand.transform.position);
        }
        else
        {
            leftHandPos = float.NegativeInfinity;
        }

        if (rightHandPos != float.NegativeInfinity && oldRightHandPos != float.NegativeInfinity && leftHandPos != float.NegativeInfinity && oldLeftHandPos != float.NegativeInfinity)
        {
            zoom(rightHandPos, oldRightHandPos, leftHandPos, oldLeftHandPos);
        }
        else if (rightHandPos != float.NegativeInfinity && oldRightHandPos != float.NegativeInfinity)
        {
            translate(rightHandPos, oldRightHandPos);
        }
        else if (leftHandPos != float.NegativeInfinity && oldLeftHandPos != float.NegativeInfinity)
        {
            translate(leftHandPos, oldLeftHandPos);
        }
        oldRightHandPos = rightHandPos;
        oldLeftHandPos = leftHandPos;

    }

    private void updateBoxCollider()
    {
        Vector3 size = new Vector3(length, height, width * tracks.Count);
        boxCollider.size = size;
        boxCollider.center = size / 2;
    }

    public void zoom(float r, float or, float l, float ol)
    {
        float scale = Mathf.Abs(r - l) / Mathf.Abs(or - ol);
        float mid = (r + l) / 2;
        float oMid = (or + ol) / 2;
        int transDist = (int)((mid - oMid) * (l2 - l1));
        l1 = l1 - transDist;
        l2 = l2 - transDist;

        int t1 = (int)(mid * (l2 - l1));
        int left = (int)(t1 * scale) - t1;

        int t2 = (int)((1 - mid) * (l2 - l1));
        int right = (int)(t2 * scale) - t2;
        l1 += left;
        l2 -= right;
        l1t.text = ""+l1;
        l2t.text = "" + l2;
        updateTracks();
    }

    public void translate(float n, float o)
    {
        int distToMove = (int)((n-o) * (l2 - l1));
        l1 -= distToMove;
        l2 -= distToMove;
        l1t.text = ""+l1;
        l2t.text = "" + l2;
        updateTracks();
    }

    public void updateTracks()
    {
        foreach (var track in tracks)
        {
            track.readPointsFromBigWig();
        }
    }

    //Testad, funkar korrekt
    public float percentage(Vector3 pos)
    {
        pos = transform.InverseTransformPoint(pos);
        pos = Vector3.Project(pos, endPoint);
        return pos.magnitude / endPoint.magnitude;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Controller (left)")
            leftHand = other.GetComponent<LeftHandScript>();
        if (other.gameObject.name == "Controller (right)")
            rightHand = other.GetComponent<RightHandScript>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Controller (left)")
        {
            leftHand = null;
        }
        if (other.gameObject.name == "Controller (right)")
        {
            rightHand = null;
        }
    }
}
