﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class Calling3D : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        float[,] f = new float[10,10];
        f[1,1] = 380.1f;
        f[4,5] = 321.2f;
        f[2,2] = 380.1f;
        f[7,4] = 321.2f;
        Controller3DMax m = new Controller3DMax(f, 100000, 200000);
        Task<List<Vector3>> t = m.Generate3DStructure();
        print("done");
        
        while(!t.IsCompleted) 
            continue;


        foreach(Vector3 v in t.Result)
            print(v);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
