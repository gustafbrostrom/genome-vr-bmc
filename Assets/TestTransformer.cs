﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TestTransformer : MonoBehaviour
{
    TextMeshPro textMeshPro;
    public bool button;
    // Start is called before the first frame update
    void Start()
    {
        textMeshPro = GetComponent<TextMeshPro>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnValidate(){
        if(button){
            button = false;
            MeshFilter mf = textMeshPro.GetComponent<MeshFilter>();
            Mesh mesh = mf.mesh;
            Vector3[] vertices = mesh.vertices;
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] += Vector3.up;
            }
            mesh.vertices = vertices;
        }
    }
}
